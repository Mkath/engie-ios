﻿using System;
using UIKit;

namespace E7.iOS.Utils
{
    public class TextField : UIPickerView
    {
        public event EventHandler DoneClicked;

        void OnDoneClicked()
        {
            EventHandler handler = DoneClicked;
            if (handler != null)
            {
                handler(this, new EventArgs());
            }
        }

        public TextField(UITextField textField)
        {
            UIToolbar toolBar = new UIToolbar()
            {
                BarStyle = UIBarStyle.Default,
                Translucent = true
            };
            toolBar.SizeToFit();

            UIBarButtonItem spaceItem = new UIBarButtonItem(UIBarButtonSystemItem.FlexibleSpace);

            UIBarButtonItem doneItem = new UIBarButtonItem()
            {
                Title = "Done",
                Style = UIBarButtonItemStyle.Done
            };
            doneItem.Clicked += (sender, e) =>
            {
                textField.ResignFirstResponder();
                OnDoneClicked();
            };
            toolBar.SetItems(new UIBarButtonItem[] { spaceItem, doneItem }, true);
            toolBar.UserInteractionEnabled = true;

            textField.InputAccessoryView = toolBar;
        }
    }
}
