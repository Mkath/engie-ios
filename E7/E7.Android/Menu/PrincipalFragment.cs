﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.Design.Widget;
using Android.Support.V4.Widget;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;
using V7Toolbar = Android.Support.V7.Widget.Toolbar;

namespace E7.Droid
{
    [Activity(Label = "PrincipalFragment", Theme = "@style/Theme.DesignDemo")]
    public class PrincipalFragment : AppCompatActivity
    {
        DrawerLayout drawerLayout;
        NavigationView navigationView;
        InicioActivity inicioFragment;
        //ConsumoFragment consumoFragment;
        //FacturacionFragment facturacionFragment;
        PreFacActivity facturacionFragment;
        PreConsActivity consumoFragment;
        //PreFacActivity facturacionFragment;
        string _usuarioInterno;
        string _nombreCompleto;
        V7Toolbar toolbar;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            // Create your application here

            _usuarioInterno = Intent.GetStringExtra("_usuarioInterno") ?? "Data not available";
            _nombreCompleto = Intent.GetStringExtra("_nombreCompleto") ?? "Data not available";

            SetContentView(layoutResID: Resource.Layout.PrincipalFragment);
            toolbar = FindViewById<V7Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            SupportActionBar.SetDisplayShowTitleEnabled(false);
            SupportActionBar.SetHomeButtonEnabled(true);
            SupportActionBar.SetHomeAsUpIndicator(Resource.Drawable.menu);

            drawerLayout = FindViewById<DrawerLayout>(Resource.Id.drawer_layout);
            navigationView = FindViewById<NavigationView>(Resource.Id.nav_view);

            CreateFragments();
            LoadInditialFragment();

            SwitchFragment(Resource.Id.home);
            navigationView.NavigationItemSelected += NavigationView_NavigationItemSelected;
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Android.Resource.Id.Home:
                    drawerLayout.OpenDrawer(Android.Support.V4.View.GravityCompat.Start);
                    return true;
            }
            return base.OnOptionsItemSelected(item);
        }

        void NavigationView_NavigationItemSelected(object sender, NavigationView.NavigationItemSelectedEventArgs e)
        {
            SwitchFragment(e.MenuItem.ItemId);
            drawerLayout.CloseDrawers();
        }

        private void CreateFragments()
        {
            inicioFragment = new InicioActivity(_usuarioInterno, _nombreCompleto);
            inicioFragment.CallConsumo += InicioFragment_CallConsumo;
            inicioFragment.CallFacturacion += InicioFragment_CallFacturacion;
            consumoFragment = new PreConsActivity();
            consumoFragment.CallConsumoFacturado += ConsumoFragment_CallConsumoFacturado;
            consumoFragment.CallMedicionYGraficas += ConsumoFragment_CallMedicionYGraficas;
            facturacionFragment = new PreFacActivity();
            facturacionFragment.CallDistribucionFacturada += FacturacionFragment_CallDistribucionFacturada;
            facturacionFragment.CallGraficoHistoricoFacturado += FacturacionFragment_CallGraficoHistoricoFacturado;
            facturacionFragment.CallGraficoHistoricoPrecios += FacturacionFragment_CallGraficoHistoricoPrecios;
        }

        void InicioFragment_CallConsumo(object sender, EventArgs e)
        {
            SwitchFragment(Resource.Id.con);
        }

        void InicioFragment_CallFacturacion(object sender, EventArgs e)
        {
            SwitchFragment(Resource.Id.fac);
        }

        void ConsumoFragment_CallConsumoFacturado(object sender, EventArgs e)
        {
            var intent = new Intent(this, typeof(Cons1Activity));
            intent.PutExtra("_usuarioInterno", _usuarioInterno);
            StartActivity(intent);
        }

        void ConsumoFragment_CallMedicionYGraficas(object sender, EventArgs e)
        {
            var intent = new Intent(this, typeof(Cons2Activity));
            intent.PutExtra("_usuarioInterno", _usuarioInterno);
            StartActivity(intent);
        }

        void FacturacionFragment_CallDistribucionFacturada(object sender, EventArgs e)
        {
            var intent = new Intent(this, typeof(fac1));
            intent.PutExtra("_usuarioInterno", _usuarioInterno);
            StartActivity(intent);
        }

        void FacturacionFragment_CallGraficoHistoricoFacturado(object sender, EventArgs e)
        {
            var intent = new Intent(this, typeof(fac2));
            intent.PutExtra("_usuarioInterno", _usuarioInterno);
            StartActivity(intent);
        }

        void FacturacionFragment_CallGraficoHistoricoPrecios(object sender, EventArgs e)
        {
            var intent = new Intent(this, typeof(fac3));
            intent.PutExtra("_usuarioInterno", _usuarioInterno);
            StartActivity(intent);
        }

        private void LoadInditialFragment()
        {
            var transaction = FragmentManager.BeginTransaction();
            transaction.Add(Resource.Id.fragmentContainer, inicioFragment).Hide(inicioFragment);
            transaction.Add(Resource.Id.fragmentContainer, consumoFragment).Hide(consumoFragment);
            transaction.Add(Resource.Id.fragmentContainer, facturacionFragment).Hide(facturacionFragment);
            transaction.Commit();
        }

        private void SwitchFragment(int FragmentId)
        {
            var transaction = FragmentManager.BeginTransaction();

            switch (FragmentId)
            {
                case Resource.Id.home:
                    transaction.Show(inicioFragment);
                    transaction.Hide(consumoFragment);
					transaction.Hide(facturacionFragment);
                    toolbar.SetBackgroundResource(Resource.Drawable.mylink);
                    //ActionBar.Title = "Consumo";
                    transaction.Commit();
                    break;
                
                case Resource.Id.con:
                    transaction.Show(consumoFragment);
                    transaction.Hide(inicioFragment);
					transaction.Hide(facturacionFragment);
                    toolbar.Background = null;
                    toolbar.SetBackgroundColor(Android.Graphics.Color.Rgb(0, 89, 158));
                    transaction.Commit();
                    break;

                case Resource.Id.fac:
                    transaction.Show(facturacionFragment);
                    transaction.Hide(inicioFragment);
                    transaction.Hide(consumoFragment);
                    toolbar.Background = null;
                    toolbar.SetBackgroundColor(Android.Graphics.Color.Rgb(167, 37, 28));
                    transaction.Commit();
                    break;
            }
        }
    }
}
