// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;

namespace E7.iOS
{
    [Register ("DistribucionFacturadaCell")]
    partial class DistribucionFacturadaCell
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel lblName { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel lblPorcentage { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView viewColor { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (lblName != null) {
                lblName.Dispose ();
                lblName = null;
            }

            if (lblPorcentage != null) {
                lblPorcentage.Dispose ();
                lblPorcentage = null;
            }

            if (viewColor != null) {
                viewColor.Dispose ();
                viewColor = null;
            }
        }
    }
}