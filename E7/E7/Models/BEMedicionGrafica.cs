﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace E7.Models
{
    public class BEMedicionGrafica
    {
        public string Item { get; set; }
        //public string FechayHora { get; set; }
        public DateTime FechayHora { get; set; }
        public string EnergiaActiva { get; set; }
        public string Potencia { get; set; }
        public string EnergiaReactiva { get; set; }
        public string Tension { get; set; }
        public string Corriente { get; set; }
    }
}