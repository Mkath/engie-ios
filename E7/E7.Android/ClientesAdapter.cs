﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;


namespace E7.Droid
{
    public class ClientesAdapter : BaseAdapter
    {
        private Context c;
        private JavaList<Clientes> clientes;
        private LayoutInflater inflater;

        public ClientesAdapter(Context c, JavaList<Clientes> clientes)
        {
            this.c = c;
            this.clientes = clientes;
        }
        public override Java.Lang.Object GetItem(int position)
        {
            return clientes.Get(position);
        }
        public override long GetItemId(int position)
        {
            return position;
        }
        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            if (inflater == null)
            {
                inflater = (LayoutInflater) c.GetSystemService(Context.LayoutInflaterService);
            }
            if (convertView == null)
            {
                convertView = inflater.Inflate(Resource.Layout.recursoSpinner,parent,false);
            }

            TextView cod = convertView.FindViewById<TextView>(Resource.Id.cod1);
            TextView nomb = convertView.FindViewById<TextView>(Resource.Id.nom1);
            cod.Text = clientes[position].CodCliente;
            nomb.Text = clientes[position].NomCliente;
            return convertView;
        }
        public override int Count
        {
            get { return clientes.Size(); }
        }
    }

}