﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using V7Toolbar = Android.Support.V7.Widget.Toolbar;
using Android.Support.V7.App;
using Android.Support.V4.Widget;
using Android.Support.Design.Widget;
using SciChart.Charting.Visuals;
using SciChart.Charting.Visuals.Axes;
using SciChart.Charting.Model.DataSeries;
using SciChart.Charting.Visuals.RenderableSeries;
using SciChart.Charting.Modifiers;
using SciChart.Drawing.Common;

namespace E7.Droid
{
    [Activity(Label = "Fac3Activity", Theme = "@style/Theme.DesignDemo")]
    public class Fac3Activity : AppCompatActivity
    {
        DrawerLayout drawerLayout;
        NavigationView navigationView;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetContentView(Resource.Layout.graf3);
            var toolbar = FindViewById<V7Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            SupportActionBar.SetDisplayShowTitleEnabled(false);
            SupportActionBar.SetHomeButtonEnabled(true);
            SupportActionBar.SetHomeAsUpIndicator(Resource.Drawable.menu);
            drawerLayout = FindViewById<DrawerLayout>(Resource.Id.drawer_layout);
            navigationView = FindViewById<NavigationView>(Resource.Id.nav_view);

            var chart = FindViewById<SciChartSurface>(Resource.Id.Chart);
            int themeid = Resource.Style.SciChart_Bright_Spark;
            chart.Theme = themeid;

            string _codCliente = Intent.GetStringExtra("_codCliente") ?? "Data not available";
            string _codPuntoF = Intent.GetStringExtra("_codPuntoF") ?? "Data not available";
            string _moneda = Intent.GetStringExtra("_moneda") ?? "Data not available";
            string _fechaD = Intent.GetStringExtra("_fechaD") ?? "Data not available";
            string _fechaH = Intent.GetStringExtra("_fechaH") ?? "Data not available";

            //string FechaDesde = "02/02/2017";
            //string FechaHasta = "02/10/2017";
            //string CodCliente = "7";
            //string CodPuntoFacturacion = "589";
            //string Moneda = "PEN";

            List<GraficoHistoricos> resultado;
            COReporte_Facturacion conexion = new COReporte_Facturacion();

            resultado = conexion.Reporte_HistoricoPrecio(_fechaD, _fechaH, _codCliente, _codPuntoF, _moneda);

            var Potencia = resultado.Where(x => x.Concepto == "Potencia").Select(x => (Convert.ToDouble(x.Consumo))).ToArray();
            var EnergiaHoraPunta = resultado.Where(x => x.Concepto == "EnergiaHoraPunta").Select(x => (Convert.ToDouble(x.Consumo))).ToArray();
            var EnergiaHoraFueraPunta = resultado.Where(x => x.Concepto == "EnergiaHoraFueraPunta").Select(x => (Convert.ToDouble(x.Consumo))).ToArray();
            var PrecioMedio = resultado.Where(x => x.Concepto == "Monomico").Select(x => (Convert.ToDouble(x.Consumo))).ToArray();
            var PCSPT = resultado.Where(x => x.Concepto == "PCSPT").Select(x => (Convert.ToDouble(x.Consumo))).ToArray();
            var SST = resultado.Where(x => x.Concepto == "SST").Select(x => (Convert.ToDouble(x.Consumo))).ToArray();


            var xAxis = new NumericAxis(this)
            {
                //AxisTitle = "Desde " + dt1 + "-" + dt11 + "/ Hasta " + dt2 + "-" + dt21,
            };
            var yAxis = new NumericAxis(this)
            {
                AxisTitle = "Gráfico Histórico Precios",
            };

            var PotenciaDataSeries = new XyDataSeries<double, double> { SeriesName = "Potencia" };
            var EnergiaHoraPuntaDataSeries = new XyDataSeries<double, double> { SeriesName = "Energia Hora Punta" };
            var EnergiaHoraFueraPuntaDataSeries = new XyDataSeries<double, double> { SeriesName = "Energia Hora Fuera Punta" };
            var PrecioMedioDataSeries = new XyDataSeries<double, double> { SeriesName = "Precio Medio" };
            var PCSPTDataSeries = new XyDataSeries<double, double> { SeriesName = "PCSPT" };
            var SSTDataSeries = new XyDataSeries<double, double> { SeriesName = "SST" };
            var totalDataSeries = new XyDataSeries<double, double> { SeriesName = "Total" };

            var cont = (Convert.ToDouble(resultado.Count) / Convert.ToDouble(6));

            for (var i = 0; i < cont; i++)
            {
                double xValue = i + 1;
                PotenciaDataSeries.Append(xValue, Potencia[i]);
                EnergiaHoraPuntaDataSeries.Append(xValue, EnergiaHoraPunta[i]);
                EnergiaHoraFueraPuntaDataSeries.Append(xValue, EnergiaHoraFueraPunta[i]);
                PrecioMedioDataSeries.Append(xValue, PrecioMedio[i]);
                PCSPTDataSeries.Append(xValue, PCSPT[i]);
                SSTDataSeries.Append(xValue, SST[i]);
                totalDataSeries.Append(xValue, Potencia[i] + EnergiaHoraPunta[i] + EnergiaHoraFueraPunta[i] + PrecioMedio[i] + PCSPT[i] + SST[i]);
            }

            var columnsCollection = new HorizontallyStackedColumnsCollection();

            columnsCollection.Add(GetRenderableSeries(PotenciaDataSeries, 0xff7cb5ec, 0xff7cb5ec));
            columnsCollection.Add(GetRenderableSeries(EnergiaHoraPuntaDataSeries, 0xFF434348, 0xFF434348));
            columnsCollection.Add(GetRenderableSeries(EnergiaHoraFueraPuntaDataSeries, 0xFF90ed7d, 0xFF90ed7d));
            columnsCollection.Add(GetRenderableSeries(PrecioMedioDataSeries, 0xFFf7a35c, 0xFFf7a35c));
            columnsCollection.Add(GetRenderableSeries(PCSPTDataSeries, 0xFF8085e9, 0xFF8085e9));
            columnsCollection.Add(GetRenderableSeries(SSTDataSeries, 0xfff15c80, 0xfff15c80));

            var legendModifier = new LegendModifier(this);
            legendModifier.SetLegendPosition(GravityFlags.Top | GravityFlags.Left, 10);
            chart.XAxes.Add(xAxis);
            chart.YAxes.Add(yAxis);

            chart.RenderableSeries.Add(columnsCollection);

            chart.ChartModifiers.Add(legendModifier);
            chart.ChartModifiers.Add(new TooltipModifier());
            chart.ChartModifiers.Add(new ZoomExtentsModifier());
            chart.ChartModifiers.Add(new PinchZoomModifier());
        }
        private StackedColumnRenderableSeries GetRenderableSeries(IDataSeries dataSeries, uint fillColor, uint strokeColor)
        {
            return new StackedColumnRenderableSeries
            {
                DataSeries = dataSeries,
                StrokeStyle = new SolidPenStyle(this, strokeColor),
                FillBrushStyle = new SolidBrushStyle(fillColor)
            };
        }
    }
}