﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using V7Toolbar = Android.Support.V7.Widget.Toolbar;
using Android.Support.V7.App;
using Android.Support.V4.Widget;
using Android.Support.Design.Widget;

namespace E7.Droid
{
    [Activity(Label = "PreConsActivity", Theme = "@style/Theme.DesignDemo")]
    public class PreConsActivity : AppCompatActivity
    {
        DrawerLayout drawerLayout;
        NavigationView navigationView;
        Button  btnf, btn5, btn6;
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            // Create your application here
           
            SetContentView(layoutResID: Resource.Layout.precons);
            var toolbar = FindViewById<V7Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            SupportActionBar.SetDisplayShowTitleEnabled(false);
            SupportActionBar.SetHomeButtonEnabled(true);
            SupportActionBar.SetHomeAsUpIndicator(Resource.Drawable.menu);
            drawerLayout = FindViewById<DrawerLayout>(Resource.Id.drawer_layout);
            navigationView = FindViewById<NavigationView>(Resource.Id.nav_view);

            btnf = FindViewById<Button>(Resource.Id.buttonf1);
            btn5 = FindViewById<Button>(Resource.Id.button5);
            btn6 = FindViewById<Button>(Resource.Id.button6);
                
            
            btnf.Click += (sender, e) => PreFacturacion();
            btn5.Click += (sender, e) => Cons1();
            btn6.Click += (sender, e) => Cons2();


           
            void PreFacturacion()
            {
                StartActivity(typeof(PreFacActivity));
            }

            void Cons1()
            {
                StartActivity(typeof(Cons1Activity));
            }
            void Cons2()
            {
                StartActivity(typeof(Cons2Activity));
            }


           
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Android.Resource.Id.Home:
                    drawerLayout.OpenDrawer(Android.Support.V4.View.GravityCompat.Start);
                    return true;
            }
            return base.OnOptionsItemSelected(item);
        }
    }
}