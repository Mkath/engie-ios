// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace E7.iOS
{
    [Register ("ConsumoController")]
    partial class ConsumoController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton btnConsumoFacturado { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton btnMedicion { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel lblInfo { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIStackView stackPrincipal { get; set; }

        [Action ("UIButton18844_TouchUpInside:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void UIButton18844_TouchUpInside (UIKit.UIButton sender);

        [Action ("UIButton18845_TouchUpInside:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void UIButton18845_TouchUpInside (UIKit.UIButton sender);

        void ReleaseDesignerOutlets ()
        {
            if (btnConsumoFacturado != null) {
                btnConsumoFacturado.Dispose ();
                btnConsumoFacturado = null;
            }

            if (btnMedicion != null) {
                btnMedicion.Dispose ();
                btnMedicion = null;
            }

            if (lblInfo != null) {
                lblInfo.Dispose ();
                lblInfo = null;
            }

            if (stackPrincipal != null) {
                stackPrincipal.Dispose ();
                stackPrincipal = null;
            }
        }
    }
}