﻿using System;
using System.Collections.Generic;

namespace E7.Models
{
    public class BEClientes
    {
        public string CodCliente { get; set; }
        public string NomCliente { get; set; }
        public List<BEMedidor> MedCliente { get; set; }
        public List<BEPuntoFacturacion> PFaCliente { get; set; }
    }
}
