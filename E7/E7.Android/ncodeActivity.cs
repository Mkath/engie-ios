﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace E7.Droid
{
    [Activity(Label = "ncodeActivity", Theme = "@android:style/Theme.NoTitleBar")]
    public class ncodeActivity : Activity
    {
        Button btn1;
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            // Create your application here

            SetContentView(layoutResID: Resource.Layout.ncode);

            btn1 = FindViewById<Button>(Resource.Id.button1);

           
            btn1.Click += (sender, e) => npass();

           

            void npass()
            {
                StartActivity(typeof(NpassActivity));
            }
        }
    }
}