﻿using System;
namespace E7.Models
{
    public class DistribucionFacturadaModel
    {
        public string Cliente { get; set; }
        public int ClienteID { get; set; }
        public string PuntoF { get; set; }
        public int PuntoFID { get; set; }
        public string Mes { get; set; }
        public string Anio { get; set; }
        public string Moneda { get; set; }
    }
}
