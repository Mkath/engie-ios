﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace E7.Droid
{
    public class BEConsumoFacturado
    {
        public string Item { get; set; }
        public string Anio { get; set; }
        public string Mes { get; set; }
        public string EnergiaActiva { get; set; }
        public string Potencia { get; set; }
    }
}