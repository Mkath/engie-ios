﻿using Android.App;
using System;
using Android.Views;
using Android.Content;
using Android.Runtime;
using Android.OS;

using Android.Widget;


namespace E7.Droid
{
    [Activity(Label = "menu", Theme = "@style/Theme.DesignDemo")]
    public class menu : Activity
    {


        Button inicio, consumo, facturacion;
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            // Create your application here

            SetContentView(layoutResID: Resource.Menu.menu);

            //navigationView = FindViewById<NavigationView>(Resource.Id.nav_view);
            inicio = FindViewById<Button>(Resource.Id.home);
            consumo= FindViewById<Button>(Resource.Id.con);
            facturacion = FindViewById<Button>(Resource.Id.fac);

            
            inicio.Click += (sender, e) => iniciopage();
            consumo.Click += (sender, e) => conspage();
            facturacion.Click += (sender, e) => facpage();


            void iniciopage()
            {
                StartActivity(typeof(InicioActivity));
            }
            void conspage()
            {
                StartActivity(typeof(PreConsActivity));
            }
            void facpage()
            {
                StartActivity(typeof(PreFacActivity));
            }


        }

      
    }
}