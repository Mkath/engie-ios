﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using V7Toolbar = Android.Support.V7.Widget.Toolbar;
using Android.Support.V7.App;
using Android.Support.V4.Widget;
using Android.Support.Design.Widget;

namespace E7.Droid
{
    [Activity(Label = "fac3", Theme = "@style/Theme.DesignDemo")]
    public class fac3 : AppCompatActivity
    {
        DrawerLayout drawerLayout;
        NavigationView navigationView;
        ClientesAdapter adapter_c;
        PuntoFacturacionAdapter adapter_p;
        MesAdapter adapter_m;
        JavaList<Clientes> clientes;
        JavaList<PuntoFacturacion> puntoFacturacion;
        JavaList<Mes> mes;
        Spinner CodCliente, IdPuntoFacturacion, Moneda, mesD, anioD, mesH, anioH;
        ImageButton Graficar;
        string _codCliente, _codPuntoF, _moneda, _mesD, _anioD, _fechaD, _mesH, _anioH, _fechaH;
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            // Create your application here

            //Listar Clientes y Punto de Facturacion
            string _usuarioInterno = Intent.GetStringExtra("_usuarioInterno") ?? "Data not available";
            COLogin oLogin = new COLogin();
            clientes = oLogin.Usuario_Cliente(_usuarioInterno);

            SetContentView(layoutResID: Resource.Layout.fac3);

            var toolbar = FindViewById<V7Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            SupportActionBar.SetDisplayShowTitleEnabled(false);
            SupportActionBar.SetHomeButtonEnabled(true);
            SupportActionBar.SetHomeAsUpIndicator(Resource.Drawable.menu);
            drawerLayout = FindViewById<DrawerLayout>(Resource.Id.drawer_layout);
            navigationView = FindViewById<NavigationView>(Resource.Id.nav_view);
            Graficar = FindViewById<ImageButton>(Resource.Id.imageButton1);

            Graficar.Click += (sender, e) => Graficos();

            //Filtros
            CodCliente = FindViewById<Spinner>(Resource.Id.spinner1);
            IdPuntoFacturacion = FindViewById<Spinner>(Resource.Id.spinner2);
            Moneda = FindViewById<Spinner>(Resource.Id.spinner3);
            mesD = FindViewById<Spinner>(Resource.Id.spinner4);
            anioD = FindViewById<Spinner>(Resource.Id.spinner5);
            mesH = FindViewById<Spinner>(Resource.Id.spinner6);
            anioH = FindViewById<Spinner>(Resource.Id.spinner7);


            CodCliente.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs>(CodCliente_ItemSelected);
            IdPuntoFacturacion.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs>(IdPuntoFacturacion_ItemSelected);
            Moneda.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs>(Moneda_ItemSelected);
            mesD.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs>(MesD_ItemSelected);
            anioD.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs>(AnioD_ItemSelected);
            mesH.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs>(MesH_ItemSelected);
            anioH.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs>(AnioH_ItemSelected);
            
            ListarClientes();
            ListarMoneda();
            ListarMesD();
            ListarAnioD();
            ListarMesH();
            ListarAnioH();
            void Graficos()
            {
                _fechaD = string.Format("01/{0}/{1}", _mesD, _anioD);
                _fechaH = string.Format("01/{0}/{1}", _mesH, _anioH);

                var intent_data = new Intent(this, typeof(Fac1Activity));
                intent_data.PutExtra("_usuarioInterno", _usuarioInterno);
                intent_data.PutExtra("_codCliente", _codCliente);
                intent_data.PutExtra("_codPuntoF", _codPuntoF);
                intent_data.PutExtra("_moneda", _moneda);
                intent_data.PutExtra("_fechaD", _fechaD);
                intent_data.PutExtra("_fechaH", _fechaH);
                StartActivity(intent_data);
            }

            void ListarClientes()
            {
                _codPuntoF = null;
                var _clientes = clientes;
                adapter_c = new ClientesAdapter(this, clientes);
                CodCliente.Adapter = adapter_c;
            }

            void CodCliente_ItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
            {
                _codCliente = clientes[e.Position].CodCliente;
                ListarPuntoFacturacion(_codCliente);
            }
            void ListarPuntoFacturacion(string _codCliente)
            {
                var _puntoFacturacion = clientes.Where(x => x.CodCliente == _codCliente).Select(x => x.PFaCliente).ToList();
                foreach (JavaList<PuntoFacturacion> item in _puntoFacturacion)
                {
                    puntoFacturacion = item;
                }
                adapter_p = new PuntoFacturacionAdapter(this, puntoFacturacion);
                IdPuntoFacturacion.Adapter = adapter_p;
            }
            void IdPuntoFacturacion_ItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
            {
                _codPuntoF = puntoFacturacion[e.Position].ID;
            }
            void ListarMoneda()
            {
                string[] items = new[] { "USD", "PEN" };
                var Adaptador = new ArrayAdapter<string>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, items);
                Moneda.Adapter = Adaptador;
            }
            void Moneda_ItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
            {
                Spinner spinner = (Spinner)sender;
                _moneda = spinner.GetItemAtPosition(e.Position).ToString();
            }
            void ListarMesD()
            {
                mes = COMes.ListarMes();
                adapter_m = new MesAdapter(this, mes);
                mesD.Adapter = adapter_m;
                var i = ((DateTime.Now).Month)-1;
                mesD.SetSelection(i);
            }

            void MesD_ItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
            {
                _mesD = mes[e.Position].IdMes;
            }
            void ListarAnioD()
            {
                string[] items = new[] { "2017","2016","2015" };
                var Adaptador = new ArrayAdapter<string>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, items);
                anioD.Adapter = Adaptador;
            }
            void AnioD_ItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
            {
                Spinner spinner = (Spinner)sender;
                _anioD = spinner.GetItemAtPosition(e.Position).ToString();
            }
            void ListarMesH()
            {
                mes = COMes.ListarMes();
                adapter_m = new MesAdapter(this, mes);
                mesH.Adapter = adapter_m;
                var i = ((DateTime.Now).Month) - 1;
                mesH.SetSelection(i);
            }

            void MesH_ItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
            {
                _mesH = mes[e.Position].IdMes;
            }
            void ListarAnioH()
            {
                string[] items = new[] { "2017", "2016", "2015" };
                var Adaptador = new ArrayAdapter<string>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, items);
                anioH.Adapter = Adaptador;
            }
            void AnioH_ItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
            {
                Spinner spinner = (Spinner)sender;
                _anioH = spinner.GetItemAtPosition(e.Position).ToString();
            }
        }

        //Menu
        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Android.Resource.Id.Home:
                    drawerLayout.OpenDrawer(Android.Support.V4.View.GravityCompat.Start);
                    return true;
            }
            return base.OnOptionsItemSelected(item);
        }
    }
}