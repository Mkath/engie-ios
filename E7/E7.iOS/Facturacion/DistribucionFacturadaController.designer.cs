// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace E7.iOS
{
    [Register ("DistribucionFacturadaController")]
    partial class DistribucionFacturadaController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel lblClientes { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel lblFacturacion { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel lblFecha { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField pickerTextFieldAnio { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField pickerTextFieldClientes { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField pickerTextFieldFacturacion { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField pickerTextFieldMes { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIStackView stackPrincipal { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView stackPrincipalBg { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView txtTitle { get; set; }

        [Action ("UIButton32816_TouchUpInside:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void UIButton32816_TouchUpInside (UIKit.UIButton sender);

        void ReleaseDesignerOutlets ()
        {
            if (lblClientes != null) {
                lblClientes.Dispose ();
                lblClientes = null;
            }

            if (lblFacturacion != null) {
                lblFacturacion.Dispose ();
                lblFacturacion = null;
            }

            if (lblFecha != null) {
                lblFecha.Dispose ();
                lblFecha = null;
            }

            if (pickerTextFieldAnio != null) {
                pickerTextFieldAnio.Dispose ();
                pickerTextFieldAnio = null;
            }

            if (pickerTextFieldClientes != null) {
                pickerTextFieldClientes.Dispose ();
                pickerTextFieldClientes = null;
            }

            if (pickerTextFieldFacturacion != null) {
                pickerTextFieldFacturacion.Dispose ();
                pickerTextFieldFacturacion = null;
            }

            if (pickerTextFieldMes != null) {
                pickerTextFieldMes.Dispose ();
                pickerTextFieldMes = null;
            }

            if (stackPrincipal != null) {
                stackPrincipal.Dispose ();
                stackPrincipal = null;
            }

            if (stackPrincipalBg != null) {
                stackPrincipalBg.Dispose ();
                stackPrincipalBg = null;
            }

            if (txtTitle != null) {
                txtTitle.Dispose ();
                txtTitle = null;
            }
        }
    }
}