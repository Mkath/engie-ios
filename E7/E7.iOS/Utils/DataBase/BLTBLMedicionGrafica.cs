﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using E7.Models;
using E7.Services;

namespace E7.iOS.Utils.DataBase
{
    public class BLTBLMedicionGrafica
    {
        List<BEMedicionGrafica> origen = new List<BEMedicionGrafica>();
        DAReporte_Consumo services = new DAReporte_Consumo();
        TBLMedicionGrafica entidad = new TBLMedicionGrafica();
        DATBLMedicionGrafica db = new DATBLMedicionGrafica();
        public string MessageCreateMG { get; set; }
        public string MessageInsertMG { get; set; }
        public string MessageReportMG { get; set; }

        public async Task<bool> CreateTableMG()
        {
            bool salida = true;
            try
            {
                var val = await db.ValidateTableMG();
                if (val == null)
                {
                    salida = await db.CreateTableMG();
                }
                else
                {
                    salida = true;
                    //string cadena = Encoding.UTF8.GetString(val[1].BusquedaJson);
                }
                return salida;
            }
            catch (Exception ex)
            {
                MessageCreateMG = ex.Message;
                return salida = false;
            }
        }

        public async Task<bool> SincronizarMG(List<Clientes> cliente, int cont)
        {
            bool salida = true;
            try
            {
                var _clientes = cliente.ToArray();
                DateTime hasta = DateTime.Now;
                string _hasta = hasta.ToString("dd/MM/yyyy");
                var desde = hasta.AddMonths(-cont);
                string _desde = desde.ToString("dd/MM/yyyy");
                for (int a = 0; a < _clientes.Count(); a++)
                {
                    if (salida == true)
                    {
                        var codCliente = _clientes[a].CodCliente;
                        var listMedidores = _clientes[a].MedCliente;

                        for (int b = 0; b < listMedidores.Count; b++)
                        {
                            if (salida == true)
                            {
                                var codMedidor = listMedidores[b].Codigo;

                                var del = db.DeleteTableMG(codCliente, codMedidor);
                                origen = await services.Reporte_MedidionGrafica(_desde, _hasta, codCliente, codMedidor);
                                var mensaje = services.ResultadoFinalRMG;
                                if (origen == null)
                                {
                                    salida = false;
                                    throw new System.InvalidOperationException();
                                }
                                else if (origen.Count == 0)
                                {
                                    salida = true;
                                }
                                else
                                {
                                    var final = Newtonsoft.Json.JsonConvert.SerializeObject(origen);
                                    System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
                                    var busqueda = encoding.GetBytes(final);

                                    entidad.CodCliente = codCliente;
                                    entidad.CodMedicion = codMedidor;
                                    entidad.BusquedaJson = busqueda;
                                    salida = await db.InsertTableMG(entidad);
                                }
                            }
                            else
                            {
                                throw new System.InvalidOperationException();
                            }
                        }
                    }
                    else
                    {
                        throw new System.InvalidOperationException();
                    }
                }
                return salida;
            }
            catch (Exception ex)
            {
                return salida;
            }
        }

        public async Task<List<TBLMedicionGrafica>> Reporte_MedicionGrafica(TBLMedicionGrafica mg)
        {
            List<TBLMedicionGrafica> list = new List<TBLMedicionGrafica>();
            try
            {
                list = await db.SelectTableMG(mg);
                return list;
            }
            catch (Exception ex)
            {
                MessageReportMG = ex.Message;
                return null;
            }
        }
    }
}
