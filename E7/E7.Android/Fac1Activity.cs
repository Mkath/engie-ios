﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using V7Toolbar = Android.Support.V7.Widget.Toolbar;
using Android.Support.V7.App;
using Android.Support.V4.Widget;
using Android.Support.Design.Widget;
using SciChart.Charting.Visuals;
using SciChart.Charting.Visuals.Axes;
using SciChart.Charting.Model.DataSeries;
using SciChart.Charting.Visuals.RenderableSeries;
using SciChart.Charting.Modifiers;
using SciChart.Drawing.Common;

namespace E7.Droid
{
    [Activity(Label = "", Icon = "@drawable/calendario", Theme = "@style/Theme.DesignDemo")]
    public class Fac1Activity : AppCompatActivity
    {
        DrawerLayout drawerLayout;
        NavigationView navigationView;
        string _usuarioInterno;
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetContentView(Resource.Layout.graf1);
            var toolbar = FindViewById<V7Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);
            //SupportActionBar.SetHomeButtonEnabled(true);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            //drawerLayout = FindViewById<DrawerLayout>(Resource.Id.drawer_layout);
            //navigationView = FindViewById<NavigationView>(Resource.Id.nav_view);

            var chart = FindViewById<SciChartSurface>(Resource.Id.Chart);
            int themeid = Resource.Style.SciChart_Bright_Spark;
            chart.Theme = themeid;

            _usuarioInterno = Intent.GetStringExtra("_usuarioInterno") ?? "Data not available";
            string _codCliente = Intent.GetStringExtra("_codCliente") ?? "Data not available";
            string _codPuntoF = Intent.GetStringExtra("_codPuntoF") ?? "Data not available";
            string _moneda = Intent.GetStringExtra("_moneda") ?? "Data not available";
            string _fechaD = Intent.GetStringExtra("_fechaD") ?? "Data not available";
            string _fechaH = Intent.GetStringExtra("_fechaH") ?? "Data not available";

            //string FechaDesde = "02/02/2017";
            //string FechaHasta = "02/10/2017";
            //string CodCliente = "7";
            //string CodPuntoFacturacion = "589";
            //string Moneda = "PEN";

            List<GraficoHistoricos> resultado;
            COReporte_Facturacion conexion = new COReporte_Facturacion();

            resultado = conexion.Reporte_HistoricoFacturado(_fechaD, _fechaH, _codCliente, _codPuntoF, _moneda);

            var EnergiaActiva = resultado.Where(x => x.Concepto == "EnergiaActiva").Select(x => (Convert.ToDouble(x.Consumo))).ToArray();
            var Potencia = resultado.Where(x => x.Concepto == "Potencia").Select(x => (Convert.ToDouble(x.Consumo))).ToArray();
            var EnergiaReactiva = resultado.Where(x => x.Concepto == "EnergiaReactiva").Select(x => (Convert.ToDouble(x.Consumo))).ToArray();
            var PeajePrincipal = resultado.Where(x => x.Concepto == "PeajePrincipal").Select(x => (Convert.ToDouble(x.Consumo))).ToArray();
            var PeajeSecundario = resultado.Where(x => x.Concepto == "PeajeSecundario").Select(x => (Convert.ToDouble(x.Consumo))).ToArray();
            var Otros = resultado.Where(x => x.Concepto == "Otros").Select(x => (Convert.ToDouble(x.Consumo))).ToArray();
            var ElectrificacionRural = resultado.Where(x => x.Concepto == "ElectrificacionRural").Select(x => (Convert.ToDouble(x.Consumo))).ToArray();
            var FISE = resultado.Where(x => x.Concepto == "FISE").Select(x => (Convert.ToDouble(x.Consumo))).ToArray();


            var xAxis = new NumericAxis(this);
            var yAxis = new NumericAxis(this)
            {
                AxisTitle = "Total " + _moneda,
                LabelProvider = new MillionsLabelProvider(),
            };

            var EnergiaActivaDataSeries = new XyDataSeries<double, double> { SeriesName = "Energia Activa" };
            var PotenciaDataSeries = new XyDataSeries<double, double> { SeriesName = "Potencia" };
            var EnergiaReactivaDataSeries = new XyDataSeries<double, double> { SeriesName = "Energia Reactiva" };
            var PeajePrincipalDataSeries = new XyDataSeries<double, double> { SeriesName = "Peaje Principal" };
            var PeajeSecundarioDataSeries = new XyDataSeries<double, double> { SeriesName = "Peaje Secundario" };
            var OtrosDataSeries = new XyDataSeries<double, double> { SeriesName = "Otros" };
            var ElectrificacionRuralDataSeries = new XyDataSeries<double, double> { SeriesName = "Electrificacion Rural" };
            var FISEDataSeries = new XyDataSeries<double, double> { SeriesName = "FISE" };
            var totalDataSeries = new XyDataSeries<double, double> { SeriesName = "Total" };

            var cont = (Convert.ToDouble(resultado.Count) / Convert.ToDouble(8));

            for (var i = 0; i < cont; i++)
            {
                double xValue = i + 1;
                EnergiaActivaDataSeries.Append(xValue, EnergiaActiva[i]);
                PotenciaDataSeries.Append(xValue, Potencia[i]);
                EnergiaReactivaDataSeries.Append(xValue, EnergiaReactiva[i]);
                PeajePrincipalDataSeries.Append(xValue, PeajePrincipal[i]);
                PeajeSecundarioDataSeries.Append(xValue, PeajeSecundario[i]);
                OtrosDataSeries.Append(xValue, Otros[i]);
                ElectrificacionRuralDataSeries.Append(xValue, ElectrificacionRural[i]);
                FISEDataSeries.Append(xValue, FISE[i]);
                totalDataSeries.Append(xValue, EnergiaActiva[i] + Potencia[i] + EnergiaReactiva[i] + PeajePrincipal[i] + PeajeSecundario[i] + Otros[i] + ElectrificacionRural[i] + FISE[i]);
            }

            var columnsCollection = new HorizontallyStackedColumnsCollection();

            columnsCollection.Add(GetRenderableSeries(EnergiaActivaDataSeries, 0xff7cb5ec, 0xff7cb5ec));
            columnsCollection.Add(GetRenderableSeries(PotenciaDataSeries, 0xFF434348, 0xFF434348));
            columnsCollection.Add(GetRenderableSeries(EnergiaReactivaDataSeries, 0xFF90ed7d, 0xFF90ed7d));
            columnsCollection.Add(GetRenderableSeries(PeajePrincipalDataSeries, 0xFFf7a35c, 0xFFf7a35c));
            columnsCollection.Add(GetRenderableSeries(PeajeSecundarioDataSeries, 0xFF8085e9, 0xFF8085e9));
            columnsCollection.Add(GetRenderableSeries(OtrosDataSeries, 0xfff15c80, 0xfff15c80));
            columnsCollection.Add(GetRenderableSeries(ElectrificacionRuralDataSeries, 0xFFe4d354, 0xFFe4d354));
            columnsCollection.Add(GetRenderableSeries(FISEDataSeries, 0xff2b908f, 0xff2b908f));
            columnsCollection.Add(GetRenderableSeries(totalDataSeries, 0xff005fa3, 0xff005fa3));

            var legendModifier = new LegendModifier(this);
            legendModifier.SetLegendPosition(GravityFlags.Top | GravityFlags.Left, 9);
            chart.XAxes.Add(xAxis);
            chart.YAxes.Add(yAxis);

            chart.RenderableSeries.Add(columnsCollection);

            chart.ChartModifiers.Add(legendModifier);
            chart.ChartModifiers.Add(new TooltipModifier());
            chart.ChartModifiers.Add(new ZoomExtentsModifier());
            chart.ChartModifiers.Add(new PinchZoomModifier());

        }
        private StackedColumnRenderableSeries GetRenderableSeries(IDataSeries dataSeries, uint fillColor, uint strokeColor)
        {
            return new StackedColumnRenderableSeries
            {
                DataSeries = dataSeries,
                StrokeStyle = new SolidPenStyle(this, strokeColor),
                FillBrushStyle = new SolidBrushStyle(fillColor)
            };
        }


        //Toolbar Back Button
        public override bool OnOptionsItemSelected(IMenuItem item)
        {

            if (item.ItemId == Android.Resource.Id.Home) ;
            {
                Finish();
                var intent_data = new Intent(this, typeof(fac3));
                intent_data.PutExtra("_usuarioInterno", _usuarioInterno);
                StartActivity(intent_data);
            }
            return base.OnOptionsItemSelected(item);

        }
    }
}
