﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using E7.Models;
using E7.Services;

namespace E7.iOS.Utils.DataBase
{
    public class BLTBLDistribucionFacturada
    {
        List<BEGraficosFacturacion> origen = new List<BEGraficosFacturacion>();
        DAReporte_Facturacion services = new DAReporte_Facturacion();
        TBLDistribucionFacturada entidad = new TBLDistribucionFacturada();
        DATBLDistribucionFacturada db = new DATBLDistribucionFacturada();

        public string MessageCreateDF { get; set; }
        public string MessageInsertDF { get; set; }
        public string MessageReportDF { get; set; }

        public async Task<bool> CreateTableDF()
        {
            bool salida = true;
            try
            {
                var val = await db.ValidateTableDF();
                if (val == null)
                {
                    salida = await db.CreateTableDF();
                }
                else
                {
                    salida = true;
                }
                return salida;
            }
            catch (Exception ex)
            {
                MessageCreateDF = ex.Message;
                return salida = false;
            }
        }

        public async Task<bool> SincronizarDF(List<Clientes> cliente, int count)
        {
            bool salida = false;
            try
            {
                var _clientes = cliente.ToArray();
                DateTime dateTime = DateTime.Now;
                var date = dateTime.AddMonths(-count);
                var del = db.DeleteTableDF(date);
                for (int a = 0; a < _clientes.Count(); a++)
                {
                    var codCliente = _clientes[a].CodCliente;
                    var listPuntoF = _clientes[a].PFaCliente;

                    for (int b = 0; b < listPuntoF.Count; b++)
                    {
                        var idPuntoF = listPuntoF[b].ID;

                        for (int c = -1; c >= -count; c--)
                        {
                            var dfecha = dateTime.AddMonths(c);
                            string fecha = dfecha.ToString("dd/MM/yyyy");
                            origen = await services.Reporte_DistribucionFacturacion(fecha, codCliente, idPuntoF, "USD");
                            var mensaje = services.ResultadoFinalRDF;
                            if (origen == null)
                            {
                                salida = false;
                            }
                            else if (origen.Count == 0)
                            {
                                salida = true;
                            }
                            else
                            {
                                var final = origen.ToArray();
                                for (int d = 0; d < origen.Count; d++)
                                {
                                    entidad.CodCliente = codCliente;
                                    entidad.CodPuntoFacturacion = idPuntoF;
                                    entidad.Mes = fecha.Substring(3, 2);
                                    entidad.Anio = fecha.Substring(6, 4);
                                    entidad.Fecha = DateTime.ParseExact(fecha, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                    entidad.Concepto = final[d].Concepto;
                                    entidad.Consumo = final[d].Consumo;
                                    salida = await db.InsertTableDF(entidad);
                                }
                            }
                        }
                    }
                }
                return salida;
            }
            catch (Exception ex)
            {
                MessageInsertDF = ex.Message;
                return salida;
            }
        }

        public async Task<List<TBLDistribucionFacturada>> Reporte_DistribucionFacturacion(TBLDistribucionFacturada df)
        {
            List<TBLDistribucionFacturada> list = new List<TBLDistribucionFacturada>();
            try
            {
                list = await db.SelectTableDF(df);
                return list;
            }
            catch (Exception ex)
            {
                MessageReportDF = ex.Message;
                return null;
            }
        }
    }
}
