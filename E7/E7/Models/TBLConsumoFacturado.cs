﻿using System;
namespace E7.Models
{
    public class TBLConsumoFacturado
    {
        public string Anio { get; set; }
        public string CodCliente { get; set; }
        public string CodPuntoFacturacion { get; set; }
        public string EnergiaActiva { get; set; }
        public string Potencia { get; set; }
        public DateTime Fecha { get; set; }
    }
}
