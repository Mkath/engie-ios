﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using E7.Models;
using E7.Services;

namespace E7.iOS.Utils.DataBase
{
    public class BLTBLHistoricoPrecio
    {
        List<BEGraficosFacturacion> origen = new List<BEGraficosFacturacion>();
        DAReporte_Facturacion services = new DAReporte_Facturacion();
        TBLHistoricoPrecio entidad = new TBLHistoricoPrecio();
        DATBLHistoricoPrecio db = new DATBLHistoricoPrecio();

        public string MessageCreateHP { get; set; }
        public string MessageInsertHP { get; set; }
        public string MessageReportHP { get; set; }

        public async Task<bool> CreateTableHP()
        {
            bool salida = true;
            try
            {
                var val = await db.ValidateTableHP();
                if (val == null)
                {
                    salida = await db.CreateTableHP();
                }
                else
                {
                    salida = true;
                }
                return salida;
            }
            catch (Exception ex)
            {
                MessageCreateHP = ex.Message;
                return salida = false;
            }
        }

        public async Task<bool> SincronizarHP(List<Clientes> cliente, int count)
        {
            bool salida = true;
            try
            {
                var _clientes = cliente.ToArray();
                DateTime dateTime = DateTime.Now;
                var hasta = new DateTime(dateTime.Year, dateTime.Month, 20);
                var date = dateTime.AddMonths(-count);
                var desde = new DateTime(date.Year, date.Month, 1);
                string[] moneda = new[] { "PEN", "USD" };

                var del = db.DeleteTableHP(desde);
                for (int a = 0; a < _clientes.Count(); a++)
                {
                    if (salida == true)
                    {
                        var _codCliente = _clientes[a].CodCliente;
                        var listPuntoF = _clientes[a].PFaCliente;

                        for (int b = 0; b < listPuntoF.Count; b++)
                        {
                            if (salida == true)
                            {
                                var _idPuntoF = listPuntoF[b].ID;

                                for (int c = 0; c < moneda.Count(); c++)
                                {
                                    if (salida == true)
                                    {
                                        var _moneda = moneda[c];
                                        string _desde = desde.ToString("dd/MM/yyyy");
                                        string _hasta = hasta.ToString("dd/MM/yyyy");
                                        origen = await services.Reporte_HistoricoPrecio(_desde, _hasta, _codCliente, _idPuntoF, _moneda);
                                        var mensaje = services.ResultadoFinalRHF;
                                        if (origen == null)
                                        {
                                            salida = false;
                                        }
                                        else if (origen.Count == 0)
                                        {
                                            salida = true;
                                        }
                                        else
                                        {
                                            var final = origen.ToArray();
                                            for (int d = 0; d < origen.Count; d++)
                                            {
                                                if (salida == true)
                                                {
                                                    entidad.CodCliente = _codCliente;
                                                    entidad.CodPuntoFacturacion = _idPuntoF;
                                                    entidad.Moneda = _moneda;
                                                    entidad.Fecha = final[d].Fecha;
                                                    entidad.Concepto = final[d].Concepto;
                                                    entidad.Consumo = final[d].Consumo;
                                                    salida = await db.InsertTableHP(entidad);
                                                }
                                                else
                                                {
                                                    throw new System.InvalidOperationException();
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        throw new System.InvalidOperationException();
                                    }
                                }
                            }
                            else
                            {
                                throw new System.InvalidOperationException();
                            }
                        }
                    }
                    else
                    {
                        throw new System.InvalidOperationException();
                    }
                }
                return salida;
            }
            catch (Exception ex)
            {
                MessageInsertHP = ex.Message;
                return salida;
            }
        }

        public async Task<List<TBLHistoricoPrecio>> Reporte_HistoricoPrecio(TBLHistoricoPrecio hp, DateTime desde, DateTime hasta)
        {
            List<TBLHistoricoPrecio> list = new List<TBLHistoricoPrecio>();
            try
            {
                list = await db.SelectTableHP(hp, desde, hasta);
                return list;
            }
            catch (Exception ex)
            {
                MessageReportHP = ex.Message;
                return null;
            }
        }
        public async Task<List<TBLHistoricoPrecio>> Reporte_HistoricoPrecio(TBLHistoricoPrecio hp)
        {
            List<TBLHistoricoPrecio> list = new List<TBLHistoricoPrecio>();
            try
            {
                list = await db.SelectTableHP(hp);
                return list;
            }
            catch (Exception ex)
            {
                MessageReportHP = ex.Message;
                return null;
            }
        }
    }
}
