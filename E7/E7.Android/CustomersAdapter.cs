﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Java.Lang;
using Java.Lang;

namespace E7.Droid
{
    public class CustomersAdapter : BaseAdapter
    {
        private Context c;
        private JavaList<Customer> customers;
        private LayoutInflater inflater;

        public CustomersAdapter(Context c, JavaList<Customer> customers)
        {
            this.c = c;
            this.customers = customers;
        }
        public override Java.Lang.Object GetItem(int position)
        {
            return customers.Get(position);
        }
        public override long GetItemId(int position)
        {
            return position;
        }
        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            if (inflater == null)
            {
                inflater = (LayoutInflater) c.GetSystemService(Context.LayoutInflaterService);
            }
            if (convertView == null)
            {
                convertView = inflater.Inflate(Resource.Layout.recursoSpinner,parent,false);
            }

            TextView cod = convertView.FindViewById<TextView>(Resource.Id.cod1);
            TextView nomb = convertView.FindViewById<TextView>(Resource.Id.nom1);
            cod.Text = customers[position].CodCliente;
            nomb.Text = customers[position].NomCliente;
            return convertView;
        }
        public override int Count
        {
            get { return customers.Size(); }
        }
    }

}