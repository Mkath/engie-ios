﻿using System;

using UIKit;

namespace E7.iOS
{
    public partial class ConsumoController : BaseController//UIViewController
    {
        //public ConsumoController() : base("ConsumoController", null)
        //{
        //}

        public ConsumoController(IntPtr b) : base(b) { }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            // Perform any additional setup after loading the view, typically from a nib.

            // 8 Plus = 736
            // 8 = 667
            // SE = 568

            var main = Application.GetInstance();
            int med = 0;
            foreach (var x in main.clientes)
                if (x.MedCliente != null)
                    foreach (var y in x.MedCliente)
                        med += 1;

            if (med == 0) // Si no se tienen medidores no se muestra el botón de Medición y Gráficas
            {
                btnMedicion.RemoveFromSuperview();

                var w = this.View.Frame.Width * .70;
                nfloat fw = (nfloat)w;

                stackPrincipal.BackgroundColor = UIColor.Magenta;
                stackPrincipal.WidthAnchor.ConstraintEqualTo(fw).Active = true;
                stackPrincipal.WidthAnchor.ConstraintEqualTo(btnConsumoFacturado.HeightAnchor, 2.0f).Active = true;
                stackPrincipal.CenterYAnchor.ConstraintEqualTo(View.CenterYAnchor, 0.5f).Active = true;

                NSLayoutConstraint.Create(this.TopLayoutGuide, NSLayoutAttribute.Top,
                                          NSLayoutRelation.Equal, stackPrincipal, NSLayoutAttribute.Top,
                                         1, -220).Active = true;

                if (this.View.Frame.Height > 568)
                {
                    NSLayoutConstraint.Create(this.TopLayoutGuide, NSLayoutAttribute.Top,
                                          NSLayoutRelation.Equal, stackPrincipal, NSLayoutAttribute.Top,
                                         1, -80).Active = true;

                    NSLayoutConstraint.Create(this.BottomLayoutGuide, NSLayoutAttribute.Top,
                                              NSLayoutRelation.Equal, stackPrincipal, NSLayoutAttribute.Bottom,
                                              1, 355).Active = true;

                    lblInfo.MinimumFontSize = 18f;
                }
                else
                {
                    NSLayoutConstraint.Create(this.BottomLayoutGuide, NSLayoutAttribute.Top,
                                              NSLayoutRelation.Equal, stackPrincipal, NSLayoutAttribute.Bottom,
                                              1, 105).Active = true;
                }
            }
            else
            {
				if (this.View.Frame.Height > 568)
				{
					NSLayoutConstraint.Create(this.BottomLayoutGuide, NSLayoutAttribute.Top,
					                          NSLayoutRelation.Equal, stackPrincipal, NSLayoutAttribute.Bottom,
					                          1, 155).Active = true;
					
					lblInfo.MinimumFontSize = 18f;
				}
				else
				{
					NSLayoutConstraint.Create(this.BottomLayoutGuide, NSLayoutAttribute.Top,
					                          NSLayoutRelation.Equal, stackPrincipal, NSLayoutAttribute.Bottom,
					                          1, 105).Active = true;
				}
            }
            
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);
        }

        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
            // Release any cached data, images, etc that aren't in use.
        }

        partial void UIButton18844_TouchUpInside(UIButton sender)
        {
            NavigationItem.BackBarButtonItem = new UIBarButtonItem("", UIBarButtonItemStyle.Plain, null);
        }

        partial void UIButton18845_TouchUpInside(UIButton sender)
        {
            NavigationItem.BackBarButtonItem = new UIBarButtonItem("", UIBarButtonItemStyle.Plain, null);
        }
    }
}

