using Foundation;
using System;
using UIKit;
using SciChart.iOS.Charting;
using CoreGraphics;
using E7.iOS.Utils;
using E7.Services;
using System.Linq;
using System.Collections.Generic;
using E7.Models;
using E7.iOS.Utils.DataBase;
using System.Globalization;
using E7.iOS.Offline;

namespace E7.iOS
{
    public partial class ConsultaGraficoHistoricoPrecios : UIViewController
    {
        SCIChartSurface _surface;
        SCIHorizontallyStackedColumnsCollection _columnCollection;
        RestClient _client;
        SCIDateTimeAxis _x;
        UIView _view;
        CGRect legendViewRect;
        string _moneda;

        public ConsultaGraficoHistoricoPrecios() : base("ConsultaGraficoHistoricoPrecios", null)
        {
        }

        public ConsultaGraficoHistoricoPrecios(IntPtr b) : base(b) { }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            // Perform any additional setup after loading the view, typically from a nib.

            _client = new RestClient();
            EdgesForExtendedLayout = UIRectEdge.None;
            ExtendedLayoutIncludesOpaqueBars = false;
            AutomaticallyAdjustsScrollViewInsets = false;
        }

        public override void ViewDidAppear(bool animated)
        {
            base.ViewDidAppear(animated);

            _surface = new SCIChartSurface();
            _surface.TranslatesAutoresizingMaskIntoConstraints = true;
            CGRect surfaceRect = new CGRect();
            surfaceRect.Width = this.View.Bounds.Width;
            surfaceRect.Height = this.View.Bounds.Height / 2;
            _surface.Frame = surfaceRect;
            //_surface.Frame = this.View.Bounds;

            legendViewRect = new CGRect();
            legendViewRect.Y = this.View.Bounds.Height / 2;
            legendViewRect.Width = this.View.Bounds.Width;
            legendViewRect.Height = this.View.Bounds.Height / 2;

            _view = new UIView();
            _view.Frame = legendViewRect;

            SCIThemeManager.ApplyTheme(_surface, SCIThemeManager.SCIChart_Bright_SparkStyleKey);

            this.View.AddSubview(_surface);
            this.View.AddSubview(_view);

            _x = ChartCreator.xDateAxis();
            _x.TextFormatting = "MMM yyyy";
            _surface.XAxes.Add(_x);

            var y = ChartCreator.yAxis("Consumo");
            y.GrowBy = new SCIDoubleRange(0, 0.5);
            _surface.YAxes.Add(y);

            GetReporte();
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);
            UILabel title = new UILabel() { Text = "Gráfico Histórico de Precios", TextColor = UIColor.White };

            NavigationController.NavigationBar.BarTintColor = Application.Rojo;
            NavigationController.NavigationBar.Translucent = false;

            NavigationItem.TitleView = title;

            var online = _client.CheckConnection();
            if (!online)
            {
                var navBar = NavigationController.NavigationBar;
                NavigationItem.SetRightBarButtonItem(
                    new UIBarButtonItem(OfflineLabel.Label(navBar.Bounds.Width, navBar.Bounds.Height, true)),
                    false);
            }
        }

        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
            // Release any cached data, images, etc that aren't in use.
        }

        async void GetReporte()
        {
            var main = Application.GetInstance();
            var gHP = main.graficoHistoricoPrecios;
            List<BEGraficosFacturacion> resultado;
            List<TBLHistoricoPrecio> resultado1;
            DAReporte_Facturacion conexion = new DAReporte_Facturacion();
            BLTBLHistoricoPrecio conexion1 = new BLTBLHistoricoPrecio();
            TBLHistoricoPrecio data = new TBLHistoricoPrecio();
            Alert alert = new Alert();
            string mhp;
            string MensajeErrorHP;

            _moneda = gHP.Moneda;

            var online = _client.CheckConnection();
            var _modooff = "";
            if (online)
                _modooff = "0";
            else
                _modooff = main.ModoOff;

            if (_moneda == "USD") { mhp = "$ "; } else if (_moneda == "PEN") { mhp = "S/ "; } else { mhp = ""; }
            if (_modooff == "0")
            {
                resultado = await conexion.Reporte_HistoricoPrecio(gHP.Desde, gHP.Hasta, gHP.ClienteID.ToString(), gHP.PuntoFID.ToString(), _moneda);
                MensajeErrorHP = conexion.ResultadoFinalRHP;
                if (resultado == null)
                {
                    var a = alert.ShowMessage("Alerta", MensajeErrorHP);
                    a.Clicked += A_Clicked;
                    a.Show();
                }
                else if (resultado.Count == 0)
                {
                    var a = alert.ShowMessage("Alerta", "No hay datos.");
                    a.Clicked += A_Clicked;
                    a.Show();
                }
                else
                {
                    var Fecha = resultado.OrderBy(x => x.Fecha).Select(x => x.Fecha).Distinct().ToArray();
                    var Potencia = resultado.OrderBy(x => x.Fecha).Where(x => x.Concepto == "Potencia").Select(x => (Convert.ToDouble(x.Consumo))).ToArray();
                    var EnergiaHoraPunta = resultado.OrderBy(x => x.Fecha).Where(x => x.Concepto == "EnergiaHoraPunta").Select(x => (Convert.ToDouble(x.Consumo))).ToArray();
                    var EnergiaHoraFueraPunta = resultado.OrderBy(x => x.Fecha).Where(x => x.Concepto == "EnergiaHoraFueraPunta").Select(x => (Convert.ToDouble(x.Consumo))).ToArray();
                    var PrecioMedio = resultado.OrderBy(x => x.Fecha).Where(x => x.Concepto == "Monomico").Select(x => (Convert.ToDouble(x.Consumo))).ToArray();
                    var PCSPT = resultado.OrderBy(x => x.Fecha).Where(x => x.Concepto == "PCSPT").Select(x => (Convert.ToDouble(x.Consumo))).ToArray();
                    var SST = resultado.OrderBy(x => x.Fecha).Where(x => x.Concepto == "SST").Select(x => (Convert.ToDouble(x.Consumo))).ToArray();
                    var cont = (Convert.ToDouble(resultado.Count) / Convert.ToDouble(6));
                    //GraficarColumn(Fecha, Potencia, EnergiaHoraPunta, EnergiaHoraFueraPunta, PrecioMedio, PCSPT, SST, cont);

                    if (resultado.Count != 0)
                    {
                        var PotenciaDataSeries = new XyDataSeries<DateTime, double> { SeriesName = "Potencia" };
                        var EnergiaHoraPuntaDataSeries = new XyDataSeries<DateTime, double> { SeriesName = "Energia Hora Punta" };
                        var EnergiaHoraFueraPuntaDataSeries = new XyDataSeries<DateTime, double> { SeriesName = "Energia Hora Fuera Punta" };
                        var PrecioMedioDataSeries = new XyDataSeries<DateTime, double> { SeriesName = "Precio Medio" };
                        var PCSPTDataSeries = new XyDataSeries<DateTime, double> { SeriesName = "PCSPT" };
                        var SSTDataSeries = new XyDataSeries<DateTime, double> { SeriesName = "SST" };
                        var totalDataSeries = new XyDataSeries<DateTime, double> { SeriesName = "Total" };

                        if (Fecha.Count() == 1)
                            _x.AxisTitle = Fecha[0].ToString("MMM yyyy");

                        for (var i = 0; i < resultado.Count / 6; i++)
                        {
                            PotenciaDataSeries.Append(Fecha[i], Potencia[i]);
                            EnergiaHoraPuntaDataSeries.Append(Fecha[i], EnergiaHoraPunta[i]);
                            EnergiaHoraFueraPuntaDataSeries.Append(Fecha[i], EnergiaHoraFueraPunta[i]);
                            PrecioMedioDataSeries.Append(Fecha[i], PrecioMedio[i]);
                            PCSPTDataSeries.Append(Fecha[i], PCSPT[i]);
                            SSTDataSeries.Append(Fecha[i], SST[i]);
                        }

                        _columnCollection = new SCIHorizontallyStackedColumnsCollection();
                        _columnCollection.Add(ChartCreator.GetColumnRenderableSeriesWithUint(PotenciaDataSeries, 0xff7cb5ec, 0xff7cb5ec));
                        _columnCollection.Add(ChartCreator.GetColumnRenderableSeriesWithUint(EnergiaHoraPuntaDataSeries, 0xFF434348, 0xFF434348));
                        _columnCollection.Add(ChartCreator.GetColumnRenderableSeriesWithUint(EnergiaHoraFueraPuntaDataSeries, 0xFF90ed7d, 0xFF90ed7d));
                        _columnCollection.Add(ChartCreator.GetColumnRenderableSeriesWithUint(PrecioMedioDataSeries, 0xFFf7a35c, 0xFFf7a35c));
                        _columnCollection.Add(ChartCreator.GetColumnRenderableSeriesWithUint(PCSPTDataSeries, 0xFF8085e9, 0xFF8085e9));
                        _columnCollection.Add(ChartCreator.GetColumnRenderableSeriesWithUint(SSTDataSeries, 0xfff15c80, 0xfff15c80));

                        var legendModifier = new SCILegendModifier();
                        legendModifier.Orientation = SCIOrientation.Vertical;
                        legendModifier.Position = SCILegendPosition.Left;
                        legendModifier.SourceMode = SCISourceMode.AllVisibleSeries;
                        legendModifier.ShowSeriesMarkers = true;
                        legendModifier.ShowCheckBoxes = true;
                        legendModifier.HolderLegendView.Frame = legendViewRect;

                        _surface.RenderableSeries.Add(_columnCollection);
                        _surface.ChartModifiers.Add(legendModifier);
                        _surface.ChartModifiers.Add(new SCICursorModifier());
                        _surface.ChartModifiers.Add(new SCIPinchZoomModifier()
                        {
                            Direction = SCIDirection2D.XDirection
                        });
                        _surface.ChartModifiers.Add(new SCIZoomExtentsModifier());
                        _surface.ChartModifiers.Add(new SCIXAxisDragModifier());

                        _view.AddSubview(legendModifier.HolderLegendView);
                    }
                    else
                    {
                        var a = alert.ShowMessage("Alerta", "No hay datos");
                        a.Clicked += A_Clicked;
                        a.Show();
                    }
                }
            }
            else if (_modooff != "0")
            {
                DateTime desde = DateTime.ParseExact(gHP.Desde, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                DateTime _desde = new DateTime(desde.Year, desde.Month, 1);
                DateTime _Fdesde = new DateTime(desde.Year, desde.Month, 20);
                DateTime hasta = DateTime.ParseExact(gHP.Hasta, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                DateTime _hasta = new DateTime(hasta.Year, hasta.Month, 20);
                data.CodCliente = gHP.ClienteID.ToString();
                data.CodPuntoFacturacion = gHP.PuntoFID.ToString();
                data.Moneda = _moneda;
                resultado1 = await conexion1.Reporte_HistoricoPrecio(data, _desde, _hasta);
                MensajeErrorHP = "Error al momento de leer los datos guardados";
                if (resultado1 == null)
                {
                    var a = alert.ShowMessage("Alerta", MensajeErrorHP);
                    a.Clicked += A_Clicked;
                    a.Show();
                    return;
                }
				var _ultimoMes = resultado1.Where(x => (x.Fecha <= _Fdesde)).ToArray();
                if (resultado1.Count == 0 || _ultimoMes.Count() == 0)
                {
                    resultado1 = await conexion1.Reporte_HistoricoPrecio(data);
                    if (resultado1 == null)
                    {
                        var a = alert.ShowMessage("Alerta", "No hay datos sincronizados.");
                        a.Clicked += A_Clicked;
                        a.Show();
                    }
                    else if (resultado1.Count == 0)
                    {
                        var a = alert.ShowMessage("Alerta", "No hay datos sincronizados.");
                        a.Clicked += A_Clicked;
                        a.Show();
                    }
                    else
                    {
                        string _meses = "";
                        if (_modooff == "1") { _meses = " mes."; } else { _meses = " meses."; }
                        var a = alert.ShowMessage("Alerta", "Usted solo tiene datos sincronizados de " + _modooff + _meses);
                        a.Clicked += (sender, e) => {
                            var Fecha = resultado1.OrderBy(x => x.Fecha).Select(x => x.Fecha).Distinct().ToArray();
                            var Potencia = resultado1.OrderBy(x => x.Fecha).Where(x => x.Concepto == "Potencia").Select(x => (Convert.ToDouble(x.Consumo))).ToArray();
                            var EnergiaHoraPunta = resultado1.OrderBy(x => x.Fecha).Where(x => x.Concepto == "EnergiaHoraPunta").Select(x => (Convert.ToDouble(x.Consumo))).ToArray();
                            var EnergiaHoraFueraPunta = resultado1.OrderBy(x => x.Fecha).Where(x => x.Concepto == "EnergiaHoraFueraPunta").Select(x => (Convert.ToDouble(x.Consumo))).ToArray();
                            var PrecioMedio = resultado1.OrderBy(x => x.Fecha).Where(x => x.Concepto == "Monomico").Select(x => (Convert.ToDouble(x.Consumo))).ToArray();
                            var PCSPT = resultado1.OrderBy(x => x.Fecha).Where(x => x.Concepto == "PCSPT").Select(x => (Convert.ToDouble(x.Consumo))).ToArray();
                            var SST = resultado1.OrderBy(x => x.Fecha).Where(x => x.Concepto == "SST").Select(x => (Convert.ToDouble(x.Consumo))).ToArray();
                            var cont = (Convert.ToDouble(resultado1.Count) / Convert.ToDouble(6));
                            //GraficarColumn(Fecha, Potencia, EnergiaHoraPunta, EnergiaHoraFueraPunta, PrecioMedio, PCSPT, SST, cont);

                            if (resultado1.Count != 0)
                            {
                                var PotenciaDataSeries = new XyDataSeries<DateTime, double> { SeriesName = "Potencia" };
                                var EnergiaHoraPuntaDataSeries = new XyDataSeries<DateTime, double> { SeriesName = "Energia Hora Punta" };
                                var EnergiaHoraFueraPuntaDataSeries = new XyDataSeries<DateTime, double> { SeriesName = "Energia Hora Fuera Punta" };
                                var PrecioMedioDataSeries = new XyDataSeries<DateTime, double> { SeriesName = "Precio Medio" };
                                var PCSPTDataSeries = new XyDataSeries<DateTime, double> { SeriesName = "PCSPT" };
                                var SSTDataSeries = new XyDataSeries<DateTime, double> { SeriesName = "SST" };
                                var totalDataSeries = new XyDataSeries<DateTime, double> { SeriesName = "Total" };

                                if (Fecha.Count() == 1)
                                    _x.AxisTitle = Fecha[0].ToString("MMM yyyy");

                                for (var i = 0; i < resultado1.Count / 6; i++)
                                {
                                    PotenciaDataSeries.Append(Fecha[i], Potencia[i]);
                                    EnergiaHoraPuntaDataSeries.Append(Fecha[i], EnergiaHoraPunta[i]);
                                    EnergiaHoraFueraPuntaDataSeries.Append(Fecha[i], EnergiaHoraFueraPunta[i]);
                                    PrecioMedioDataSeries.Append(Fecha[i], PrecioMedio[i]);
                                    PCSPTDataSeries.Append(Fecha[i], PCSPT[i]);
                                    SSTDataSeries.Append(Fecha[i], SST[i]);
                                }

                                _columnCollection = new SCIHorizontallyStackedColumnsCollection();
                                _columnCollection.Add(ChartCreator.GetColumnRenderableSeriesWithUint(PotenciaDataSeries, 0xff7cb5ec, 0xff7cb5ec));
                                _columnCollection.Add(ChartCreator.GetColumnRenderableSeriesWithUint(EnergiaHoraPuntaDataSeries, 0xFF434348, 0xFF434348));
                                _columnCollection.Add(ChartCreator.GetColumnRenderableSeriesWithUint(EnergiaHoraFueraPuntaDataSeries, 0xFF90ed7d, 0xFF90ed7d));
                                _columnCollection.Add(ChartCreator.GetColumnRenderableSeriesWithUint(PrecioMedioDataSeries, 0xFFf7a35c, 0xFFf7a35c));
                                _columnCollection.Add(ChartCreator.GetColumnRenderableSeriesWithUint(PCSPTDataSeries, 0xFF8085e9, 0xFF8085e9));
                                _columnCollection.Add(ChartCreator.GetColumnRenderableSeriesWithUint(SSTDataSeries, 0xfff15c80, 0xfff15c80));

                                var legendModifier = new SCILegendModifier();
                                legendModifier.Orientation = SCIOrientation.Vertical;
                                legendModifier.Position = SCILegendPosition.Left;
                                legendModifier.SourceMode = SCISourceMode.AllVisibleSeries;
                                legendModifier.ShowSeriesMarkers = true;
                                legendModifier.ShowCheckBoxes = true;
                                legendModifier.HolderLegendView.Frame = legendViewRect;

                                _surface.RenderableSeries.Add(_columnCollection);
                                _surface.ChartModifiers.Add(legendModifier);
                                _surface.ChartModifiers.Add(new SCICursorModifier());
                                _surface.ChartModifiers.Add(new SCIPinchZoomModifier()
                                {
                                    Direction = SCIDirection2D.XDirection
                                });
                                _surface.ChartModifiers.Add(new SCIZoomExtentsModifier());
                                _surface.ChartModifiers.Add(new SCIXAxisDragModifier());

                                _view.AddSubview(legendModifier.HolderLegendView);
                            }
                            else
                            {
                                a = alert.ShowMessage("Alerta", "No hay datos");
                                a.Clicked += A_Clicked;
                                a.Show();
                            }
                        };
                        a.Show();
                    }
                }
                else
                {
                    var Fecha = resultado1.OrderBy(x => x.Fecha).Select(x => x.Fecha).Distinct().ToArray();
                    var Potencia = resultado1.OrderBy(x => x.Fecha).Where(x => x.Concepto == "Potencia").Select(x => (Convert.ToDouble(x.Consumo))).ToArray();
                    var EnergiaHoraPunta = resultado1.OrderBy(x => x.Fecha).Where(x => x.Concepto == "EnergiaHoraPunta").Select(x => (Convert.ToDouble(x.Consumo))).ToArray();
                    var EnergiaHoraFueraPunta = resultado1.OrderBy(x => x.Fecha).Where(x => x.Concepto == "EnergiaHoraFueraPunta").Select(x => (Convert.ToDouble(x.Consumo))).ToArray();
                    var PrecioMedio = resultado1.OrderBy(x => x.Fecha).Where(x => x.Concepto == "Monomico").Select(x => (Convert.ToDouble(x.Consumo))).ToArray();
                    var PCSPT = resultado1.OrderBy(x => x.Fecha).Where(x => x.Concepto == "PCSPT").Select(x => (Convert.ToDouble(x.Consumo))).ToArray();
                    var SST = resultado1.OrderBy(x => x.Fecha).Where(x => x.Concepto == "SST").Select(x => (Convert.ToDouble(x.Consumo))).ToArray();
                    var cont = (Convert.ToDouble(resultado1.Count) / Convert.ToDouble(6));
                    //GraficarColumn(Fecha, Potencia, EnergiaHoraPunta, EnergiaHoraFueraPunta, PrecioMedio, PCSPT, SST, cont);

                    if (resultado1.Count != 0)
                    {
                        var PotenciaDataSeries = new XyDataSeries<DateTime, double> { SeriesName = "Potencia" };
                        var EnergiaHoraPuntaDataSeries = new XyDataSeries<DateTime, double> { SeriesName = "Energia Hora Punta" };
                        var EnergiaHoraFueraPuntaDataSeries = new XyDataSeries<DateTime, double> { SeriesName = "Energia Hora Fuera Punta" };
                        var PrecioMedioDataSeries = new XyDataSeries<DateTime, double> { SeriesName = "Precio Medio" };
                        var PCSPTDataSeries = new XyDataSeries<DateTime, double> { SeriesName = "PCSPT" };
                        var SSTDataSeries = new XyDataSeries<DateTime, double> { SeriesName = "SST" };
                        var totalDataSeries = new XyDataSeries<DateTime, double> { SeriesName = "Total" };

                        if (Fecha.Count() == 1)
                            _x.AxisTitle = Fecha[0].ToString("MMM yyyy");

                        for (var i = 0; i < resultado1.Count / 6; i++)
                        {
                            PotenciaDataSeries.Append(Fecha[i], Potencia[i]);
                            EnergiaHoraPuntaDataSeries.Append(Fecha[i], EnergiaHoraPunta[i]);
                            EnergiaHoraFueraPuntaDataSeries.Append(Fecha[i], EnergiaHoraFueraPunta[i]);
                            PrecioMedioDataSeries.Append(Fecha[i], PrecioMedio[i]);
                            PCSPTDataSeries.Append(Fecha[i], PCSPT[i]);
                            SSTDataSeries.Append(Fecha[i], SST[i]);
                        }

                        _columnCollection = new SCIHorizontallyStackedColumnsCollection();
                        _columnCollection.Add(ChartCreator.GetColumnRenderableSeriesWithUint(PotenciaDataSeries, 0xff7cb5ec, 0xff7cb5ec));
                        _columnCollection.Add(ChartCreator.GetColumnRenderableSeriesWithUint(EnergiaHoraPuntaDataSeries, 0xFF434348, 0xFF434348));
                        _columnCollection.Add(ChartCreator.GetColumnRenderableSeriesWithUint(EnergiaHoraFueraPuntaDataSeries, 0xFF90ed7d, 0xFF90ed7d));
                        _columnCollection.Add(ChartCreator.GetColumnRenderableSeriesWithUint(PrecioMedioDataSeries, 0xFFf7a35c, 0xFFf7a35c));
                        _columnCollection.Add(ChartCreator.GetColumnRenderableSeriesWithUint(PCSPTDataSeries, 0xFF8085e9, 0xFF8085e9));
                        _columnCollection.Add(ChartCreator.GetColumnRenderableSeriesWithUint(SSTDataSeries, 0xfff15c80, 0xfff15c80));

                        var legendModifier = new SCILegendModifier();
                        legendModifier.Orientation = SCIOrientation.Vertical;
                        legendModifier.Position = SCILegendPosition.Left;
                        legendModifier.SourceMode = SCISourceMode.AllVisibleSeries;
                        legendModifier.ShowSeriesMarkers = true;
                        legendModifier.ShowCheckBoxes = true;
                        legendModifier.HolderLegendView.Frame = legendViewRect;

                        _surface.RenderableSeries.Add(_columnCollection);
                        _surface.ChartModifiers.Add(legendModifier);
                        _surface.ChartModifiers.Add(new SCICursorModifier());
                        _surface.ChartModifiers.Add(new SCIPinchZoomModifier()
                        {
                            Direction = SCIDirection2D.XDirection
                        });
                        _surface.ChartModifiers.Add(new SCIZoomExtentsModifier());
                        _surface.ChartModifiers.Add(new SCIXAxisDragModifier());

                        _view.AddSubview(legendModifier.HolderLegendView);
                    }
                    else
                    {
                        var a = alert.ShowMessage("Alerta", "No hay datos");
                        a.Clicked += A_Clicked;
                        a.Show();
                    }
                }
            }
        }

        void A_Clicked(object sender, UIButtonEventArgs e)
        {
            this.NavigationController.PopViewController(true);
        }
    }
}