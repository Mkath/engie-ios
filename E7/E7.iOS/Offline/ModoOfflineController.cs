using System;
using UIKit;
using E7.Services;
using E7.iOS.Utils;
using E7.iOS.Utils.DataBase;
using System.Collections.Generic;
using E7.Models;
using Foundation;

namespace E7.iOS
{
    public partial class ModoOfflineController : BaseController
    {
		Application _main;
        RestClient _client;
        Alert _alert;
        LoadingOverlay _loadPop;
		PickerDataModel _pickerDataMeses;
		UIPickerView _pickerViewMeses;
        BLTBLConsumoFacturado _blCF;
        BLTBLMedicionGrafica _blMG;
        BLTBLDistribucionFacturada _blDF;
        BLTBLHistoricoFacturado _blHF;
		BLTBLHistoricoPrecio _blHP;

        public ModoOfflineController (IntPtr handle) : base (handle)
        {
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);
            this.NavigationController.NavigationBar.BarTintColor = Application.Verde;
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            // Perform any additional setup after loading the view, typically from a nib.

            _main = Application.GetInstance();
            _client = new RestClient();
			_alert = new Alert();
            _loadPop = new LoadingOverlay(UIScreen.MainScreen.Bounds);
            _blCF = new BLTBLConsumoFacturado();
            _blMG = new BLTBLMedicionGrafica();
            _blDF = new BLTBLDistribucionFacturada();
            _blHF = new BLTBLHistoricoFacturado();
            _blHP = new BLTBLHistoricoPrecio();
            lblUltimaSincronizacion.Text = "Última fecha de sincronización: " + _main.UltimaSincronizacion;
            _main.ModoOff = "1";
            LoadPicker();
        }

        void _pickerDataMeses_ValueChanged(object sender, EventArgs e)
        {
            pickerTextFieldMeses.Text = _pickerDataMeses.SelectedItem;
            //pickerTextFieldMeses.ResignFirstResponder();
            _main.ModoOff = pickerTextFieldMeses.Text;
            if (int.Parse(pickerTextFieldMeses.Text) == 1)
                lblMes.Text = "mes";
            else
                lblMes.Text = "meses";
        }

        async partial void BtnSincronizar_TouchUpInside(UIButton sender)
        {
            View.Add(_loadPop);
            NavigationItem.LeftBarButtonItem.Enabled = false;
            var connection = _client.CheckConnection();
            if (connection)
            {
                var salidaCF = await _blCF.CreateTableCF();
                if (salidaCF || !salidaCF)
                {
                    SincronizarCF();
                }
            }
            else
            {
                _loadPop.Hide();
                _alert.ShowMessage("Error de conexión", "Su dispositivo se encuentra fuera de red\nRevisar la conexión a internet");
            }
        }

        async void SincronizarCF()
        {
            _loadPop.Hide();
            _loadPop = new LoadingOverlay(UIScreen.MainScreen.Bounds, "Sincronizando datos 1 de 5");
            View.Add(_loadPop);
            await _blCF.SincronizarCF(_main.clientes);
            var salidaMG = await _blMG.CreateTableMG();
            if (salidaMG)
            {
                SincronizarMG();
            }
            else
            {
                _alert.ShowMessage("Modo OffLine", "Falló el proceso de sincronización 1/5");
                _loadPop.Hide();
                NavigationItem.LeftBarButtonItem.Enabled = true;
            }
        }

        async void SincronizarMG()
        {
            _loadPop.Hide();
            _loadPop = new LoadingOverlay(UIScreen.MainScreen.Bounds, "Sincronizando datos 2 de 5");
            View.Add(_loadPop);
            await _blMG.SincronizarMG(_main.clientes, int.Parse(_main.ModoOff));
            var salidaHF = await _blHF.CreateTableHF();
            if (salidaHF)
            {
                SincronizarDF();
            }
            else
            {
                _alert.ShowMessage("Modo OffLine", "Falló el proceso de sincronización 2/5");
                _loadPop.Hide();
                NavigationItem.LeftBarButtonItem.Enabled = true;
            }
        }

        async void SincronizarDF()
        {
            _loadPop.Hide();
            _loadPop = new LoadingOverlay(UIScreen.MainScreen.Bounds, "Sincronizando datos 3 de 5");
            View.Add(_loadPop);
            await _blDF.SincronizarDF(_main.clientes, int.Parse(_main.ModoOff));
            var salidaDF = await _blDF.CreateTableDF();
            if (salidaDF)
            {
                SincronizarHF();
            }
            else
            {
                _alert.ShowMessage("Modo OffLine", "Falló el proceso de sincronización 3/5");
                _loadPop.Hide();
                NavigationItem.LeftBarButtonItem.Enabled = true;
            }
        }

        async void SincronizarHF()
        {
            _loadPop.Hide();
            _loadPop = new LoadingOverlay(UIScreen.MainScreen.Bounds, "Sincronizando datos 4 de 5");
            View.Add(_loadPop);
            await _blHF.SincronizarHF(_main.clientes, int.Parse(_main.ModoOff));
            var salidaHP = await _blHP.CreateTableHP();
            if (salidaHP)
            {
                SincronizarHP();
            }
            else
            {
                _alert.ShowMessage("Modo OffLine", "Falló el proceso de sincronización 4/5");
                _loadPop.Hide();
                NavigationItem.LeftBarButtonItem.Enabled = true;
            }
        }

        async void SincronizarHP()
        {
            _loadPop.Hide();
            _loadPop = new LoadingOverlay(UIScreen.MainScreen.Bounds, "Sincronizando datos 5 de 5");
            View.Add(_loadPop);
            await _blHP.SincronizarHP(_main.clientes, int.Parse(_main.ModoOff));
            Save();
            _loadPop.Hide();
            _alert.ShowMessage("Modo OffLine", "Culminó el proceso de sincronización");

            NavigationItem.LeftBarButtonItem.Enabled = true;
        }

        void Save()
        {
            var fecha = DateTime.Now.ToString("g");
            lblUltimaSincronizacion.Text = "Última fecha de sincronización: " + fecha;
            var plist = NSUserDefaults.StandardUserDefaults;
            plist.SetString(_main.ModoOff, "ModoOff");
            plist.SetString(fecha, "UltimaSincronizacion");
        }

        void LoadPicker()
        {
            _pickerDataMeses = new PickerDataModel();

            _pickerDataMeses.Items.Add("1");
            _pickerDataMeses.Items.Add("2");
            _pickerDataMeses.Items.Add("3");

            _pickerDataMeses.SelectedIndex = 0;
            pickerTextFieldMeses.Text = _pickerDataMeses.SelectedItem;
            //_pickerViewMeses = new UIPickerView();
            _pickerViewMeses = new TextField(pickerTextFieldMeses);
            _pickerDataMeses.ValueChanged += _pickerDataMeses_ValueChanged;
            _pickerViewMeses.Model = _pickerDataMeses;
            pickerTextFieldMeses.InputView = _pickerViewMeses;
        }
    }
}