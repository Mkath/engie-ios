﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using E7.Models;
using E7.Services;

namespace E7.iOS.Utils.DataBase
{
    public class BLTBLHistoricoFacturado
    {
        List<BEGraficosFacturacion> origen = new List<BEGraficosFacturacion>();
        DAReporte_Facturacion services = new DAReporte_Facturacion();
        TBLHistoricoFacturado entidad = new TBLHistoricoFacturado();
        DATBLHistoricoFacturado db = new DATBLHistoricoFacturado();

        public string MessageCreateHF { get; set; }
        public string MessageInsertHF { get; set; }
        public string MessageReportHF { get; set; }

        public async Task<bool> CreateTableHF()
        {
            bool salida = true;
            try
            {
                var val = await db.ValidateTableHF();
                if (val == null)
                {
                    salida = await db.CreateTableHF();
                }
                else
                {
                    salida = true;
                }
                return salida;
            }
            catch (Exception ex)
            {
                MessageCreateHF = ex.Message;
                return salida = false;
            }
        }

        public async Task<bool> SincronizarHF(List<Clientes> cliente, int count)
        {
            bool salida = true;
            try
            {
                var _clientes = cliente.ToArray();
                DateTime dateTime = DateTime.Now;
                var hasta = new DateTime(dateTime.Year, dateTime.Month, 20);
                var date = dateTime.AddMonths(-count);
                var desde = new DateTime(date.Year, date.Month, 1);
                string[] moneda = new[] { "PEN", "USD" };

                var del = db.DeleteTableHF(desde);
                for (int a = 0; a < _clientes.Count(); a++)
                {
                    if (salida == true)
                    {
                        var _codCliente = _clientes[a].CodCliente;
                        var listPuntoF = _clientes[a].PFaCliente;

                        for (int b = 0; b < listPuntoF.Count; b++)
                        {
                            if (salida == true)
                            {
                                var _idPuntoF = listPuntoF[b].ID;

                                for (int c = 0; c < moneda.Count(); c++)
                                {
                                    if (salida == true)
                                    {
                                        var _moneda = moneda[c];
                                        string _desde = desde.ToString("dd/MM/yyyy");
                                        string _hasta = hasta.ToString("dd/MM/yyyy");
                                        origen = await services.Reporte_HistoricoFacturado(_desde, _hasta, _codCliente, _idPuntoF, _moneda);
                                        var mensaje = services.ResultadoFinalRHF;
                                        if (origen == null)
                                        {
                                            salida = false;
                                        }
                                        else if (origen.Count == 0)
                                        {
                                            salida = true;
                                        }
                                        else
                                        {
                                            var final = origen.ToArray();
                                            for (int d = 0; d < origen.Count; d++)
                                            {
                                                if (salida == true)
                                                {
                                                    entidad.CodCliente = _codCliente;
                                                    entidad.CodPuntoFacturacion = _idPuntoF;
                                                    entidad.Moneda = _moneda;
                                                    entidad.Fecha = final[d].Fecha;
                                                    entidad.Concepto = final[d].Concepto;
                                                    entidad.Consumo = final[d].Consumo;
                                                    salida = await db.InsertTableHF(entidad);
                                                }
                                                else
                                                {
                                                    throw new System.InvalidOperationException();
                                                }
                                            }
                                        }

                                    }
                                    else
                                    {
                                        throw new System.InvalidOperationException();
                                    }
                                }
                            }
                            else
                            {
                                throw new System.InvalidOperationException();
                            }
                        }
                    }
                    else
                    {
                        throw new System.InvalidOperationException();
                    }
                }
                return salida;
            }
            catch (Exception ex)
            {
                MessageInsertHF = ex.Message;
                return salida;
            }
        }

        public async Task<List<TBLHistoricoFacturado>> Reporte_HistoricoFacturado(TBLHistoricoFacturado hf, DateTime desde, DateTime hasta)
        {
            List<TBLHistoricoFacturado> list = new List<TBLHistoricoFacturado>();
            try
            {
                list = await db.SelectTableHF(hf, desde, hasta);
                return list;
            }
            catch (Exception ex)
            {
                MessageReportHF = ex.Message;
                return null;
            }
        }

        public async Task<List<TBLHistoricoFacturado>> Reporte_HistoricoFacturado(TBLHistoricoFacturado hf)
        {
            List<TBLHistoricoFacturado> list = new List<TBLHistoricoFacturado>();
            try
            {
                list = await db.SelectTableHF(hf);
                return list;
            }
            catch (Exception ex)
            {
                MessageReportHF = ex.Message;
                return null;
            }
        }
    }
}
