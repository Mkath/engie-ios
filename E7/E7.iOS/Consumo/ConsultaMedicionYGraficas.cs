using Foundation;
using System;
using UIKit;
using CoreGraphics;
using E7.Services;
using E7.iOS.Offline;

namespace E7.iOS
{
    public partial class ConsultaMedicionYGraficas : UITabBarController
    {
        RestClient _client;
        public ConsultaMedicionYGraficas (IntPtr handle) : base (handle)
        {
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);

            _client = new RestClient();
            UILabel title = new UILabel() { Text = "Medición y Gráficas de Potencia / Energía", TextColor = UIColor.White };

            NavigationController.NavigationBar.BarTintColor = Application.Azul;
            NavigationController.NavigationBar.Translucent = false;

            NavigationItem.TitleView = title;
            this.TabBar.TintColor = UIColor.White;
            this.TabBar.BarTintColor = Application.Azul;

            var online = _client.CheckConnection();
            if (!online)
            {
                var navBar = NavigationController.NavigationBar;
                NavigationItem.SetRightBarButtonItem(
                    new UIBarButtonItem(OfflineLabel.Label(navBar.Bounds.Width, navBar.Bounds.Height, false)),
                    false);
            }
        }
    }
}