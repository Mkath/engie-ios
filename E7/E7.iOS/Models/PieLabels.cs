﻿using System;
using UIKit;

namespace E7.iOS.Models
{
    public class PieLabels
    {
        public string Color { get; set; }
        public string Name { get; set; }
        public string Porcentage { get; set; }
    }
}
