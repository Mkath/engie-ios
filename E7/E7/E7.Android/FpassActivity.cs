﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace E7.Droid
{
    [Activity(Label = "FpassActivity", Theme = "@android:style/Theme.NoTitleBar")]
    public class FpassActivity : Activity
    {
        Button btn3, btn4;
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            // Create your application here

            SetContentView(layoutResID: Resource.Layout.fpass);

            btn3 = FindViewById<Button>(Resource.Id.button3);
            btn4 = FindViewById<Button>(Resource.Id.button4);

            btn3.Click += (sender, e) => ncode();
            btn3.Click += (sender, e) => login();

            void ncode()
            {
                StartActivity(typeof(ncodeActivity));
            }

            void login()
            {
                StartActivity(typeof(LoginActivity));
            }

        }
    }
}