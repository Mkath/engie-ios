﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace E7.Droid
{
    public class BEMedicionGrafica
    {
        public string Item { get; set; }
        public string FechayHora { get; set; }
        public string EnergiaActiva { get; set; }
        public string Potencia { get; set; }
        public string EnergiaReactiva { get; set; }
    }
}