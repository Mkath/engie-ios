﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace E7.Models
{
    public class GraficoHistoricos
    {
        public string Concepto { get; set; }
        public string Anio { get; set; }
        public string Mes { get; set; }
        public string Consumo { get; set; }
    }
}