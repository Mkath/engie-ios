﻿using System.Collections.Generic;

namespace E7.Models
{
    public class BEUsuarioLoginE7
    {
        public string Usuario { get; set; }
        public string UsuarioInterno { get; set; }
        public string IDUsuario { get; set; }
        public string NombreCompleto { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public string MensajeError { get; set; }
        public string ValidarOperacion { get; set; }
        public List<Clientes> Usuario_cliente { get; set; }
    }
    public class Clientes
    {
        public string CodCliente { get; set; }
        public string NomCliente { get; set; }
        public List<Medidor> MedCliente { get; set; }
        public List<PuntoFacturacion> PFaCliente { get; set; }
    }
    public class Medidor
    {
        public string CodCliente { get; set; }
        public string Codigo { get; set; }
        public string Descripcion { get; set; }
    }
    public class PuntoFacturacion
    {
        public string CodCliente { get; set; }
        public string ID { get; set; }
        public string Descripcion { get; set; }
    }
}
