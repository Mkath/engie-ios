﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using SciChart.Charting.Visuals;
using SciChart.Charting.Visuals.Axes;
using SciChart.Data.Model;
using SciChart.Charting.Model.DataSeries;
using SciChart.Charting.Visuals.RenderableSeries;
using SciChart.Drawing.Common;
using SciChart.Charting.Visuals.PointMarkers;
using SciChart.Charting.Modifiers;
using SciChart.Core.Framework;
using Android.Graphics;
using SciChart.Charting.Numerics.LabelProviders;
using Java.Lang;
using SciChart.Core.Utility;
using V7Toolbar = Android.Support.V7.Widget.Toolbar;
using Android.Support.V7.App;
using Android.Support.V4.Widget;
using Android.Support.Design.Widget;

namespace E7.Droid
{
    [Activity(Label = "graf", Theme = "@style/Theme.DesignDemo")]
    public class graf : AppCompatActivity
    {
        DrawerLayout drawerLayout;
        NavigationView navigationView;
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.graf1);
            var toolbar = FindViewById<V7Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            SupportActionBar.SetDisplayShowTitleEnabled(false);
            SupportActionBar.SetHomeButtonEnabled(true);
            SupportActionBar.SetHomeAsUpIndicator(Resource.Drawable.menu);
            drawerLayout = FindViewById<DrawerLayout>(Resource.Id.drawer_layout);
            navigationView = FindViewById<NavigationView>(Resource.Id.nav_view);


            // Get our chart from the layout resource,
            var chart = FindViewById<SciChartSurface>(Resource.Id.Chart);
            int themeid = Resource.Style.SciChart_Bright_Spark;
            chart.Theme = themeid;


            string _codCliente = Intent.GetStringExtra("_codCliente") ?? "Data not available";
            string _anio = Intent.GetStringExtra("_anio") ?? "Data not available";
            string _codPuntoF = Intent.GetStringExtra("_codPuntoF") ?? "Data not available";

            //Llamar data
            List<BEConsumoFacturado> resultado;
            COReporte_Consumo conexion = new COReporte_Consumo();

            resultado = conexion.Reporte_ConsumoFacturado(_anio, _codCliente, _codPuntoF);

            var energiaActiva = resultado.Select(x => (Convert.ToDouble(x.EnergiaActiva))).ToList();
            var Potencia = resultado.Select(x => (Convert.ToDouble(x.Potencia))).ToList();
            var mes = resultado.Select(x => x.Mes).ToList();
            double[] _PuntoX1 = energiaActiva.ToArray();
            double[] _PuntoX2 = Potencia.ToArray();
            string[] _PuntoY = mes.ToArray();

            // Create a numeric X axis
            var xAxis = new NumericAxis(this)
            {
                AxisTitle = "Meses",
                //LabelProvider = new MesesLabelProvider(_PuntoY),
            };

            // Create a numeric Y axis
            var yAxis = new NumericAxis(this)
            {
                AxisTitle = "Energía Activa",
            };                        

            var ds1 = new XyDataSeries<double, double> { SeriesName = "Energia" };

            for (var i = 0; i < _PuntoX1.Length; i++)
            {
                ds1.Append(i, _PuntoX1[i]);

            }
            //
            // Add xAxis to the XAxes collection of the chart
            chart.XAxes.Add(xAxis);

            // Add yAxis to the YAxes collection of the chart
            chart.YAxes.Add(yAxis);

            // Create XyDataSeries to host data for our chart
            var lineData = new XyDataSeries<double, double>();
            var scatterData = new XyDataSeries<double, double>();

            //// Append data which should be drawn
            //for (var i = 0; i < 12; i++)
            //{
            //    lineData.Append(i, Math.Sin(i * 0.1));
            //}

            // Create line series with data appended into lineData
            var lineSeries = new FastLineRenderableSeries()
            {
                DataSeries = ds1,
                StrokeStyle = new SolidPenStyle(Color.DarkBlue, 3)
            };

            // Create scatter series with data appended into scatterData
            //var scatterSeries = new XyScatterRenderableSeries()
            //{
            //    DataSeries = scatterData,
            //    PointMarker = new EllipsePointMarker()
            //    {
            //        Width = 10,
            //        Height = 10,
            //        StrokeStyle = new SolidPenStyle(Color.Green, 2),
            //        FillStyle = new SolidBrushStyle(Color.LightBlue)
            //    }
            //};

            // Add the renderable series to the RenderableSeries collection of the chart
            chart.RenderableSeries.Add(lineSeries);
            //chart.RenderableSeries.Add(scatterSeries);


            //no mirar

            // Create interactivity modifiers
            var pinchZoomModifier = new PinchZoomModifier();
            pinchZoomModifier.SetReceiveHandledEvents(true);

            var zoomPanModifier = new ZoomPanModifier();
            zoomPanModifier.SetReceiveHandledEvents(true);

            var zoomExtentsModifier = new ZoomExtentsModifier();
            zoomExtentsModifier.SetReceiveHandledEvents(true);

            var yAxisDragModifier = new YAxisDragModifier();
            yAxisDragModifier.SetReceiveHandledEvents(true);

            // Create modifier group from declared modifiers
            var modifiers = new ModifierGroup(pinchZoomModifier, zoomPanModifier, zoomExtentsModifier, yAxisDragModifier);

            // Add the interactions to the ChartModifiers collection of the chart
            chart.ChartModifiers.Add(modifiers);

        }

        //Menu
        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Android.Resource.Id.Home:
                    drawerLayout.OpenDrawer(Android.Support.V4.View.GravityCompat.Start);
                    return true;
            }
            return base.OnOptionsItemSelected(item);
        }
    }
    public class MesesLabelProvider : FormatterLabelProviderBase
    {
        public MesesLabelProvider(string[] mes) : base(typeof(NumericAxis).ToClass(), new MesesLabelFormatter())
        {
            MesesLabelFormatter c = new MesesLabelFormatter();
            c.List(mes);
        }
    }
    public class MesesLabelFormatter : Java.Lang.Object, ILabelFormatter
    {
        private string[] _xLabels;


        public void List(string[] mes)
        {
            if (mes == null)
            {
                _xLabels = new string[] { "Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic" };
            }
            else
            {
                _xLabels = mes;
            }
        }
        public void Update(Java.Lang.Object axis)
        {

        }
        public ICharSequence FormatLabelFormatted(Java.Lang.IComparable dataValue)
        {
            var i = (int)ComparableUtil.ToDouble(dataValue);
            var result = "";
            if (i >= 0 && i < 4)
            {
                result = _xLabels[i];
            }
            return new Java.Lang.String(result);
        }

        public ICharSequence FormatCursorLabelFormatted(Java.Lang.IComparable dataValue)
        {
            var i = (int)ComparableUtil.ToDouble(dataValue);
            string result;
            if (i >= 0 && i < 4)
            {
                result = _xLabels[i];
            }
            else if (i < 0)
            {
                result = _xLabels[0];
            }
            else
            {
                result = _xLabels[3];
            }
            return new Java.Lang.String(result);
        }

    }
}

