﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using E7.Models;
using E7.Services;

namespace E7.iOS.Utils.DataBase
{
    public class BLTBLConsumoFacturado
    {
        List<BEConsumoFacturado> origen = new List<BEConsumoFacturado>();
        DAReporte_Consumo services = new DAReporte_Consumo();
        TBLConsumoFacturado entidad = new TBLConsumoFacturado();
        DATBLConsumoFacturado db = new DATBLConsumoFacturado();

        public string MessageCreateCF { get; set; }
        public string MessageInsertCF { get; set; }
        public string MessageReportCF { get; set; }

        public async Task<bool> CreateTableCF()
        {
            bool salida = true;
            try
            {
                var val = await db.ValidateTableCF();
                if (val == null)
                {
                    salida = await db.CreateTableCF();
                }
                else
                {
                    salida = true;
                }
                return salida;
            }
            catch (Exception ex)
            {
                MessageCreateCF = ex.Message;
                return salida = false;
            }
        }

        public async Task<bool> SincronizarCF(List<Clientes> cliente)
        {
            bool salida = true;
            try
            {
                var _clientes = cliente.ToArray();
                DateTime dateTime = DateTime.Now;
                string anio = dateTime.Year.ToString();
                var date = new DateTime(dateTime.Year, 1, 1);
                var del = db.DeleteTableDF(date);
                for (int a = 0; a < _clientes.Count(); a++)
                {
                    if (salida == true)
                    {
                        var codCliente = _clientes[a].CodCliente;
                        var listPuntoF = _clientes[a].PFaCliente;

                        for (int b = 0; b < listPuntoF.Count; b++)
                        {
                            if (salida == true)
                            {
                                var idPuntoF = listPuntoF[b].ID;

                                origen = await services.Reporte_ConsumoFacturado(anio, codCliente, idPuntoF);
                                var mensaje = services.ResultadoFinalRCF;
                                if (origen == null)
                                {
                                    salida = false;
                                }
                                else if (origen.Count == 0)
                                {
                                    salida = true;
                                }
                                else
                                {
                                    var final = origen.ToArray();
                                    for (int d = 0; d < origen.Count; d++)
                                    {
                                        if (salida == true)
                                        {
                                            entidad.CodCliente = codCliente;
                                            entidad.CodPuntoFacturacion = idPuntoF;
                                            entidad.Anio = anio;
                                            entidad.Fecha = final[d].Fecha;
                                            entidad.EnergiaActiva = final[d].EnergiaActiva;
                                            entidad.Potencia = final[d].Potencia;
                                            salida = await db.InsertTableDF(entidad);
                                        }
                                        else
                                        {
                                            throw new System.InvalidOperationException();
                                        }
                                    }
                                }
                            }
                            else
                            {
                                throw new System.InvalidOperationException();
                            }
                        }
                    }
                    else
                    {
                        throw new System.InvalidOperationException();
                    }
                }
                return salida;
            }
            catch (Exception ex)
            {
                MessageInsertCF = ex.Message;
                return salida;
            }
        }

        public async Task<List<TBLConsumoFacturado>> Reporte_ConsumoFacturado(TBLConsumoFacturado cf)
        {
            List<TBLConsumoFacturado> list = new List<TBLConsumoFacturado>();
            try
            {
                list = await db.SelectTableCF(cf);
                return list;
            }
            catch (Exception ex)
            {
                MessageReportCF = ex.Message;
                return null;
            }
        }
        public async Task<List<TBLConsumoFacturado>> Reporte_ConsumoFacturadoTodo(TBLConsumoFacturado cf)
        {
            List<TBLConsumoFacturado> list = new List<TBLConsumoFacturado>();
            try
            {
                list = await db.SelectTableTodoCF(cf);
                return list;
            }
            catch (Exception ex)
            {
                MessageReportCF = ex.Message;
                return null;
            }
        }
    }
}
