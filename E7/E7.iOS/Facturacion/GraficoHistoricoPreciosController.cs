﻿using System;
using E7.iOS.Utils;
using E7.Models;
using UIKit;

namespace E7.iOS
{
    public partial class GraficoHistoricoPreciosController : UIViewController
    {
        PickerDataModel _pickerDataClientes, _pickerDataFacturacion, _pickerDataDesdeMes, _pickerDataDesdeAnio;
        PickerDataModel _pickerDataHastaMes, _pickerDataHastaAnio, _pickerDataMoneda;
        TextField _pickerViewClientes, _pickerViewFacturacion, _pickerViewDesdeMes, _pickerViewDesdeAnio;
        TextField _pickerViewHastaMes, _pickerViewHastaAnio, _pickerViewMoneda;
        Application _main;
        Alert _alerta;
        bool _showMessage;

        public GraficoHistoricoPreciosController() : base("GraficoHistoricoPreciosController", null)
        {
        }

        public GraficoHistoricoPreciosController(IntPtr b) : base(b) { }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            // Perform any additional setup after loading the view, typically from a nib.

            if (this.View.Frame.Height <= 480)
            {
                NSLayoutConstraint.Create(txtTitle, NSLayoutAttribute.Top,
                                            NSLayoutRelation.Equal, this.View, NSLayoutAttribute.Top,
                                            1, 82).Active = true;
                NSLayoutConstraint.Create(stackPrincipalBg, NSLayoutAttribute.Top,
                                          NSLayoutRelation.Equal, this.View, NSLayoutAttribute.Top,
                                          1, 115).Active = true;
                NSLayoutConstraint.Create(stackPrincipal, NSLayoutAttribute.Top, 
                                            NSLayoutRelation.Equal, this.View, NSLayoutAttribute.Top, 
                                            1, 125).Active = true;
                lblClientes.Font = lblClientes.Font.WithSize(10);
                lblFacturacion.Font = lblClientes.Font.WithSize(10);
                lblDesde.Font = lblClientes.Font.WithSize(10);
                lblHasta.Font = lblClientes.Font.WithSize(10);
                lblMoneda.Font = lblClientes.Font.WithSize(10);
            }
            else if (this.View.Frame.Height <= 568)
            {
                stackPrincipal.Spacing = 0;
            }

			_alerta = new Alert();
            _main = Application.GetInstance();
            _main.graficoHistoricoPrecios = new GraficoHistoricoFacturadoModel();
            GetClientes();
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);
            NavigationController.NavigationBar.Translucent = true;
        }

        void _pickerDataClientes_ValueChanged(object sender, EventArgs e)
        {
            pickerTextFieldClientes.Text = _pickerDataClientes.SelectedItem;
            _main.graficoHistoricoPrecios.Cliente = _pickerDataClientes.SelectedItem;

            _pickerDataFacturacion.Items.Clear();
            foreach (var x in _main.clientes)
            {
                if (x.NomCliente == _pickerDataClientes.SelectedItem)
                {
                    _main.graficoHistoricoPrecios.ClienteID = int.Parse(x.CodCliente);
                    _main.graficoHistoricoPrecios.PuntoFID = 0;
                    foreach (var y in x.PFaCliente)
                    {
                        _pickerDataFacturacion.Items.Add(y.Descripcion);
                    }
                }
            }
            _pickerDataFacturacion.SelectedIndex = 0;
            pickerTextFieldFacturacion.Text = _pickerDataFacturacion.SelectedItem;
        }

        void _pickerDataFacturacion_ValueChanged(object sender, EventArgs e)
        {
            pickerTextFieldFacturacion.Text = _pickerDataFacturacion.SelectedItem;
            _main.graficoHistoricoPrecios.PuntoF = _pickerDataFacturacion.SelectedItem;

            foreach (var x in _main.clientes)
                foreach (var y in x.PFaCliente)
                    if (y.Descripcion == _pickerDataFacturacion.SelectedItem)
                        _main.graficoHistoricoPrecios.PuntoFID = int.Parse(y.ID);
        }

        void _pickerDataDesdeMes_ValueChanged(object sender, EventArgs e)
        {
            var value = pickerTextFieldDesdeMes.Text;
            pickerTextFieldDesdeMes.Text = _pickerDataDesdeMes.SelectedItem;

            if (!DatesHelper.IsDateValid(
                string.Format("01/{0}/{1}", DatesHelper.GetMonthKey(pickerTextFieldDesdeMes.Text),
                              pickerTextFieldDesdeAnio.Text),
                string.Format("01/{0}/{1}", DatesHelper.GetMonthKey(pickerTextFieldHastaMes.Text),
                              pickerTextFieldHastaAnio.Text)))
            {
                pickerTextFieldDesdeMes.Text = value;
                _showMessage = true;
            }

            SaveDates();
        }

        void _pickerDataDesdeAnio_ValueChanged(object sender, EventArgs e)
        {
            var value = pickerTextFieldDesdeAnio.Text;
            pickerTextFieldDesdeAnio.Text = _pickerDataDesdeAnio.SelectedItem;

            if (!DatesHelper.IsDateValid(
                string.Format("01/{0}/{1}", 
                              DatesHelper.GetMonthKey(pickerTextFieldDesdeMes.Text), 
                              pickerTextFieldDesdeAnio.Text), 
                string.Format("01/{0}/{1}", 
                              DatesHelper.GetMonthKey(pickerTextFieldHastaMes.Text), 
                              pickerTextFieldHastaAnio.Text)))
            {
                pickerTextFieldDesdeAnio.Text = value;
                _showMessage = true;
            }

            SaveDates();
        }

        void _pickerDataHastaMes_ValueChanged(object sender, EventArgs e)
        {
            var value = pickerTextFieldHastaMes.Text;
            pickerTextFieldHastaMes.Text = _pickerDataHastaMes.SelectedItem;

            if (!DatesHelper.IsDateValid(
                string.Format("01/{0}/{1}", DatesHelper.GetMonthKey(pickerTextFieldDesdeMes.Text), 
                              pickerTextFieldDesdeAnio.Text), 
                string.Format("01/{0}/{1}", DatesHelper.GetMonthKey(pickerTextFieldHastaMes.Text), 
                              pickerTextFieldHastaAnio.Text)))
            {
                pickerTextFieldHastaMes.Text = value;
                _showMessage = true;
            }

            SaveDates();
        }

        void _pickerDataHastaAnio_ValueChanged(object sender, EventArgs e)
        {
            var value = pickerTextFieldHastaAnio.Text;
            pickerTextFieldHastaAnio.Text = _pickerDataHastaAnio.SelectedItem;

            var desde = string.Format("01/{0}/{1}", DatesHelper.GetMonthKey(pickerTextFieldDesdeMes.Text), pickerTextFieldDesdeAnio.Text);
            var hasta = string.Format("01/{0}/{1}", DatesHelper.GetMonthKey(pickerTextFieldHastaMes.Text), pickerTextFieldHastaAnio.Text);

            if (!DatesHelper.IsDateValid(desde, hasta))
            {
                pickerTextFieldHastaAnio.Text = value;
                _showMessage = true;
            }

            SaveDates();
        }

        void _pickerDataMoneda_ValueChanged(object sender, EventArgs e)
        {
            pickerTextFieldMoneda.Text = _pickerDataMoneda.SelectedItem;
            _main.graficoHistoricoPrecios.Moneda = _pickerDataMoneda.SelectedItem;
        }

        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
            // Release any cached data, images, etc that aren't in use.
        }

        partial void UIButton35609_TouchUpInside(UIButton sender)
        {
            NavigationItem.BackBarButtonItem = new UIBarButtonItem("", UIBarButtonItemStyle.Plain, null);
        }

        public override bool ShouldPerformSegue(string segueIdentifier, Foundation.NSObject sender)
        {
            //if (_pickerDataFacturacion.SelectedIndex == 0) {
            //    var a = _alerta.ShowMessage("Error", "Seleccione un punto de facturación");
            //    a.Show();
            //    return false;
            //}
            if (_pickerDataFacturacion.SelectedItem == "SELECCIONE") {
                var a = _alerta.ShowMessage("Error", "Seleccione un punto de facturación");
                a.Show();
                return false;
            }
            return true;
        }

        void GetClientes()
        {
            var main = Application.GetInstance();
            var clientes = main.clientes;

            _pickerDataClientes = new PickerDataModel();
            _pickerDataFacturacion = new PickerDataModel();
            _pickerDataDesdeMes = new PickerDataModel();
            _pickerDataDesdeAnio = new PickerDataModel();
            _pickerDataHastaMes = new PickerDataModel();
            _pickerDataHastaAnio = new PickerDataModel();
            _pickerDataMoneda = new PickerDataModel();

            foreach (var c in clientes)
                _pickerDataClientes.Items.Add(c.NomCliente);
            
            if (clientes[0] != null)
                foreach (var x in clientes[0].PFaCliente)
                    if (x.ID == "0")
                        _pickerDataFacturacion.Items.Add("SELECCIONE");
                    else
						_pickerDataFacturacion.Items.Add(x.Descripcion);

            _main.graficoHistoricoPrecios.ClienteID = int.Parse((clientes[0] != null) ? clientes[0].CodCliente : "0");
            _main.graficoHistoricoPrecios.PuntoFID = int.Parse((clientes[0].PFaCliente[0] != null) ? clientes[0].PFaCliente[0].ID : "0");
            _main.graficoHistoricoPrecios.Moneda = "USD";

            foreach (var x in Application.Meses)
                _pickerDataDesdeMes.Items.Add(x);
            
            foreach (var x in Application.Anios)
                _pickerDataDesdeAnio.Items.Add(x);
            
            foreach (var x in Application.Meses)
                _pickerDataHastaMes.Items.Add(x);
            
            foreach (var x in Application.Anios)
                _pickerDataHastaAnio.Items.Add(x);
            
            foreach (var x in Application.Monedas)
                _pickerDataMoneda.Items.Add(x);

            _pickerViewClientes = new TextField(pickerTextFieldClientes);
			_pickerDataClientes.ValueChanged += _pickerDataClientes_ValueChanged;
			_pickerViewClientes.Model = _pickerDataClientes;
            pickerTextFieldClientes.Text = _pickerDataClientes.SelectedItem;
            pickerTextFieldClientes.InputView = _pickerViewClientes;

            _pickerViewFacturacion = new TextField(pickerTextFieldFacturacion);
			_pickerDataFacturacion.ValueChanged += _pickerDataFacturacion_ValueChanged;
			_pickerViewFacturacion.Model = _pickerDataFacturacion;
            pickerTextFieldFacturacion.Text = _pickerDataFacturacion.SelectedItem;
            pickerTextFieldFacturacion.InputView = _pickerViewFacturacion;

            _pickerViewDesdeMes = new TextField(pickerTextFieldDesdeMes);
            _pickerViewDesdeMes.DoneClicked += DoneClick;
			_pickerDataDesdeMes.ValueChanged += _pickerDataDesdeMes_ValueChanged;
			_pickerViewDesdeMes.Model = _pickerDataDesdeMes;
            pickerTextFieldDesdeMes.Text = _pickerDataDesdeMes.SelectedItem;
            pickerTextFieldDesdeMes.InputView = _pickerViewDesdeMes;

            _pickerViewDesdeAnio = new TextField(pickerTextFieldDesdeAnio);
            _pickerViewDesdeAnio.DoneClicked += DoneClick;
			_pickerDataDesdeAnio.ValueChanged += _pickerDataDesdeAnio_ValueChanged;
			_pickerViewDesdeAnio.Model = _pickerDataDesdeAnio;
            pickerTextFieldDesdeAnio.Text = _pickerDataDesdeAnio.SelectedItem;
            pickerTextFieldDesdeAnio.InputView = _pickerViewDesdeAnio;

            _pickerViewHastaMes = new TextField(pickerTextFieldHastaMes);
            _pickerViewHastaMes.DoneClicked += DoneClick;
			_pickerDataHastaMes.ValueChanged += _pickerDataHastaMes_ValueChanged;
			_pickerViewHastaMes.Model = _pickerDataHastaMes;
            pickerTextFieldHastaMes.Text = _pickerDataHastaMes.SelectedItem;
            pickerTextFieldHastaMes.InputView = _pickerViewHastaMes;

            _pickerViewHastaAnio = new TextField(pickerTextFieldHastaAnio);
            _pickerViewHastaAnio.DoneClicked += DoneClick;
			_pickerDataHastaAnio.ValueChanged += _pickerDataHastaAnio_ValueChanged;
			_pickerViewHastaAnio.Model = _pickerDataHastaAnio;
            pickerTextFieldHastaAnio.Text = _pickerDataHastaAnio.SelectedItem;
            pickerTextFieldHastaAnio.InputView = _pickerViewHastaAnio;

            _pickerViewMoneda = new TextField(pickerTextFieldMoneda);
			_pickerDataMoneda.ValueChanged += _pickerDataMoneda_ValueChanged;
			_pickerViewMoneda.Model = _pickerDataMoneda;
            pickerTextFieldMoneda.Text = _pickerDataMoneda.SelectedItem;
            pickerTextFieldMoneda.InputView = _pickerViewMoneda;

            SetDates();
        }

        void SetDates()
        {
            var dia = DateTime.Now.Day.ToString("d2");
            var mes = DateTime.Now.Month.ToString("d2");
            var anio = DateTime.Now.Year.ToString();

            pickerTextFieldDesdeMes.Text = DatesHelper.GetMonthName(mes);
            pickerTextFieldDesdeAnio.Text = anio;
            pickerTextFieldHastaMes.Text = DatesHelper.GetMonthName(mes);
            pickerTextFieldHastaAnio.Text = anio;

            _pickerDataDesdeMes.SelectedIndex = int.Parse(mes) - 1;
            _pickerViewDesdeMes.Select(_pickerDataDesdeMes.SelectedIndex, 0, true);
            _pickerDataHastaMes.SelectedIndex = int.Parse(mes) - 1;
            _pickerViewHastaMes.Select(_pickerDataHastaMes.SelectedIndex, 0, true);

            _main.graficoHistoricoPrecios.Desde = string.Format("01/{0}/{1}", mes, anio);
            _main.graficoHistoricoPrecios.Hasta = string.Format("01/{0}/{1}", mes, anio);
        }

        void SaveDates()
        {
            _main.graficoHistoricoPrecios.Desde = string.Format("01/{0}/{1}", DatesHelper.GetMonthKey(pickerTextFieldDesdeMes.Text), pickerTextFieldDesdeAnio.Text);
            _main.graficoHistoricoPrecios.Hasta = string.Format("01/{0}/{1}", DatesHelper.GetMonthKey(pickerTextFieldHastaMes.Text), pickerTextFieldHastaAnio.Text);
        }

        void DoneClick(object sender, EventArgs e)
        {
            if (_showMessage)
            {
                _showMessage = false;
                var a = _alerta.ShowMessage("La fecha no es válida", "Por favor seleccione una fecha válida");
                a.Show();
            }
        }
    }
}
