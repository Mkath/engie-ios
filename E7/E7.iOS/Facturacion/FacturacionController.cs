﻿using System;

using UIKit;

namespace E7.iOS
{
    public partial class FacturacionController : BaseController//UIViewController
    {
        //public FacturacionController() : base("FacturacionController", null)
        //{
        //}

        public FacturacionController(IntPtr b) : base (b) { }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            // Perform any additional setup after loading the view, typically from a nib.
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);
            NavigationController.NavigationBar.BarTintColor = Application.Rojo;
        }

        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
            // Release any cached data, images, etc that aren't in use.
        }

        partial void UIButton28196_TouchUpInside(UIButton sender)
        {
            NavigationItem.BackBarButtonItem = new UIBarButtonItem("", UIBarButtonItemStyle.Plain, null);
        }

        partial void UIButton28198_TouchUpInside(UIButton sender)
        {
            NavigationItem.BackBarButtonItem = new UIBarButtonItem("", UIBarButtonItemStyle.Plain, null);
        }

        partial void UIButton28197_TouchUpInside(UIButton sender)
        {
            NavigationItem.BackBarButtonItem = new UIBarButtonItem("", UIBarButtonItemStyle.Plain, null);
        }
    }
}

