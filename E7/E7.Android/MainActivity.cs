﻿using System;

using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using SciChart.Charting.Visuals;
using SciChart.Charting.Visuals.Axes;
using SciChart.Data.Model;
using SciChart.Charting.Model.DataSeries;
using SciChart.Charting.Visuals.RenderableSeries;
using SciChart.Drawing.Common;
using SciChart.Charting.Visuals.PointMarkers;
using Android.Graphics;
using SciChart.Charting.Modifiers;
using SciChart.Core.Framework;
using E7.Droid;


namespace E7.Droid
{
	[Activity (Label = "E7.Android", MainLauncher = true, Icon = "@drawable/icon", Theme = "@style/Theme.DesignDemo")]
	public class MainActivity : Activity
	{
	

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

            //Inicio de Sesión
            var intent = new Intent(this, typeof(SplashScreen));
            StartActivity(intent);
            //var intent = new Intent(this, typeof(fac3));
            //StartActivity(intent);

            // Set our view from the "main" layout resource
            //SetContentView(Resource.Layout.fpass);

            // Get our button from the layout resource,
            // and attach an event to it

            //    //Get our chart from the layout resource,
            //    var chart = FindViewById<SciChartSurface>(Resource.Id.Chart);

            //    //// Create a numeric X axis
            //    var xAxis = new NumericAxis(this) { AxisTitle = "Meses" };

            //    //// Create a numeric Y axis
            //    var yAxis = new NumericAxis(this)
            //    {
            //        AxisTitle = "Value",
            //        VisibleRange = new DoubleRange(-1, 1)
            //    };

            //    //// Add xAxis to the XAxes collection of the chart
            //    chart.XAxes.Add(xAxis);

            //    //// Add yAxis to the YAxes collection of the chart
            //    chart.YAxes.Add(yAxis);


            //    // Create XyDataSeries to host data for our chart
            //    var lineData = new XyDataSeries<double, double>() { SeriesName = "Sin(x)" };
            //    var scatterData = new XyDataSeries<double, double>() { SeriesName = "Cos(x)" };

            //    // Append data which should be drawn
            //    for (var i = 0; i < 1000; i++)
            //    {
            //        lineData.Append(i, Math.Sin(i * 0.1));
            //        scatterData.Append(i, Math.Cos(i * 0.1));
            //    }


            //    // Create line series with data appended into lineData
            //    var lineSeries = new FastLineRenderableSeries()
            //    {
            //        DataSeries = lineData,
            //        StrokeStyle = new SolidPenStyle(Color.LightBlue, 2)
            //    };

            //    // Create scatter series with data appended into scatterData
            //    var scatterSeries = new XyScatterRenderableSeries()
            //    {
            //        DataSeries = scatterData,
            //        PointMarker = new EllipsePointMarker()
            //        {
            //            Width = 10,
            //            Height = 10,
            //            StrokeStyle = new SolidPenStyle(Color.Green, 2),
            //            FillStyle = new SolidBrushStyle(Color.LightBlue)
            //        }
            //    };

            //    // Add the renderable series to the RenderableSeries collection of the chart
            //    chart.RenderableSeries.Add(lineSeries);
            //    chart.RenderableSeries.Add(scatterSeries);


            //    var pinchZoomModifier = new PinchZoomModifier();
            //    pinchZoomModifier.SetReceiveHandledEvents(true);

            //    var zoomPanModifier = new ZoomPanModifier();
            //    zoomPanModifier.SetReceiveHandledEvents(true);

            //    var zoomExtentsModifier = new ZoomExtentsModifier();
            //    zoomExtentsModifier.SetReceiveHandledEvents(true);

            //    var yAxisDragModifier = new YAxisDragModifier();
            //    yAxisDragModifier.SetReceiveHandledEvents(true);

            //    // Create modifier group from declared modifiers
            //    var modifiers = new ModifierGroup(pinchZoomModifier, zoomPanModifier, zoomExtentsModifier, yAxisDragModifier);

            //    // Add the interactions to the ChartModifiers collection of the chart
            //    chart.ChartModifiers.Add(modifiers);

            //    // Create and configure legend
            //    var legendModifier = new LegendModifier(this);
            //    legendModifier.SetLegendPosition(GravityFlags.Bottom | GravityFlags.CenterHorizontal, 10);
            //    legendModifier.SetOrientation(SciChart.Core.Framework.Orientation.Horizontal);

            //    // Create RolloverModifier to show tooltips
            //    var rolloverModifier = new RolloverModifier();
            //    rolloverModifier.SetReceiveHandledEvents(true);
        }
    }
}


