using Foundation;
using System;
using UIKit;
using E7.iOS.Utils;
using E7.Services;

namespace E7.iOS
{
    public partial class CodePasswordController : KeyboardBaseController
    {
        int _intentosRestantes = 3;

        public CodePasswordController (IntPtr handle) : base (handle)
        {
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);

            tfCodigo.BackgroundColor = UIColor.FromRGBA(0, 0, 0, 256);
            tfCodigo.TextColor = UIColor.White;
            tfCodigo.Layer.BorderColor = UIColor.White.CGColor;
            tfCodigo.Layer.BorderWidth = 1.5f;
            tfCodigo.Layer.CornerRadius = 13.0f;
            tfCodigo.AttributedPlaceholder = new NSAttributedString("Código", null, UIColor.White);

            btnContinuar.BackgroundColor = UIColor.FromRGB(2, 73, 125);
            btnContinuar.Layer.CornerRadius = 18.0f;
            btnContinuar.Layer.ShadowOpacity = 0.8f;
            btnContinuar.Layer.ShadowOffset = new CoreGraphics.CGSize(0.0f, 3.0f);
            btnContinuar.TouchDown += BtnContinuar_TouchDown;

            btnCancelar.BackgroundColor = UIColor.FromRGB(2, 73, 125);
            btnCancelar.Layer.CornerRadius = 18.0f;
            btnCancelar.Layer.ShadowOpacity = 0.8f;
            btnCancelar.Layer.ShadowOffset = new CoreGraphics.CGSize(0.0f, 3.0f);
        }

        partial void BtnCancelar_TouchUpInside(UIButton sender)
        {
            this.DismissModalViewController(true);
        }

        async void BtnContinuar_TouchDown(object sender, EventArgs e)
        {
            if (_intentosRestantes == 0)
            {
                ShowMessage("Error", "Se alcanzó el límite de intentos");
                return;
            }
            if (tfCodigo.Text == Application.Codigo)
            {
                var nuevaContrasenaController = (NewPasswordController)Storyboard.InstantiateViewController("NewPasswordController");
                await PresentViewControllerAsync(nuevaContrasenaController, true);
			}
            else
            {
                _intentosRestantes -= 1;
                ShowMessage("Error", "El código es incorrecto, vuelva a intentarlo");
            }
        }

        async partial void BtnReenviar_TouchUpInside(UIButton sender)
        {
            View.Add(_loadPop);
            RandomPassword rp = new RandomPassword();
            var _codigo = await rp.Generate();

            var resultado = await _client.EnviarMailToken(Application.Usuario, _codigo);
            if (resultado.ValidarOperacion == "true")
            {
                Application.Codigo = _codigo;
                ShowMessage("Código", "Se envío el código");
                _loadPop.Hide();
            }
        }
    }
}