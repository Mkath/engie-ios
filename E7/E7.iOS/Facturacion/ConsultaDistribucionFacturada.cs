using Foundation;
using System;
using UIKit;
using OxyPlot.Xamarin.iOS;
using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;
//using CoreGraphics;
using E7.Services;
using CoreGraphics;
using System.Collections.Generic;
using E7.iOS.Models;
using System.Linq;
using System.Globalization;
using E7.iOS.Utils;
using E7.iOS.Offline;
//using SciChart.iOS.Charting;

namespace E7.iOS
{
    public partial class ConsultaDistribucionFacturada : UIViewController
    {
        RestClient _client;
        PlotModel _plotModel;
        PlotView _plotView;
        string[] colors = new string[] { "#7cb5ec", "#434348", "#90ed7d", "#f7a35c", "#8085e9", "#f15c80", "#e4d354", "#2b908f" };
        public static string Month = "";
        public static string Year = "";

        public ConsultaDistribucionFacturada(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            _client = new RestClient();
            _plotModel = new PlotModel { Title = DatesHelper.GetMonthNameByAb(Month) + " " + Year };
            _plotView = new PlotView ();
            CGRect rect1 = new CGRect();
            rect1.Width = this.View.Frame.Width;
            rect1.Height = this.View.Frame.Height / 2.5f;
            _plotView.Frame = rect1;

            GetReporte();
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);

            UILabel title = new UILabel() { Text = "Distribución Facturación", TextColor = UIColor.White };

            NavigationController.NavigationBar.BarTintColor = Application.Rojo;
            NavigationController.NavigationBar.Translucent = false;

            NavigationItem.TitleView = title;

            var online = _client.CheckConnection();
            if (!online)
            {
                var navBar = NavigationController.NavigationBar;
                NavigationItem.SetRightBarButtonItem(
                    new UIBarButtonItem(OfflineLabel.Label(navBar.Bounds.Width, navBar.Bounds.Height, true)),
                    false);
            }
        }

        async void GetReporte()
        {
            var main = Application.GetInstance();
            var distribucionFacturada = main.distribucionFacturada;
            var fecha = string.Format("01/{0}/{1}", distribucionFacturada.Mes, distribucionFacturada.Anio);
            var resultado = await _client.Reporte_DistribucionFacturacion(fecha, distribucionFacturada.ClienteID.ToString(), distribucionFacturada.PuntoFID.ToString(), "USD");
            if (resultado == null)
            {
                Alert alert = new Alert();
                var a = alert.ShowMessage("", "No hay datos");
                a.Clicked += A_Clicked;
                a.Show();
                return;
            }
            if (resultado.Count != 0)
            {
                var total = resultado.Sum(x => Convert.ToDouble(x.Consumo));

                var pieLabels = new List<PieLabels>();
                var seriesP1 = new PieSeries
                {
                    StrokeThickness = 0,
                    InsideLabelPosition = -5,
                    AngleSpan = 360,
                    StartAngle = 270,
                    OutsideLabelFormat = null
                };

                for (var i = 0; i < resultado.Count; i++)
                {
                    seriesP1.Slices.Add(new PieSlice(resultado[i].Concepto, double.Parse(resultado[i].Consumo))
                    {
                        Fill = OxyColor.Parse(colors[i])
                    });
                    string porcentaje = ((double.Parse(resultado[i].Consumo) / total) * 100).ToString("N2");
                    string consumo = string.Format("{0:n}", double.Parse(resultado[i].Consumo));
                    pieLabels.Add(new PieLabels()
                    {
                        Color = colors[i],
                        Porcentage = porcentaje + "%",
                        Name = resultado[i].Concepto + " ($ " + consumo + ")"
                    });
                }

                PieLabelsTableView.Source = new PieLabelsTVS(pieLabels);
                PieLabelsTableView.ReloadData();

                _plotModel.Series.Add(seriesP1);
                _plotView.Model = _plotModel;

                this.View.AddSubview(_plotView);
            }
            else
            {
                Alert alert = new Alert();
                var a = alert.ShowMessage("", "No hay datos");
                a.Clicked += A_Clicked;
                a.Show();
            }
        }

        void A_Clicked(object sender, UIButtonEventArgs e)
        {
            this.NavigationController.PopViewController(true);
        }
    }

    class PieLabelsTVS : UITableViewSource
    {
        List<PieLabels> pieLabels;

        public PieLabelsTVS(List<PieLabels> pieLabels)
        {
            this.pieLabels = pieLabels;
        }

        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {
            var cell = (DistribucionFacturadaCell)tableView.DequeueReusableCell("Cell_Id", indexPath);
            var c = pieLabels[indexPath.Row];
            cell.UpdateCell(c);
            return cell;
        }

        public override nint RowsInSection(UITableView tableview, nint section)
        {
            return pieLabels.Count;
        }
    }
}