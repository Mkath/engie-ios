﻿using System;
using E7.iOS.Offline;
using E7.Services;
using UIKit;

namespace E7.iOS
{
    public partial class ConsultaConsumoFacturadoController : UITabBarController
    {
        RestClient _client;

        public ConsultaConsumoFacturadoController(IntPtr b) : base(b) { }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);

            _client = new RestClient();

            UILabel title = new UILabel() { Text = "Consumo Facturado", TextColor = UIColor.White };

			NavigationController.NavigationBar.BarTintColor = Application.Azul;
            NavigationController.NavigationBar.Translucent = false;

            NavigationItem.TitleView = title;
            TabBar.TintColor = UIColor.White;
            TabBar.BarTintColor = Application.Azul;

            var online = _client.CheckConnection();
            if (!online)
            {
                var navBar = NavigationController.NavigationBar;
                NavigationItem.SetRightBarButtonItem(
                    new UIBarButtonItem(OfflineLabel.Label(navBar.Bounds.Width, navBar.Bounds.Height, false)),
                    false);
            }

   //         var navBar = NavigationController.NavigationBar;

   //         CoreGraphics.CGRect cGRect = new CoreGraphics.CGRect(
   //             (navBar.Bounds.Width / 5) * 5,
   //             (navBar.Bounds.Height / 3) * 1,
   //             60,
   //             20);

   //         UILabel txt = new UILabel(cGRect);
   //         txt.Text = "  Modo Offline  ";
   //         txt.TextColor = Application.Azul;
   //         txt.AdjustsFontSizeToFitWidth = true;
   //         txt.Font = UIFont.BoldSystemFontOfSize(8f);
   //         txt.Center = new CoreGraphics.CGPoint(cGRect.Width / 2, cGRect.Height / 2);

			//UIView view = new UIView(cGRect);
			//view.BackgroundColor = UIColor.White;
            //view.AddSubview(txt);

            //NavigationItem.SetRightBarButtonItem(new UIBarButtonItem(view), false);
        }
    }
}
