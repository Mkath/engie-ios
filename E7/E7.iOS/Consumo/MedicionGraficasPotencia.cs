using Foundation;
using System;
using UIKit;
using SciChart.iOS.Charting;
using E7.iOS.Utils;

namespace E7.iOS
{
    public partial class MedicionGraficasPotencia : UIViewController
    {
        SCIChartSurface _surface;
        SCIVerticallyStackedMountainsCollection _mountainCollection;

        public MedicionGraficasPotencia() : base("MedicionGraficasPotencia", null)
        {
        }

        public MedicionGraficasPotencia(IntPtr b) : base(b) { }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            // Perform any additional setup after loading the view, typically from a nib.

            EdgesForExtendedLayout = UIRectEdge.None;

            _surface = new SCIChartSurface();

            _surface.TranslatesAutoresizingMaskIntoConstraints = true;

            SCIThemeManager.ApplyTheme(_surface, SCIThemeManager.SCIChart_Bright_SparkStyleKey);

            _surface.XAxes.Add(ChartCreator.xDateAxis());
            var y = ChartCreator.yAxis("Potencia (kW)");
            y.LabelProvider = new ThousandsLabelProvider();
            _surface.YAxes.Add(y);

            this.View.AddSubview(_surface);

            GetReporte();
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);

            _surface.Frame = this.View.Bounds;
        }

        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
            // Release any cached data, images, etc that aren't in use.
        }

        void GetReporte()
        {
            if (MedicionGraficasEActiva.resultadoMedicionYGraficas == null)
                return;

            var resultado = MedicionGraficasEActiva.resultadoMedicionYGraficas;

            var ds1 = new XyDataSeries<DateTime, double> { SeriesName = "Potencia (kW)", AcceptUnsortedData = true };

            foreach (var x in resultado)
            {
                ds1.Append(x.FechayHora, double.Parse(x.Potencia));
            }

            var series1 = ChartCreator.GetMountainRenderableSeries(ds1, new SCISolidPenStyle(UIColor.FromRGB(47, 174, 249), 2), new SCISolidBrushStyle(UIColor.FromRGBA(256, 256, 256, 256)));

            _mountainCollection = new SCIVerticallyStackedMountainsCollection();
            _mountainCollection.Add(series1);

            _surface.RenderableSeries.Add(_mountainCollection);
            _surface.ChartModifiers.Add(new SCICursorModifier());
            _surface.ChartModifiers.Add(new SCIPinchZoomModifier()
            {
                Direction = SCIDirection2D.XDirection
            });
            _surface.ChartModifiers.Add(new SCIZoomExtentsModifier());
        }
    }
}