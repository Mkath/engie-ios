using Foundation;
using System;
using UIKit;
using SciChart.iOS.Charting;
using CoreGraphics;
using E7.iOS.Utils;
using E7.Services;
using System.Linq;
using System.Collections.Generic;
using E7.Models;
using E7.iOS.Utils.DataBase;
using System.Globalization;
using E7.iOS.Offline;

namespace E7.iOS
{
    public partial class ConsultaGraficoHistoricoFacturado : UIViewController
    {
        SCIChartSurface _surface;
        SCIHorizontallyStackedColumnsCollection _columnCollection;
        RestClient _client;
        SCIDateTimeAxis _x;
        UIView _view;
        CGRect legendViewRect;
        string _moneda;

        public ConsultaGraficoHistoricoFacturado() : base("ConsultaGraficoHistoricoFacturado", null)
        {
        }

        public ConsultaGraficoHistoricoFacturado(IntPtr b) : base(b) { }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            // Perform any additional setup after loading the view, typically from a nib.

            _client = new RestClient();
            EdgesForExtendedLayout = UIRectEdge.None;
            ExtendedLayoutIncludesOpaqueBars = false;
            AutomaticallyAdjustsScrollViewInsets = false;
        }

        public override void ViewDidAppear(bool animated)
        {
            base.ViewDidAppear(animated);

            _surface = new SCIChartSurface();
            _surface.TranslatesAutoresizingMaskIntoConstraints = true;
            CGRect surfaceRect = new CGRect();
            surfaceRect.Width = this.View.Bounds.Width;
            surfaceRect.Height = this.View.Bounds.Height / 2;
            _surface.Frame = surfaceRect;
            //_surface.Frame = this.View.Bounds;

            legendViewRect = new CGRect();
            legendViewRect.Y = this.View.Bounds.Height / 2;
            legendViewRect.Width = this.View.Bounds.Width;
            legendViewRect.Height = this.View.Bounds.Height / 2;

            _view = new UIView();
            _view.Frame = legendViewRect;

            SCIThemeManager.ApplyTheme(_surface, SCIThemeManager.SCIChart_Bright_SparkStyleKey);

            this.View.AddSubview(_surface);
            this.View.AddSubview(_view);

            _x = ChartCreator.xDateAxis();
            _x.TextFormatting = "MMM yyyy";
            _surface.XAxes.Add(_x);

            var y = ChartCreator.yAxis("Total");
            y.LabelProvider = new MillionsLabelProvider();
            y.GrowBy = new SCIDoubleRange(0, 0.5);
            _surface.YAxes.Add(y);

            GetReporte();
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);
            UILabel title = new UILabel() { Text = "Gráfico Histórico Facturado", TextColor = UIColor.White };

            NavigationController.NavigationBar.BarTintColor = Application.Rojo;
            NavigationController.NavigationBar.Translucent = false;

            NavigationItem.TitleView = title;

            var online = _client.CheckConnection();
            if (!online)
            {
                var navBar = NavigationController.NavigationBar;
                NavigationItem.SetRightBarButtonItem(
                    new UIBarButtonItem(OfflineLabel.Label(navBar.Bounds.Width, navBar.Bounds.Height, true)),
                    false);
            }
        }

        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
            // Release any cached data, images, etc that aren't in use.
        }

        async void GetReporte()
        {
            var main = Application.GetInstance();
            var gHF = main.graficoHistoricoFacturado;
            List<BEGraficosFacturacion> resultado;
            List<TBLHistoricoFacturado> resultado1;
            DAReporte_Facturacion conexion = new DAReporte_Facturacion();
            BLTBLHistoricoFacturado conexion1 = new BLTBLHistoricoFacturado();
            TBLHistoricoFacturado data = new TBLHistoricoFacturado();
            Alert alert = new Alert();
            string mhp;
            string MensajeErrorHF;
            _moneda = gHF.Moneda;

            var online = _client.CheckConnection();
            var _modooff = "";
            if (online)
                _modooff = "0";
            else
                _modooff = main.ModoOff;
            
            if (_moneda == "USD") { mhp = "$ "; } else if (_moneda == "PEN") { mhp = "S/ "; } else { mhp = ""; }
            if (_modooff == "0")
            {
                resultado = await conexion.Reporte_HistoricoFacturado(gHF.Desde, gHF.Hasta, gHF.ClienteID.ToString(), gHF.PuntoFID.ToString(), _moneda);
                MensajeErrorHF = conexion.ResultadoFinalRHF;
                if (resultado == null)
                {
                    var a = alert.ShowMessage("Alerta", MensajeErrorHF);
                    a.Clicked += A_Clicked;
                    a.Show();
                }
                else if (resultado.Count == 0)
                {
                    var a = alert.ShowMessage("Alerta", "No hay datos.");
                    a.Clicked += A_Clicked;
                    a.Show();
                }
                else
                {
                    var Fecha = resultado.OrderBy(x => x.Fecha).Select(x => x.Fecha).Distinct().ToArray();
                    var EnergiaActiva = resultado.OrderBy(x => x.Fecha).Where(x => x.Concepto == "EnergiaActiva").Select(x => (Convert.ToDouble(x.Consumo))).ToArray();
                    var Potencia = resultado.OrderBy(x => x.Fecha).Where(x => x.Concepto == "Potencia").Select(x => (Convert.ToDouble(x.Consumo))).ToArray();
                    var EnergiaReactiva = resultado.OrderBy(x => x.Fecha).Where(x => x.Concepto == "EnergiaReactiva").Select(x => (Convert.ToDouble(x.Consumo))).ToArray();
                    var PeajePrincipal = resultado.OrderBy(x => x.Fecha).Where(x => x.Concepto == "PeajePrincipal").Select(x => (Convert.ToDouble(x.Consumo))).ToArray();
                    var PeajeSecundario = resultado.OrderBy(x => x.Fecha).Where(x => x.Concepto == "PeajeSecundario").Select(x => (Convert.ToDouble(x.Consumo))).ToArray();
                    var Otros = resultado.OrderBy(x => x.Fecha).Where(x => x.Concepto == "Otros").Select(x => (Convert.ToDouble(x.Consumo))).ToArray();
                    var ElectrificacionRural = resultado.OrderBy(x => x.Fecha).Where(x => x.Concepto == "ElectrificacionRural").Select(x => (Convert.ToDouble(x.Consumo))).ToArray();
                    var FISE = resultado.OrderBy(x => x.Fecha).Where(x => x.Concepto == "FISE").Select(x => (Convert.ToDouble(x.Consumo))).ToArray();
                    var cont = (Convert.ToDouble(resultado.Count) / Convert.ToDouble(8));

                    //GraficarColumn(Fecha, EnergiaActiva, EnergiaReactiva, PeajePrincipal, PeajeSecundario, Otros, FISE, Potencia, ElectrificacionRural, cont);

                    if (resultado.Count != 0)
                    {
                        var EnergiaActivaDataSeries = new XyDataSeries<DateTime, double> { SeriesName = "Energia Activa" };
                        var PotenciaDataSeries = new XyDataSeries<DateTime, double> { SeriesName = "Potencia" };
                        var EnergiaReactivaDataSeries = new XyDataSeries<DateTime, double> { SeriesName = "Energia Reactiva" };
                        var PeajePrincipalDataSeries = new XyDataSeries<DateTime, double> { SeriesName = "Peaje Principal" };
                        var PeajeSecundarioDataSeries = new XyDataSeries<DateTime, double> { SeriesName = "Peaje Secundario" };
                        var OtrosDataSeries = new XyDataSeries<DateTime, double> { SeriesName = "Otros" };
                        var ElectrificacionRuralDataSeries = new XyDataSeries<DateTime, double> { SeriesName = "Electrificacion Rural" };
                        var FISEDataSeries = new XyDataSeries<DateTime, double> { SeriesName = "FISE" };
                        var totalDataSeries = new XyDataSeries<DateTime, double> { SeriesName = "Total" };

                        if (Fecha.Count() == 1)
                            _x.AxisTitle = Fecha[0].ToString("MMM yyyy");

                        for (var i = 0; i < resultado.Count / 8; i++)
                        {
                            EnergiaActivaDataSeries.Append(Fecha[i], EnergiaActiva[i]);
                            PotenciaDataSeries.Append(Fecha[i], Potencia[i]);
                            EnergiaReactivaDataSeries.Append(Fecha[i], EnergiaReactiva[i]);
                            PeajePrincipalDataSeries.Append(Fecha[i], PeajePrincipal[i]);
                            PeajeSecundarioDataSeries.Append(Fecha[i], PeajeSecundario[i]);
                            OtrosDataSeries.Append(Fecha[i], Otros[i]);
                            ElectrificacionRuralDataSeries.Append(Fecha[i], ElectrificacionRural[i]);
                            FISEDataSeries.Append(Fecha[i], FISE[i]);
                        }

                        _columnCollection = new SCIHorizontallyStackedColumnsCollection();
                        _columnCollection.Add(ChartCreator.GetColumnRenderableSeriesWithUint(EnergiaActivaDataSeries, 0xff7cb5ec, 0xff7cb5ec));
                        _columnCollection.Add(ChartCreator.GetColumnRenderableSeriesWithUint(PotenciaDataSeries, 0xFF434348, 0xFF434348));
                        _columnCollection.Add(ChartCreator.GetColumnRenderableSeriesWithUint(EnergiaReactivaDataSeries, 0xFF90ed7d, 0xFF90ed7d));
                        _columnCollection.Add(ChartCreator.GetColumnRenderableSeriesWithUint(PeajePrincipalDataSeries, 0xFFf7a35c, 0xFFf7a35c));
                        _columnCollection.Add(ChartCreator.GetColumnRenderableSeriesWithUint(PeajeSecundarioDataSeries, 0xFF8085e9, 0xFF8085e9));
                        _columnCollection.Add(ChartCreator.GetColumnRenderableSeriesWithUint(OtrosDataSeries, 0xfff15c80, 0xfff15c80));
                        _columnCollection.Add(ChartCreator.GetColumnRenderableSeriesWithUint(ElectrificacionRuralDataSeries, 0xFFe4d354, 0xFFe4d354));
                        _columnCollection.Add(ChartCreator.GetColumnRenderableSeriesWithUint(FISEDataSeries, 0xff2b908f, 0xff2b908f));

                        var legendModifier = new SCILegendModifier()
                        {
                            Orientation = SCIOrientation.Vertical,
                            Position = SCILegendPosition.Left,
                            SourceMode = SCISourceMode.AllVisibleSeries,
                            ShowSeriesMarkers = true,
                            ShowCheckBoxes = true,
                        };

                        legendModifier.HolderLegendView.Frame = legendViewRect;

                        _surface.RenderableSeries.Add(_columnCollection);
                        _surface.ChartModifiers.Add(new SCICursorModifier());
                        _surface.ChartModifiers.Add(legendModifier);
                        _surface.ChartModifiers.Add(new SCIPinchZoomModifier()
                        {
                            Direction = SCIDirection2D.XDirection
                        });
                        _surface.ChartModifiers.Add(new SCIZoomExtentsModifier());
                        _surface.ChartModifiers.Add(new SCIXAxisDragModifier());

                        _view.AddSubview(legendModifier.HolderLegendView);
                    }
                    else
                    {
                        var a = alert.ShowMessage("Alerta", "No hay datos");
                        a.Clicked += A_Clicked;
                        a.Show();
                    }
                }
            }
            else if (_modooff != "0")
            {
                DateTime desde = DateTime.ParseExact(gHF.Desde, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                DateTime _desde = new DateTime(desde.Year, desde.Month, 1);
                DateTime _Fdesde = new DateTime(desde.Year, desde.Month, 20);
                DateTime hasta = DateTime.ParseExact(gHF.Hasta, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                DateTime _hasta = new DateTime(hasta.Year, hasta.Month, 20);
                data.CodCliente = gHF.ClienteID.ToString();
                data.CodPuntoFacturacion = gHF.PuntoFID.ToString();
                data.Moneda = _moneda;
                resultado1 = await conexion1.Reporte_HistoricoFacturado(data, _desde, _hasta);
                MensajeErrorHF = "Error al momento de leer los datos guardados.";
                if (resultado1 == null)
                {
                    var a = alert.ShowMessage("Alerta", MensajeErrorHF);
                    a.Clicked += A_Clicked;
                    a.Show();
                    return;
                }
                var _ultimoMes = resultado1.Where(x => (x.Fecha <= _Fdesde)).ToArray();
                if (resultado1.Count == 0 || _ultimoMes.Count() == 0)
                {
                    resultado1 = await conexion1.Reporte_HistoricoFacturado(data);

                    if (resultado1 == null)
                    {
                        var a = alert.ShowMessage("Alerta", "No hay datos sincronizados.");
                        a.Clicked += A_Clicked;
                        a.Show();
                    }
                    else if (resultado1.Count == 0)
                    {
                        var a = alert.ShowMessage("Alerta", "No hay datos sincronizados.");
                        a.Clicked += A_Clicked;
                        a.Show();
                    }
                    else
                    {
                        string _meses = "";
                        if (_modooff == "1") { _meses = " mes."; } else { _meses = " meses."; }
                        var a = alert.ShowMessage("Alerta", "Usted solo tiene datos sincronizados de " + _modooff + _meses);
                        a.Clicked += (sender, e) => {
                            var Fecha = resultado1.OrderBy(x => x.Fecha).Select(x => x.Fecha).Distinct().ToArray();
                            var EnergiaActiva = resultado1.OrderBy(x => x.Fecha).Where(x => x.Concepto == "EnergiaActiva").Select(x => (Convert.ToDouble(x.Consumo))).ToArray();
                            var Potencia = resultado1.OrderBy(x => x.Fecha).Where(x => x.Concepto == "Potencia").Select(x => (Convert.ToDouble(x.Consumo))).ToArray();
                            var EnergiaReactiva = resultado1.OrderBy(x => x.Fecha).Where(x => x.Concepto == "EnergiaReactiva").Select(x => (Convert.ToDouble(x.Consumo))).ToArray();
                            var PeajePrincipal = resultado1.OrderBy(x => x.Fecha).Where(x => x.Concepto == "PeajePrincipal").Select(x => (Convert.ToDouble(x.Consumo))).ToArray();
                            var PeajeSecundario = resultado1.OrderBy(x => x.Fecha).Where(x => x.Concepto == "PeajeSecundario").Select(x => (Convert.ToDouble(x.Consumo))).ToArray();
                            var Otros = resultado1.OrderBy(x => x.Fecha).Where(x => x.Concepto == "Otros").Select(x => (Convert.ToDouble(x.Consumo))).ToArray();
                            var ElectrificacionRural = resultado1.OrderBy(x => x.Fecha).Where(x => x.Concepto == "ElectrificacionRural").Select(x => (Convert.ToDouble(x.Consumo))).ToArray();
                            var FISE = resultado1.OrderBy(x => x.Fecha).Where(x => x.Concepto == "FISE").Select(x => (Convert.ToDouble(x.Consumo))).ToArray();
                            var cont = (Convert.ToDouble(resultado1.Count) / Convert.ToDouble(8));
                            //GraficarColumn(Fecha, EnergiaActiva, EnergiaReactiva, PeajePrincipal, PeajeSecundario, Otros, FISE, Potencia, ElectrificacionRural, cont);

                            if (resultado1.Count != 0)
                            {
                                var EnergiaActivaDataSeries = new XyDataSeries<DateTime, double> { SeriesName = "Energia Activa" };
                                var PotenciaDataSeries = new XyDataSeries<DateTime, double> { SeriesName = "Potencia" };
                                var EnergiaReactivaDataSeries = new XyDataSeries<DateTime, double> { SeriesName = "Energia Reactiva" };
                                var PeajePrincipalDataSeries = new XyDataSeries<DateTime, double> { SeriesName = "Peaje Principal" };
                                var PeajeSecundarioDataSeries = new XyDataSeries<DateTime, double> { SeriesName = "Peaje Secundario" };
                                var OtrosDataSeries = new XyDataSeries<DateTime, double> { SeriesName = "Otros" };
                                var ElectrificacionRuralDataSeries = new XyDataSeries<DateTime, double> { SeriesName = "Electrificacion Rural" };
                                var FISEDataSeries = new XyDataSeries<DateTime, double> { SeriesName = "FISE" };
                                var totalDataSeries = new XyDataSeries<DateTime, double> { SeriesName = "Total" };

                                if (Fecha.Count() == 1)
                                    _x.AxisTitle = Fecha[0].ToString("MMM yyyy");

                                for (var i = 0; i < resultado1.Count / 8; i++)
                                {
                                    EnergiaActivaDataSeries.Append(Fecha[i], EnergiaActiva[i]);
                                    PotenciaDataSeries.Append(Fecha[i], Potencia[i]);
                                    EnergiaReactivaDataSeries.Append(Fecha[i], EnergiaReactiva[i]);
                                    PeajePrincipalDataSeries.Append(Fecha[i], PeajePrincipal[i]);
                                    PeajeSecundarioDataSeries.Append(Fecha[i], PeajeSecundario[i]);
                                    OtrosDataSeries.Append(Fecha[i], Otros[i]);
                                    ElectrificacionRuralDataSeries.Append(Fecha[i], ElectrificacionRural[i]);
                                    FISEDataSeries.Append(Fecha[i], FISE[i]);
                                }

                                _columnCollection = new SCIHorizontallyStackedColumnsCollection();
                                _columnCollection.Add(ChartCreator.GetColumnRenderableSeriesWithUint(EnergiaActivaDataSeries, 0xff7cb5ec, 0xff7cb5ec));
                                _columnCollection.Add(ChartCreator.GetColumnRenderableSeriesWithUint(PotenciaDataSeries, 0xFF434348, 0xFF434348));
                                _columnCollection.Add(ChartCreator.GetColumnRenderableSeriesWithUint(EnergiaReactivaDataSeries, 0xFF90ed7d, 0xFF90ed7d));
                                _columnCollection.Add(ChartCreator.GetColumnRenderableSeriesWithUint(PeajePrincipalDataSeries, 0xFFf7a35c, 0xFFf7a35c));
                                _columnCollection.Add(ChartCreator.GetColumnRenderableSeriesWithUint(PeajeSecundarioDataSeries, 0xFF8085e9, 0xFF8085e9));
                                _columnCollection.Add(ChartCreator.GetColumnRenderableSeriesWithUint(OtrosDataSeries, 0xfff15c80, 0xfff15c80));
                                _columnCollection.Add(ChartCreator.GetColumnRenderableSeriesWithUint(ElectrificacionRuralDataSeries, 0xFFe4d354, 0xFFe4d354));
                                _columnCollection.Add(ChartCreator.GetColumnRenderableSeriesWithUint(FISEDataSeries, 0xff2b908f, 0xff2b908f));

                                var legendModifier = new SCILegendModifier()
                                {
                                    Orientation = SCIOrientation.Vertical,
                                    Position = SCILegendPosition.Left,
                                    SourceMode = SCISourceMode.AllVisibleSeries,
                                    ShowSeriesMarkers = true,
                                    ShowCheckBoxes = true,
                                };

                                legendModifier.HolderLegendView.Frame = legendViewRect;

                                _surface.RenderableSeries.Add(_columnCollection);
                                _surface.ChartModifiers.Add(new SCICursorModifier());
                                _surface.ChartModifiers.Add(legendModifier);
                                _surface.ChartModifiers.Add(new SCIPinchZoomModifier()
                                {
                                    Direction = SCIDirection2D.XDirection
                                });
                                _surface.ChartModifiers.Add(new SCIZoomExtentsModifier());
                                _surface.ChartModifiers.Add(new SCIXAxisDragModifier());
                                        
                                _view.AddSubview(legendModifier.HolderLegendView);
                            }
                            else
                            {
                                a = alert.ShowMessage("Alerta", "No hay datos");
                                a.Clicked += A_Clicked;
                                a.Show();
                            }
                        };
                        a.Show();
                    }
                }
                else
                {
                    var Fecha = resultado1.OrderBy(y => y.Fecha).Select(x => x.Fecha).Distinct().ToArray();
                    var EnergiaActiva = resultado1.OrderBy(y => y.Fecha).Where(x => x.Concepto == "EnergiaActiva").Select(x => (Convert.ToDouble(x.Consumo))).ToArray();
                    var Potencia = resultado1.OrderBy(y => y.Fecha).Where(x => x.Concepto == "Potencia").Select(x => (Convert.ToDouble(x.Consumo))).ToArray();
                    var EnergiaReactiva = resultado1.OrderBy(y => y.Fecha).Where(x => x.Concepto == "EnergiaReactiva").Select(x => (Convert.ToDouble(x.Consumo))).ToArray();
                    var PeajePrincipal = resultado1.OrderBy(y => y.Fecha).Where(x => x.Concepto == "PeajePrincipal").Select(x => (Convert.ToDouble(x.Consumo))).ToArray();
                    var PeajeSecundario = resultado1.OrderBy(y => y.Fecha).Where(x => x.Concepto == "PeajeSecundario").Select(x => (Convert.ToDouble(x.Consumo))).ToArray();
                    var Otros = resultado1.OrderBy(y => y.Fecha).Where(x => x.Concepto == "Otros").Select(x => (Convert.ToDouble(x.Consumo))).ToArray();
                    var ElectrificacionRural = resultado1.OrderBy(y => y.Fecha).Where(x => x.Concepto == "ElectrificacionRural").Select(x => (Convert.ToDouble(x.Consumo))).ToArray();
                    var FISE = resultado1.OrderBy(y => y.Fecha).Where(x => x.Concepto == "FISE").Select(x => (Convert.ToDouble(x.Consumo))).ToArray();
                    var cont = (Convert.ToDouble(resultado1.Count) / Convert.ToDouble(8));

                    //GraficarColumn(Fecha, EnergiaActiva, EnergiaReactiva, PeajePrincipal, PeajeSecundario, Otros, FISE, Potencia, ElectrificacionRural, cont);
                    if (resultado1.Count != 0)
                    {
                        var EnergiaActivaDataSeries = new XyDataSeries<DateTime, double> { SeriesName = "Energia Activa" };
                        var PotenciaDataSeries = new XyDataSeries<DateTime, double> { SeriesName = "Potencia" };
                        var EnergiaReactivaDataSeries = new XyDataSeries<DateTime, double> { SeriesName = "Energia Reactiva" };
                        var PeajePrincipalDataSeries = new XyDataSeries<DateTime, double> { SeriesName = "Peaje Principal" };
                        var PeajeSecundarioDataSeries = new XyDataSeries<DateTime, double> { SeriesName = "Peaje Secundario" };
                        var OtrosDataSeries = new XyDataSeries<DateTime, double> { SeriesName = "Otros" };
                        var ElectrificacionRuralDataSeries = new XyDataSeries<DateTime, double> { SeriesName = "Electrificacion Rural" };
                        var FISEDataSeries = new XyDataSeries<DateTime, double> { SeriesName = "FISE" };
                        var totalDataSeries = new XyDataSeries<DateTime, double> { SeriesName = "Total" };

                        if (Fecha.Count() == 1)
                            _x.AxisTitle = Fecha[0].ToString("MMM yyyy");

                        for (var i = 0; i < resultado1.Count / 8; i++)
                        {
                            EnergiaActivaDataSeries.Append(Fecha[i], EnergiaActiva[i]);
                            PotenciaDataSeries.Append(Fecha[i], Potencia[i]);
                            EnergiaReactivaDataSeries.Append(Fecha[i], EnergiaReactiva[i]);
                            PeajePrincipalDataSeries.Append(Fecha[i], PeajePrincipal[i]);
                            PeajeSecundarioDataSeries.Append(Fecha[i], PeajeSecundario[i]);
                            OtrosDataSeries.Append(Fecha[i], Otros[i]);
                            ElectrificacionRuralDataSeries.Append(Fecha[i], ElectrificacionRural[i]);
                            FISEDataSeries.Append(Fecha[i], FISE[i]);
                        }

                        _columnCollection = new SCIHorizontallyStackedColumnsCollection();
                        _columnCollection.Add(ChartCreator.GetColumnRenderableSeriesWithUint(EnergiaActivaDataSeries, 0xff7cb5ec, 0xff7cb5ec));
                        _columnCollection.Add(ChartCreator.GetColumnRenderableSeriesWithUint(PotenciaDataSeries, 0xFF434348, 0xFF434348));
                        _columnCollection.Add(ChartCreator.GetColumnRenderableSeriesWithUint(EnergiaReactivaDataSeries, 0xFF90ed7d, 0xFF90ed7d));
                        _columnCollection.Add(ChartCreator.GetColumnRenderableSeriesWithUint(PeajePrincipalDataSeries, 0xFFf7a35c, 0xFFf7a35c));
                        _columnCollection.Add(ChartCreator.GetColumnRenderableSeriesWithUint(PeajeSecundarioDataSeries, 0xFF8085e9, 0xFF8085e9));
                        _columnCollection.Add(ChartCreator.GetColumnRenderableSeriesWithUint(OtrosDataSeries, 0xfff15c80, 0xfff15c80));
                        _columnCollection.Add(ChartCreator.GetColumnRenderableSeriesWithUint(ElectrificacionRuralDataSeries, 0xFFe4d354, 0xFFe4d354));
                        _columnCollection.Add(ChartCreator.GetColumnRenderableSeriesWithUint(FISEDataSeries, 0xff2b908f, 0xff2b908f));

                        var legendModifier = new SCILegendModifier()
                        {
                            Orientation = SCIOrientation.Vertical,
                            Position = SCILegendPosition.Left,
                            SourceMode = SCISourceMode.AllVisibleSeries,
                            ShowSeriesMarkers = true,
                            ShowCheckBoxes = true,
                        };

                        legendModifier.HolderLegendView.Frame = legendViewRect;

                        _surface.RenderableSeries.Add(_columnCollection);
                        _surface.ChartModifiers.Add(new SCICursorModifier());
                        _surface.ChartModifiers.Add(legendModifier);
                        _surface.ChartModifiers.Add(new SCIPinchZoomModifier()
                        {
                            Direction = SCIDirection2D.XDirection
                        });
                        _surface.ChartModifiers.Add(new SCIZoomExtentsModifier());
                        _surface.ChartModifiers.Add(new SCIXAxisDragModifier());

                        _view.AddSubview(legendModifier.HolderLegendView);
                    }
                    else
                    {
                        var a = alert.ShowMessage("Alerta", "No hay datos");
                        a.Clicked += A_Clicked;
                        a.Show();
                    }
                }
            }
        }

        void A_Clicked(object sender, UIButtonEventArgs e)
        {
            try
            {
				this.NavigationController.PopViewController(true);
            }
            catch { }
        }
    }
}