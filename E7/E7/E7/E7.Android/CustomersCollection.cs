﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace E7.Droid
{
    public class Customer
    {
        public string CodCliente { get; set; }
        public string NomCliente { get; set; }
    }
    public class Med
    {
        public int CodMedidor { get; set; }
        public string NomMedidor { get; set; }
    }
    class CustomersCollection
    {
        public static JavaList<Customer> ListCustomer()
        {
            JavaList<Customer> customrs = new JavaList<Customer>();
            Customer customer = new Customer();

            customrs.Add(new Customer() { CodCliente = "0", NomCliente = "--Seleccione--" });
            customrs.Add(new Customer() { CodCliente = "164", NomCliente = "AGRÍCOLA HUARMEY S.A." });
            customrs.Add(new Customer() { CodCliente = "178", NomCliente = "AGROINDUSTRIAS SAN JACINTO S.A.A." });
            customrs.Add(new Customer() { CodCliente = "153", NomCliente = "ALGODONERA CONTINENTAL S.A.C." });
            customrs.Add(new Customer() { CodCliente = "172", NomCliente = "AMERICATEL PERÚ S.A." });
            customrs.Add(new Customer() { CodCliente = "139", NomCliente = "ANABI S.A.C." });
            customrs.Add(new Customer() { CodCliente = "26", NomCliente = "APUMAYO S.A.C." });
            customrs.Add(new Customer() { CodCliente = "157", NomCliente = "AUSTRAL GROUP S.A." });
            customrs.Add(new Customer() { CodCliente = "135", NomCliente = "Banco de Crédito del Perú" });
            customrs.Add(new Customer() { CodCliente = "176", NomCliente = "CARTAVIO S.A.A" });
            customrs.Add(new Customer() { CodCliente = "156", NomCliente = "CASA GRANDE S.A." });
            customrs.Add(new Customer() { CodCliente = "136", NomCliente = "CERAMICA LIMA S.A." });
            customrs.Add(new Customer() { CodCliente = "10000001", NomCliente = "CLIENTE_ENGIE ENERGÍA PERÚ" });
            customrs.Add(new Customer() { CodCliente = "57", NomCliente = "COMPANIA MINERA ANTAMINA S.A." });
            customrs.Add(new Customer() { CodCliente = "5", NomCliente = "COMPAÑIA MINERA ANTAPACCAY S.A." });
            customrs.Add(new Customer() { CodCliente = "158", NomCliente = "COMPAÑÍA MINERA CHUNGAR S.A.C." });
            customrs.Add(new Customer() { CodCliente = "20", NomCliente = "COMPAÑIA MINERA SANTA LUISA S.A." });
            customrs.Add(new Customer() { CodCliente = "104", NomCliente = "CONSORCIO ELECTRICO DE VILLACURI S.A.C." });
            customrs.Add(new Customer() { CodCliente = "174", NomCliente = "CORPORACIÓN DE INDUSTRIAS PLÁSTICAS S.A." });
            customrs.Add(new Customer() { CodCliente = "32", NomCliente = "EDECAÑETE S.A." });
            customrs.Add(new Customer() { CodCliente = "86", NomCliente = "EDELNOR S.A.A." });
            customrs.Add(new Customer() { CodCliente = "33", NomCliente = "ELECTRO DUNAS S.A.A." });
            customrs.Add(new Customer() { CodCliente = "34", NomCliente = "ELECTRO PUNO S.A.A." });
            customrs.Add(new Customer() { CodCliente = "112", NomCliente = "ELECTRO SUR ESTE S.A.A." });
            customrs.Add(new Customer() { CodCliente = "114", NomCliente = "ELECTRO UCAYALI S.A." });
            customrs.Add(new Customer() { CodCliente = "88", NomCliente = "ELECTROCENTRO S.A." });
            customrs.Add(new Customer() { CodCliente = "89", NomCliente = "ELECTRONOROESTE S.A." });
            customrs.Add(new Customer() { CodCliente = "113", NomCliente = "ELECTRONORTE S.A." });
            customrs.Add(new Customer() { CodCliente = "90", NomCliente = "ELECTROSUR S.A." });
            customrs.Add(new Customer() { CodCliente = "138", NomCliente = "ESMERALDA CORP. S.A.C" });
            customrs.Add(new Customer() { CodCliente = "179", NomCliente = "FARMEX" });
            customrs.Add(new Customer() { CodCliente = "175", NomCliente = "GLORIA S.A" });
            customrs.Add(new Customer() { CodCliente = "92", NomCliente = "HIDRANDINA S.A." });
            customrs.Add(new Customer() { CodCliente = "23", NomCliente = "INDUSTRIAL PAPELERA ATLAS S.A." });
            customrs.Add(new Customer() { CodCliente = "159", NomCliente = "INDUSTRIAS FIBRAFORTE S.A." });
            customrs.Add(new Customer() { CodCliente = "165", NomCliente = "INMOBILIARIA BOTAFOGO S.A.C." });
            customrs.Add(new Customer() { CodCliente = "162", NomCliente = "INMOBILIARIA EL QUINDE S.A.C." });
            customrs.Add(new Customer() { CodCliente = "166", NomCliente = "INMOBILIARIA KANDOO S.A.C." });
            customrs.Add(new Customer() { CodCliente = "169", NomCliente = "INMOBILIARIA PISAC S.A.C." });
            customrs.Add(new Customer() { CodCliente = "167", NomCliente = "INVERSIONES ALAMEDA SUR S.A.C." });
            customrs.Add(new Customer() { CodCliente = "160", NomCliente = "INVERSIONES NACIONALES DE TURISMO S.A." });
            customrs.Add(new Customer() { CodCliente = "168", NomCliente = "INVERSIONES VILLA EL SALVADOR S.A.C." });
            customrs.Add(new Customer() { CodCliente = "29", NomCliente = "LINDE GAS PERU S.A." });
            customrs.Add(new Customer() { CodCliente = "31", NomCliente = "LUZ DEL SUR S.A.A." });
            customrs.Add(new Customer() { CodCliente = "3", NomCliente = "MANUFACTURA DE METALES Y ALUMINIO RECORD S.A." });
            customrs.Add(new Customer() { CodCliente = "11", NomCliente = "MINERA BATEAS S.A.C." });
            customrs.Add(new Customer() { CodCliente = "30", NomCliente = "MINERA LAS BAMBAS S.A." });
            customrs.Add(new Customer() { CodCliente = "151", NomCliente = "MINERA YANACOCHA S.R.L." });
            customrs.Add(new Customer() { CodCliente = "137", NomCliente = "MINSUR S.A." });
            customrs.Add(new Customer() { CodCliente = "12", NomCliente = "NYRSTAR CORICANCHA S.A." });
            customrs.Add(new Customer() { CodCliente = "152", NomCliente = "ORIGAN S.A." });
            customrs.Add(new Customer() { CodCliente = "22", NomCliente = "OWENS ILLINOIS PERU S.A." });
            customrs.Add(new Customer() { CodCliente = "27", NomCliente = "PAPELERA DEL SUR S.A." });
            customrs.Add(new Customer() { CodCliente = "8", NomCliente = "PAPELERA NACIONAL S.A." });
            customrs.Add(new Customer() { CodCliente = "163", NomCliente = "PARQUE LAMBRAMANI S.A.C." });
            customrs.Add(new Customer() { CodCliente = "21", NomCliente = "PETROLEOS DEL PERU PETROPERU S.A." });
            customrs.Add(new Customer() { CodCliente = "171", NomCliente = "PISOPAK Perú S.A.C." });
            customrs.Add(new Customer() { CodCliente = "7", NomCliente = "QUIMPAC S.A." });
            customrs.Add(new Customer() { CodCliente = "93", NomCliente = "SOCIEDAD ELECTRICA DEL SUR OESTE S.A." });
            customrs.Add(new Customer() { CodCliente = "134", NomCliente = "SOCIEDAD MINERA CERRO VERDE" });
            customrs.Add(new Customer() { CodCliente = "1", NomCliente = "SOUTHERN PERU COPPER CORPORATION" });
            customrs.Add(new Customer() { CodCliente = "155", NomCliente = "TRUPAL S.A." });
            customrs.Add(new Customer() { CodCliente = "2", NomCliente = "UNIVERSIDAD DE LIMA" });
            customrs.Add(new Customer() { CodCliente = "144", NomCliente = "VOTORANTIM METAIS - CAJAMARQUILLA S.A." });
            customrs.Add(new Customer() { CodCliente = "150", NomCliente = "YURA S.A." });

            return (customrs);

        }
    }
    
}