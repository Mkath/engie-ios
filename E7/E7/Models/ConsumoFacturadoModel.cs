﻿using System;
namespace E7.Models
{
    public class ConsumoFacturadoModel
    {
        public string Anio { get; set; }
        public string Cliente { get; set; }
        public int ClienteID { get; set; }
        public string PuntoF { get; set; }
        public int PuntoFID { get; set; }
    }
}
