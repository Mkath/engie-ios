﻿using System;
using CoreGraphics;
using E7.iOS.Utils;
using E7.Services;
using Foundation;
using UIKit;

namespace E7.iOS
{
    public class KeyboardBaseController : UIViewController
    {
        public RestClient _client;
        public Alert _alerta;
        public LoadingOverlay _loadPop;
        UIView activeview;
        float scroll_amount = 0.0f;
        float bottom = 0.0f;
        float offset = 50.0f;
        bool moveViewUp = false;
        bool isScrolled;

        public KeyboardBaseController(IntPtr handle) : base(handle) { }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            _loadPop = new LoadingOverlay(UIScreen.MainScreen.Bounds);
            _alerta = new Alert();
            _client = new RestClient();

            UITapGestureRecognizer closeKeyboard = null;

            Action closeKeyboardAction = () => {
                View.EndEditing(true);
            };

            closeKeyboard = new UITapGestureRecognizer(closeKeyboardAction);
            this.View.AddGestureRecognizer(closeKeyboard);

            NSNotificationCenter.DefaultCenter.AddObserver(UIKeyboard.DidShowNotification, KeyBoardUpNotification);
            NSNotificationCenter.DefaultCenter.AddObserver(UIKeyboard.WillHideNotification, KeyBoardDownNotification);
        }

        public void ShowMessage(string title, string message)
        {
            _alerta.ShowMessageWithButton(title, message);
        }

        void KeyBoardUpNotification(NSNotification notification)
        {
            CGRect r = UIKeyboard.FrameEndFromNotification(notification);

            foreach (UIView view in this.View.Subviews)
            {
                if (view.IsFirstResponder)
                    activeview = view;
            }

            if (activeview == null)
                return;

			bottom = (float)(activeview.Frame.Y + activeview.Frame.Height + offset);

            scroll_amount = (float)(r.Height - (View.Frame.Size.Height - bottom));

            if (scroll_amount > 0)
            {
                moveViewUp = true;
                if (!isScrolled)
                    ScrollTheView(moveViewUp);
            }
            else
            {
                moveViewUp = false;
            }
        }

        void KeyBoardDownNotification(NSNotification notification)
        {
            if (moveViewUp)
            {
                ScrollTheView(false);
            }
        }

        void ScrollTheView(bool move)
        {
            CGRect resetFrame = View.Frame;
            resetFrame.Y = 0;
            View.Frame = resetFrame;

            UIView.BeginAnimations(string.Empty, System.IntPtr.Zero);
            //UIView.SetAnimationDuration(0.3);

            CGRect frame = this.View.Frame;

            if (move)
            {
                frame.Y -= scroll_amount;
            }
            else
            {
                if (resetFrame.Y != 0)
                    frame.Y += scroll_amount;
                scroll_amount = 0;
            }

			isScrolled = !isScrolled;
            View.Frame = frame;
            UIView.CommitAnimations();
        }
    }
}
