using E7.iOS.Utils;
using Foundation;
using SciChart.iOS.Charting;
using System;
using UIKit;

namespace E7.iOS
{
    public partial class MedicionGraficasTension : UIViewController
    {
        SCIChartSurface _surface;
        SCIVerticallyStackedMountainsCollection _mountainCollection;
        Alert alert;

        public MedicionGraficasTension() : base("MedicionGraficasTension", null)
        {
        }

        public MedicionGraficasTension (IntPtr handle) : base (handle) { }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            // Perform any additional setup after loading the view, typically from a nib.

            alert = new Alert();
            EdgesForExtendedLayout = UIRectEdge.None;
            _surface = new SCIChartSurface();

            _surface.TranslatesAutoresizingMaskIntoConstraints = true;

            SCIThemeManager.ApplyTheme(_surface, SCIThemeManager.SCIChart_Bright_SparkStyleKey);

            _surface.XAxes.Add(ChartCreator.xDateAxis());
            var y = ChartCreator.yAxis("Tensión (V)");
            y.LabelProvider = new ThousandsLabelProvider();
            _surface.YAxes.Add(y);

            this.View.AddSubview(_surface);

            GetReporte();
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewDidAppear(animated);

            _surface.Frame = this.View.Bounds;
        }

        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
            // Release any cached data, images, etc that aren't in use.
        }

        void GetReporte()
        {
            try
            {
                if (MedicionGraficasEActiva.resultadoMedicionYGraficas == null)
                    return;

                var resultado = MedicionGraficasEActiva.resultadoMedicionYGraficas;

                var ds1 = new XyDataSeries<DateTime, double> { SeriesName = "Tensión (V)", AcceptUnsortedData = true };

                foreach (var x in resultado)
                {
                    if (x.Tension == "")
                        continue;
                    ds1.Append(x.FechayHora, double.Parse(x.Tension));
                }

                var series1 = ChartCreator.GetMountainRenderableSeries(ds1, new SCISolidPenStyle(UIColor.FromRGB(47, 174, 249), 2), new SCISolidBrushStyle(UIColor.FromRGBA(47, 174, 249, 150)));

                _mountainCollection = new SCIVerticallyStackedMountainsCollection();
                _mountainCollection.Add(series1);

                _surface.RenderableSeries.Add(_mountainCollection);
                _surface.ChartModifiers.Add(new SCICursorModifier());
                _surface.ChartModifiers.Add(new SCIPinchZoomModifier()
                {
                    Direction = SCIDirection2D.XDirection
                });
                _surface.ChartModifiers.Add(new SCIZoomExtentsModifier());
            }
            catch (Exception e)
            {
                var a = alert.ShowMessage("Error", e.Message);
                a.Show();
            }
        }
    }
}