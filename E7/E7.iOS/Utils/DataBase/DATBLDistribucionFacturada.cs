﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using E7.Models;
using SQLite;

namespace E7.iOS.Utils.DataBase
{
    public class DATBLDistribucionFacturada
    {
        string folder = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);

        public Task<bool> CreateTableDF()
        {
            return Task.Run(() =>
            {
                try
                {
                    using (var connection = new SQLiteConnection(System.IO.Path.Combine(folder, "Extranet.db")))
                    {
                        connection.CreateTable<TBLDistribucionFacturada>();
                        return true;
                    }
                }
                catch (SQLiteException ex)
                {
                    //Log.Info("SQLiteEx", ex.Message);
                    Debug.WriteLine(ex);
                    return false;
                }
            });
        }

        public Task<bool> InsertTableDF(TBLDistribucionFacturada df)
        {
            return Task.Run(() =>
            {
                try
                {
                    using (var connection = new SQLiteConnection(System.IO.Path.Combine(folder, "Extranet.db")))
                    {
                        var list = connection.Query<TBLDistribucionFacturada>("SELECT * FROM TBLDistribucionFacturada WHERE Mes=? AND Anio=? AND CodCliente=? AND CodPuntoFacturacion=? AND Concepto=?", df.Mes, df.Anio, df.CodCliente, df.CodPuntoFacturacion, df.Concepto);
                        if (list.Count == 0)
                        {
                            connection.Insert(df);
                        }
                        return true;
                    }
                }
                catch (SQLiteException ex)
                {
                    //Log.Info("SQLiteEx", ex.Message);
                    Debug.WriteLine(ex);
                    return false;
                }
            });
        }

        public Task<List<TBLDistribucionFacturada>> SelectTableDF(TBLDistribucionFacturada df)
        {
            return Task.Run(() =>
            {
                try
                {
                    List<TBLDistribucionFacturada> list = new List<TBLDistribucionFacturada>();
                    using (var connection = new SQLiteConnection(System.IO.Path.Combine(folder, "Extranet.db")))
                    {
                        list = connection.Query<TBLDistribucionFacturada>("SELECT * FROM TBLDistribucionFacturada WHERE Mes=? AND Anio=? AND CodCliente=? AND CodPuntoFacturacion=?", df.Mes, df.Anio, df.CodCliente, df.CodPuntoFacturacion);
                        return list;
                    }
                }
                catch (SQLiteException ex)
                {
                    //Log.Info("SQLiteEx", ex.Message);
                    Debug.WriteLine(ex);
                    return null;
                }
            });
        }

        public Task<List<TBLDistribucionFacturada>> ValidateTableDF()
        {
            return Task.Run(() =>
            {
                try
                {
                    List<TBLDistribucionFacturada> list = new List<TBLDistribucionFacturada>();
                    using (var connection = new SQLiteConnection(System.IO.Path.Combine(folder, "Extranet.db")))
                    {
                        list = connection.Table<TBLDistribucionFacturada>().ToList();
                        return list;
                    }
                }
                catch (SQLiteException ex)
                {
                    //Log.Info("SQLiteEx", ex.Message);
                    Debug.WriteLine(ex);
                    return null;
                }
            });
        }

        public Task<bool> DeleteTableDF(DateTime date)
        {
            return Task.Run(() =>
            {
                try
                {
                    using (var connection = new SQLiteConnection(System.IO.Path.Combine(folder, "Extranet.db")))
                    {
                        connection.Query<TBLDistribucionFacturada>("DELETE FROM TBLDistribucionFacturada WHERE Fecha<?", date.Ticks);
                        return true;
                    }
                }
                catch (SQLiteException ex)
                {
                    //Log.Info("SQLiteEx", ex.Message);
                    Debug.WriteLine(ex);
                    return false;
                }
            });
        }
    }
}
