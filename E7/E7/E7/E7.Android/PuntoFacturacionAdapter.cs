﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;


namespace E7.Droid
{
    public class PuntoFacturacionAdapter : BaseAdapter
    {
        private Context c;
        private JavaList<PuntoFacturacion> puntosFacturacion;
        private LayoutInflater inflater;

        public PuntoFacturacionAdapter(Context c, JavaList<PuntoFacturacion> puntosFacturacion)
        {
            this.c = c;
            this.puntosFacturacion = puntosFacturacion;
        }
        public override Java.Lang.Object GetItem(int position)
        {
            return puntosFacturacion.Get(position);
        }
        public override long GetItemId(int position)
        {
            return position;
        }
        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            if (inflater == null)
            {
                inflater = (LayoutInflater) c.GetSystemService(Context.LayoutInflaterService);
            }
            if (convertView == null)
            {
                convertView = inflater.Inflate(Resource.Layout.recursoSpinner,parent,false);
            }

            TextView cod = convertView.FindViewById<TextView>(Resource.Id.cod1);
            TextView nomb = convertView.FindViewById<TextView>(Resource.Id.nom1);
            cod.Text = puntosFacturacion[position].IdPuntoFacturacion;
            nomb.Text = puntosFacturacion[position].DescPuntoFacturacion;
            return convertView;
        }
        public override int Count
        {
            get { return puntosFacturacion.Size(); }
        }
    }

}