using Foundation;
using System;
using UIKit;
using SciChart.iOS.Charting;
using CoreGraphics;
using E7.iOS.Utils;
using E7.Services;
using System.Collections.Generic;
using E7.Models;
using E7.iOS.Utils.DataBase;
using System.Linq;

namespace E7.iOS
{
    public partial class EnergiaActivaController : UIViewController
    {
        SCIChartSurface _surface;
        SCIVerticallyStackedColumnsCollection _columnCollection;
        RestClient _client;
        XyDataSeries<double, double> _ds1;
        SCIStackedColumnRenderableSeries _series1;
        LoadingOverlay _loadPop;
        public static List<BEConsumoFacturado> resultadoConsumoFacturado { get; set; }

        public EnergiaActivaController() : base("EnergiaActivaController", null)
        {
        }

        public EnergiaActivaController(IntPtr b) : base(b) { }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            // Perform any additional setup after loading the view, typically from a nib.

            EdgesForExtendedLayout = UIRectEdge.None;
            _loadPop = new LoadingOverlay(UIScreen.MainScreen.Bounds);
            _client = new RestClient();
            _surface = new SCIChartSurface();
            _columnCollection = new SCIVerticallyStackedColumnsCollection();
            resultadoConsumoFacturado = new List<BEConsumoFacturado>();

            SCIThemeManager.ApplyTheme(_surface, SCIThemeManager.SCIChart_Bright_SparkStyleKey);
            _surface.TranslatesAutoresizingMaskIntoConstraints = true;

            var x = ChartCreator.xAxis();
            x.LabelProvider = new MonthLabelProvider();
            _surface.XAxes.Add(x);

            var y = ChartCreator.yAxis("Energía Activa (kWh)");
            y.LabelProvider = new MillionsLabelProvider();
            _surface.YAxes.Add(y);

            this.View.AddSubview(_surface);

            GetReporte();
        }

        public override void ViewDidAppear(bool animated)
        {
            base.ViewDidAppear(animated);

            _surface.Frame = this.View.Bounds;
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);
        }

        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
            // Release any cached data, images, etc that aren't in use.
        }

        async void GetReporte()
        {
            View.Add(_loadPop);
            var main = Application.GetInstance();
            var consumoFacturado = main.consumoFacturado;
            List<BEConsumoFacturado> resultado;
            List<TBLConsumoFacturado> resultado1;
            BLTBLConsumoFacturado conexion1 = new BLTBLConsumoFacturado();
            TBLConsumoFacturado data = new TBLConsumoFacturado();
            string MensajeErrorCF;
            Alert alert = new Alert();

            var online = _client.CheckConnection();
			var _modooff = "";
            if (online)
                _modooff = "0";
            else
                _modooff = main.ModoOff;
                
            if (_modooff == "0")
            {
                resultado = await _client.Reporte_ConsumoFacturado(consumoFacturado.Anio, consumoFacturado.ClienteID.ToString(), consumoFacturado.PuntoFID.ToString());
                MensajeErrorCF = _client.ResultadoFinalRCF;
                if (resultado == null)
                {
                    var a = alert.ShowMessage("Alerta", MensajeErrorCF);
                    a.Clicked += A_Clicked;
                    a.Show();
                }
                else if (resultado.Count == 0)
                {
                    var a = alert.ShowMessage("Alerta", "No hay datos.");
                    a.Clicked += A_Clicked;
                    a.Show();
                }
                else
                {
                    resultadoConsumoFacturado = resultado;
                    var EnergiaActiva = resultado.OrderBy(y => y.Fecha).Select(x => (Convert.ToDouble(x.EnergiaActiva))).ToArray();
                    var Potencia = resultado.OrderBy(y => y.Fecha).Select(x => (Convert.ToDouble(x.Potencia))).ToArray();
                    var Fecha = resultado.OrderBy(y => y.Fecha).Select(x => (Convert.ToDouble(x.Fecha.Month))).ToArray();

                    _ds1 = new XyDataSeries<double, double> { SeriesName = "Energía Activa (kWh)" };

                    for (var i = 0; i < EnergiaActiva.Count(); i++)
                    {
                        _ds1.Append(Fecha[i], EnergiaActiva[i]);
                    }

                    _series1 = ChartCreator.GetColumnRenderableSeries(_ds1, new SCISolidPenStyle(UIColor.FromRGB(47, 174, 249), 2), new SCISolidBrushStyle(UIColor.FromRGB(47, 174, 249)));

                    _columnCollection.Add(_series1);

                    _surface.RenderableSeries.Add(_columnCollection);
                    _surface.ChartModifiers.Add(new SCICursorModifier());
                }
                _loadPop.Hide();
            }
            else if (_modooff != "0")
            {
                data.Anio = consumoFacturado.Anio;
                data.CodCliente = consumoFacturado.ClienteID.ToString();
                data.CodPuntoFacturacion = consumoFacturado.PuntoFID.ToString();
                resultado1 = await conexion1.Reporte_ConsumoFacturado(data);
                MensajeErrorCF = "Error al momento de leer los datos guardados";
                if (resultado1 == null)
                {
                    var a = alert.ShowMessage("Alerta", MensajeErrorCF);
                    a.Clicked += A_Clicked;
                    a.Show();
                }
                else if (resultado1.Count == 0)
                {
                    resultado1 = await conexion1.Reporte_ConsumoFacturadoTodo(data);
                    if (resultado1 == null)
                    {
                        var a = alert.ShowMessage("Alerta", "No hay datos sincronizados.");
                        a.Clicked += A_Clicked;
                        a.Show();
                    }
                    else if (resultado1.Count == 0)
                    {
                        var a = alert.ShowMessage("Alerta", "No hay datos sincronizados.");
                        a.Clicked += A_Clicked;
                        a.Show();
                    }
                    else
                    {
                        string _meses = "";
                        if (_modooff == "1") { _meses = " mes."; } else { _meses = " meses."; }
                        var a = alert.ShowMessage("Alerta", "Usted solo tiene datos sincronizados de " + _modooff + _meses);
                        a.Clicked += (sender, e) => {
                            DateTime ahora = DateTime.Now;
                            var _ultimoMes = resultado1.OrderByDescending(x => x.Fecha).First(x => (x.Fecha < ahora.AddMonths(1))).Fecha;
                            DateTime busqueda = _ultimoMes.AddMonths(-(Convert.ToInt32(_modooff)));
                            DateTime _busqueda = new DateTime(busqueda.Year, busqueda.Month, 1);

                            var EnergiaActiva = resultado1.Where(x => x.Fecha >= _busqueda).OrderBy(y => y.Fecha).Select(x => (Convert.ToDouble(x.EnergiaActiva))).ToArray();
                            var Potencia = resultado1.Where(x => x.Fecha >= _busqueda).OrderBy(y => y.Fecha).Select(x => (Convert.ToDouble(x.Potencia))).ToArray();
                            var Fecha = resultado1.Where(x => x.Fecha >= _busqueda).OrderBy(y => y.Fecha).Select(x => (Convert.ToDouble(x.Fecha.Month))).ToArray();

                            _ds1 = new XyDataSeries<double, double> { SeriesName = "Energía Activa (kWh)" };

                            for (var i = 0; i < EnergiaActiva.Count(); i++)
                            {
                                _ds1.Append(Fecha[i], EnergiaActiva[i]);
                                resultadoConsumoFacturado.Add(new BEConsumoFacturado()
                                {
                                    EnergiaActiva = EnergiaActiva[i].ToString(),
                                    Potencia = Potencia[i].ToString(),
                                    Mes = Fecha[i].ToString()
                                });
                            }

                            _series1 = ChartCreator.GetColumnRenderableSeries(_ds1, new SCISolidPenStyle(UIColor.FromRGB(47, 174, 249), 2), new SCISolidBrushStyle(UIColor.FromRGB(47, 174, 249)));

                            _columnCollection.Add(_series1);

                            _surface.RenderableSeries.Add(_columnCollection);
                            _surface.ChartModifiers.Add(new SCICursorModifier());
                        };
                        a.Show();
                    }
                }
                else
                {
                    string _meses = "";
                    if (_modooff == "1") { _meses = " mes."; } else { _meses = " meses."; }
                    var a = alert.ShowMessage("Alerta", "Usted solo tiene datos sincronizados de " + _modooff + _meses);
                    a.Clicked += (sender, e) => {
                        DateTime ahora = DateTime.Now;
                        var _ultimoMes = resultado1.OrderByDescending(x => x.Fecha).First(x => (x.Fecha < ahora.AddMonths(1))).Fecha;
                        DateTime busqueda = _ultimoMes.AddMonths(-(Convert.ToInt32(_modooff)));
                        DateTime _busqueda = new DateTime(busqueda.Year, busqueda.Month, 1);

                        var EnergiaActiva = resultado1.Where(x => x.Fecha >= _busqueda).OrderBy(y => y.Fecha).Select(x => (Convert.ToDouble(x.EnergiaActiva))).ToArray();
                        var Potencia = resultado1.Where(x => x.Fecha >= _busqueda).OrderBy(y => y.Fecha).Select(x => (Convert.ToDouble(x.Potencia))).ToArray();
                        var Fecha = resultado1.Where(x => x.Fecha >= _busqueda).OrderBy(y => y.Fecha).Select(x => (Convert.ToDouble(x.Fecha.Month))).ToArray();

                        _ds1 = new XyDataSeries<double, double> { SeriesName = "Energía Activa (kWh)" };

                        resultadoConsumoFacturado.Clear();
                        for (var i = 0; i < EnergiaActiva.Count(); i ++)
                        {
                            _ds1.Append(Fecha[i], EnergiaActiva[i]);
                            resultadoConsumoFacturado.Add(new BEConsumoFacturado()
                            {
                                EnergiaActiva = EnergiaActiva[i].ToString(),
                                Potencia = Potencia[i].ToString(),
                                Mes = Fecha[i].ToString()
                            });
                        }

                        _series1 = ChartCreator.GetColumnRenderableSeries(_ds1, new SCISolidPenStyle(UIColor.FromRGB(47, 174, 249), 2), new SCISolidBrushStyle(UIColor.FromRGB(47, 174, 249)));

                        _columnCollection.Add(_series1);

                        _surface.RenderableSeries.Add(_columnCollection);
                        _surface.ChartModifiers.Add(new SCICursorModifier());
                    };
                    a.Show();
                }
                _loadPop.Hide();
            }
        }

        void A_Clicked(object sender, UIButtonEventArgs e)
        {
            try
            {
				this.NavigationController.PopViewController(true);
            }
            catch {}
        }
    }
}