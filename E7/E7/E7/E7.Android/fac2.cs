﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using V7Toolbar = Android.Support.V7.Widget.Toolbar;
using Android.Support.V7.App;
using Android.Support.V4.Widget;
using Android.Support.Design.Widget;

namespace E7.Droid
{
    [Activity(Label = "fac2", Theme = "@style/Theme.DesignDemo")]
    public class fac2 : AppCompatActivity
    {
        DrawerLayout drawerLayout;
        NavigationView navigationView;
        TextView _dateDisplay;
        Button _dateSelectButton;

        ClientesAdapter adapter_c;
        PuntoFacturacionAdapter adapter_p;
        JavaList<Clientes> clientes;
        JavaList<PuntoFacturacion> puntoFacturacion;
        Spinner CodCliente, IdPuntoFacturacion;
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            // Create your application here

            SetContentView(layoutResID: Resource.Layout.fac2);
            _dateDisplay = FindViewById<TextView>(Resource.Id.editText1);
            _dateSelectButton = FindViewById<Button>(Resource.Id.button1);
            _dateSelectButton.Click += DateSelect_OnClick;
            var toolbar = FindViewById<V7Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            SupportActionBar.SetDisplayShowTitleEnabled(false);
            SupportActionBar.SetHomeButtonEnabled(true);
            SupportActionBar.SetHomeAsUpIndicator(Resource.Drawable.menu);
            drawerLayout = FindViewById<DrawerLayout>(Resource.Id.drawer_layout);
            navigationView = FindViewById<NavigationView>(Resource.Id.nav_view);


            //Filtros

            CodCliente = FindViewById<Spinner>(Resource.Id.spinner1);
            IdPuntoFacturacion = FindViewById<Spinner>(Resource.Id.spinner2);


            CodCliente.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs>(CodCliente_ItemSelected);
            IdPuntoFacturacion.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs>(IdPuntoFacturacion_ItemSelected);

            ListarClientes();

            void ListarClientes()
            {
                clientes = COLogin.ListarClientes();
                adapter_c = new ClientesAdapter(this, clientes);
                CodCliente.Adapter = adapter_c;
                CodCliente.ItemSelected += CodCliente_ItemSelected;
            }

            void CodCliente_ItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
            {
                //string toast = string.Format("Cliente seleccionado: {0}", clientes[e.Position].NomCliente);
                //Toast.MakeText(this, toast, ToastLength.Short).Show();
                var _codCliente = clientes[e.Position].CodCliente;
                ListarPuntoFacturacion(_codCliente);
            }
            void ListarPuntoFacturacion(string _codCliente)
            {
                puntoFacturacion = COLogin.ListarPuntoFacturacion(_codCliente);
                adapter_p = new PuntoFacturacionAdapter(this, puntoFacturacion);
                IdPuntoFacturacion.Adapter = adapter_p;
                IdPuntoFacturacion.ItemSelected += IdPuntoFacturacion_ItemSelected;
            }
            void IdPuntoFacturacion_ItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
            {
                //string toast = string.Format("Punto de Facturación seleccionado: {0}", puntoFacturacion[e.Position].DescPuntoFacturacion);
                //Toast.MakeText(this, toast, ToastLength.Short).Show();
            }
        }

        void DateSelect_OnClick(object sender, EventArgs eventArgs)
        {
            DatePickerFragment frag = DatePickerFragment.NewInstance(delegate (DateTime time)
            {
                _dateDisplay.Text = time.ToShortDateString();
            });
            frag.Show(FragmentManager, DatePickerFragment.TAG);
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Android.Resource.Id.Home:
                    drawerLayout.OpenDrawer(Android.Support.V4.View.GravityCompat.Start);
                    return true;
            }
            return base.OnOptionsItemSelected(item);
        }
    }
}

