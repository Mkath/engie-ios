﻿using System;
using UIKit;
using E7.iOS.Utils;
using Foundation;

namespace E7.iOS
{
    public partial class LoginController : KeyboardBaseController
    {
        SaveAccountInfo _saveAccount;
        bool _keepSession = false;

        public LoginController(IntPtr b) : base(b) { }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            // Perform any additional setup after loading the view, typically from a nib.

            //btnOlvidoContrasena.Hidden = true;
            //stackMantenerSesion.Hidden = true;

            this.View.AddSubview(imgLogo);
            if (this.View.Frame.Height <= 480)
            {
                NSLayoutConstraint.Create(imgLogo, NSLayoutAttribute.Top,
                                          NSLayoutRelation.Equal, this.TopLayoutGuide, NSLayoutAttribute.Bottom,
                                          1, 110).Active = true;
            }

            _saveAccount = new SaveAccountInfo();

            #if DEBUG
            tfUsuario.Text = "vitpower2";
            tfPassword.Text = "12345";
            //tfUsuario.Text = "cliente_pruebapp";
            //tfPassword.Text = "Engie1234";
            //tfUsuario.Text = "engie_comercial";
            //tfPassword.Text = "Engie2018$";
            #endif

            UITapGestureRecognizer chk = null;
            Action changeChkImgAction = () =>
            {
                if (!_keepSession)
                {
					_keepSession = !_keepSession;
                    chkImage.Image = new UIImage("checkbox_checked.png"); // keepSession = true
                }
                else
                {
					_keepSession = !_keepSession;
                    chkImage.Image = new UIImage("checkbox_unchecked.png"); // keepSession = false
                }
            };
            chk = new UITapGestureRecognizer(changeChkImgAction);
            chkImage.UserInteractionEnabled = true;
            chkImage.AddGestureRecognizer(chk);

            if (!string.IsNullOrEmpty(_saveAccount.UserName))
            {
                tfUsuario.Text = _saveAccount.UserName;
                tfPassword.Text = _saveAccount.Password;
				IniciarSesion();
            }
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);

            tfUsuario.BackgroundColor = UIColor.FromRGBA(0, 0, 0, 256);
			tfUsuario.TextColor = UIColor.White;
            tfUsuario.Layer.BorderColor = UIColor.White.CGColor;
            tfUsuario.Layer.BorderWidth = 1.5f;
            tfUsuario.Layer.CornerRadius = 13.0f;
            tfUsuario.AttributedPlaceholder = new NSAttributedString("Usuario", null, UIColor.White);

            tfPassword.BackgroundColor = UIColor.FromRGBA(0, 0, 0, 256);
            tfPassword.TextColor = UIColor.White;
            tfPassword.Layer.BorderColor = UIColor.White.CGColor;
            tfPassword.Layer.BorderWidth = 1.5f;
            tfPassword.Layer.CornerRadius = 13.0f;
            tfPassword.AttributedPlaceholder = new NSAttributedString("Contraseña", null, UIColor.White);

            btnIngresar.BackgroundColor = UIColor.FromRGB(2, 73, 125);
            btnIngresar.Layer.CornerRadius = 18.0f;
            btnIngresar.Layer.ShadowOpacity = 0.8f;
            btnIngresar.Layer.ShadowOffset = new CoreGraphics.CGSize(0.0f, 3.0f);
        }

        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
            // Release any cached data, images, etc that aren't in use.
        }

        partial void UIButton48401_TouchUpInside(UIButton sender)
        {
            // Open root view
            IniciarSesion();
        }

        async void IniciarSesion()
        {
            tfUsuario.Text = tfUsuario.Text.Trim();
            if (string.IsNullOrEmpty(tfUsuario.Text) || string.IsNullOrEmpty(tfPassword.Text))
            {
                ShowMessage("Error", "Por favor ingrese usuario y contraseña");
                return;
            }
            _loadPop = new LoadingOverlay(UIScreen.MainScreen.Bounds);
            View.Add(_loadPop);
            var online = _client.CheckConnection();
            if (online)
            {
                var response = await _client.AutenticarUsuario(tfUsuario.Text, tfPassword.Text);
                if (response == null)
                {

                }
                if (response.ValidarOperacion == "true")
                {
                    if (_keepSession)
                        _saveAccount.SaveCredentials(tfUsuario.Text, tfPassword.Text);
                    
                    var main = Application.GetInstance();
                    main.usuarioInterno = response.UsuarioInterno;
                    main.nombreCompleto = response.NombreCompleto;
                    main.clientes = await _client.Usuario_Cliente(main.usuarioInterno);

                    if (main.clientes == null)
                    {
                        _loadPop.Hide();
                        ShowMessage("No se le ha asignado ningún cliente.", "Por favor contáctese con: soporteapp@pe.engie.com");
                        return;
                    }

                    if (main.clientes.Count == 0)
                    {
                        _loadPop.Hide();
                        ShowMessage("No se le ha asignado ningún cliente.", "Por favor contáctese con: soporteapp@pe.engie.com");
                        return;
                    }

                    var plist = NSUserDefaults.StandardUserDefaults;
                    main.ModoOff = plist.StringForKey("ModoOff");
                    main.UltimaSincronizacion = plist.StringForKey("UltimaSincronizacion");

                    (UIApplication.SharedApplication.Delegate as AppDelegate).SetRootViewController(new RootViewController(), true);
                }
                else
                {
                    ShowMessage("Error", "Usuario incorrecto");
                }
            }
            else
            {
                ShowMessage("Error", "Verifique su conexión a internet.");
            }
            _loadPop.Hide();
        }
    }
}
