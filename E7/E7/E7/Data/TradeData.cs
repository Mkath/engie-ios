﻿using System;
using System.Collections.Generic;
using System.Text;

namespace E7.Data
{
    public class TradeData
    {
        public DateTime TradeDate { get; set; }
        public double TradePrice { get; set; }
        public double TradeSize { get; set; }
    }
}
