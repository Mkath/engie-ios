﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Net.Http;

namespace E7.Droid
{
    public class COReporte_Facturacion
    {
        public List<GraficoHistoricos> Reporte_HistoricoFacturado(string FechaDesde, string FechaHasta, string CodCliente, string CodPuntoFacturacion, string Moneda)
        {
            #region Método Get Listar Consumo Facturado
            string Api = "http://appwebservice.engie-energia.pe/appmovil/Api/SReportes/";

            List<GraficoHistoricos> entidad = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Api);

                // Realizar la petición GET
                HttpResponseMessage response = client.GetAsync("ReporteF_HistoricoFacturado?FechaDesde=" + FechaDesde + "&FechaHasta=" + FechaHasta + "&CodCliente=" + CodCliente + "&CodPuntoFacturacion=" + CodPuntoFacturacion + "&Moneda=" + Moneda).Result;

                if (response.IsSuccessStatusCode)
                {
                    var res = response.Content.ReadAsStringAsync().Result;
                    entidad = (List<GraficoHistoricos>)Newtonsoft.Json.JsonConvert.DeserializeObject<List<GraficoHistoricos>>(res);
                }
            }
            return entidad;
            #endregion  
        }
        public List<GraficoHistoricos> Reporte_HistoricoPrecio(string FechaDesde, string FechaHasta, string CodCliente, string CodPuntoFacturacion, string Moneda)
        {
            #region Método Get Listar Consumo Facturado
            string Api = "http://appwebservice.engie-energia.pe/appmovil/Api/SReportes/";

            List<GraficoHistoricos> entidad = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Api);

                // Realizar la petición GET
                HttpResponseMessage response = client.GetAsync("ReporteF_HistoricoPrecio?FechaDesde=" + FechaDesde + "&FechaHasta=" + FechaHasta + "&CodCliente=" + CodCliente + "&CodPuntoFacturacion=" + CodPuntoFacturacion + "&Moneda=" + Moneda).Result;

                if (response.IsSuccessStatusCode)
                {
                    var res = response.Content.ReadAsStringAsync().Result;
                    entidad = (List<GraficoHistoricos>)Newtonsoft.Json.JsonConvert.DeserializeObject<List<GraficoHistoricos>>(res);
                }
            }
            return entidad;
            #endregion  
        }

        public List<GraficoHistoricos> Reporte_DistribucionFacturacion(string Fecha, string CodCliente, string CodPuntoFacturacion, string Moneda)
        {
            #region Método Get Listar Consumo Facturado
            string Api = "http://appwebservice.engie-energia.pe/appmovil/Api/SReportes/";

            List<GraficoHistoricos> entidad = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Api);

                // Realizar la petición GET
                HttpResponseMessage response = client.GetAsync("ReporteF_DistribucionFacturacion?Fecha=" + Fecha  + "&CodCliente=" + CodCliente + "&CodPuntoFacturacion=" + CodPuntoFacturacion + "&Moneda=" + Moneda).Result;

                if (response.IsSuccessStatusCode)
                {
                    var res = response.Content.ReadAsStringAsync().Result;
                    entidad = (List<GraficoHistoricos>)Newtonsoft.Json.JsonConvert.DeserializeObject<List<GraficoHistoricos>>(res);
                }
            }
            return entidad;
            #endregion  
        }
    }
}