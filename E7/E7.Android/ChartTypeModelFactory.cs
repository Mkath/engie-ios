﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using SciChart.Charting.Visuals.RenderableSeries;
using SciChart.Charting.Model.DataSeries;
using SciChart.Drawing.Common;
using SciChart.Charting.Visuals.PointMarkers;

namespace E7.Droid
{
    class ChartTypeModelFactory
    {
        //Reporte de Consumo - Consumo Facturado
        public static ChartTypeModel EnergiaActivaStackedColumn(Context context, double[] EnergiaActiva, double[] Mes, uint fillColor, uint strokeColor)
        {
            var collection = new HorizontallyStackedColumnsCollection();

            var dataSeries = new XyDataSeries<double, double> { SeriesName = "Energía Activa" };
            for (var i = 0; i < EnergiaActiva.Count(); i++)
            {
                dataSeries.Append(Mes[i], EnergiaActiva[i]);
            }

            var rSeries = new StackedColumnRenderableSeries
            {
                DataSeries = dataSeries,
                StrokeStyle = new SolidPenStyle(fillColor,6),
                FillBrushStyle = new LinearGradientBrushStyle(0, 0, 0, 0, fillColor, strokeColor)
            };
            collection.Add(rSeries);

            var name = "Energía Activa";
            return new ChartTypeModel(collection, name);
        }
        public static ChartTypeModel PotenciaStackedLine(double[] Potencia, double[] Mes, uint fillColor)
        {
            var collection = new VerticallyStackedMountainsCollection();

            var dataSeries = new XyDataSeries<double, double> { SeriesName = "Potencia" };
            for (var i = 0; i < Potencia.Count(); i++)
            {
                dataSeries.Append(Mes[i], Potencia[i]);
            }
            var rSeries = new StackedMountainRenderableSeries
            {
                DataSeries = dataSeries,
                StrokeStyle = new SolidPenStyle(fillColor, 6),
                AreaStyle = new LinearGradientBrushStyle(1, 1, 0, 1, 0, 0),
                PointMarker = new EllipsePointMarker
                {
                    Width = 10,
                    Height = 10,
                    FillStyle = new SolidBrushStyle(0xff7cb5ec),
                    StrokeStyle = new SolidPenStyle(fillColor)
                }

            };
            collection.Add(rSeries);

            var name = "Potencia";
            return new ChartTypeModel(collection, name);
        }
        
        //Reporte de Consumo - Medición y Grafica
        public static ChartTypeModel EnergiaActivaStackedMountains(Context context, double[] EnergiaActiva, double[] FechayHora, bool isOneHundredPercent, uint fillColor, uint strokeColor)
        {
            var collection = new VerticallyStackedMountainsCollection { IsOneHundredPercent = isOneHundredPercent };

            var dataSeries = new XyDataSeries<double, double> { SeriesName = "Energía Activa" };
            for (var i = 0; i < EnergiaActiva.Count(); i++)
            {
                dataSeries.Append(FechayHora[i], EnergiaActiva[i]);
            }
            var rSeries = new StackedMountainRenderableSeries
            {
                DataSeries = dataSeries,
                StrokeStyle = new SolidPenStyle(fillColor,5),
                AreaStyle = new LinearGradientBrushStyle(0, 0, 0, 1, fillColor, strokeColor),
                PointMarker = new SquarePointMarker
                {
                    Width = 10,
                    Height = 10,
                    FillStyle = new SolidBrushStyle(fillColor),
                    StrokeStyle = new SolidPenStyle(fillColor)
                }
            };
            collection.Add(rSeries);


            var name = "Energía Activa";
            return new ChartTypeModel(collection, name);
        }
        public static ChartTypeModel PotenciaStackedMountains(Context context, double[] Potencia, double[] FechayHora, bool isOneHundredPercent, uint fillColor, uint strokeColor)
        {
            var collection = new VerticallyStackedMountainsCollection { IsOneHundredPercent = isOneHundredPercent };

            var dataSeries = new XyDataSeries<double, double> { SeriesName = "Potencia" };
            for (var i = 0; i < Potencia.Count(); i++)
            {
                dataSeries.Append(FechayHora[i], Potencia[i]);
            }

            var rSeries = new StackedMountainRenderableSeries
            {
                DataSeries = dataSeries,
                StrokeStyle = new SolidPenStyle(fillColor,5),
                AreaStyle = new LinearGradientBrushStyle(1, 1, 0, 1, 0, 0),
                PointMarker = new SquarePointMarker
                {
                    Width = 10,
                    Height = 10,
                    FillStyle = new SolidBrushStyle(fillColor),
                    StrokeStyle = new SolidPenStyle(fillColor)
                }
            };
            collection.Add(rSeries);


            var name = "Potencia";
            return new ChartTypeModel(collection, name);
        }
        public static ChartTypeModel EnergiaReactivaStackedMountains(Context context, double[] EnergiaReactiva, double[] FechayHora, bool isOneHundredPercent, uint fillColor, uint strokeColor)
        {
            var collection = new VerticallyStackedMountainsCollection { IsOneHundredPercent = isOneHundredPercent };

            var dataSeries = new XyDataSeries<double, double> { SeriesName = "Energía Reactiva" };
            for (var i = 0; i < EnergiaReactiva.Count(); i++)
            {
                dataSeries.Append(FechayHora[i], EnergiaReactiva[i]);
            }

            var rSeries = new StackedMountainRenderableSeries
            {
                DataSeries = dataSeries,
                StrokeStyle = new SolidPenStyle(fillColor,5),
                AreaStyle = new LinearGradientBrushStyle(0, 0, 0, 1, fillColor, strokeColor),
                PointMarker = new SquarePointMarker
                {
                    Width = 10,
                    Height = 10,
                    FillStyle = new SolidBrushStyle(fillColor),
                    StrokeStyle = new SolidPenStyle(fillColor)
                }

            };
            collection.Add(rSeries);

            var name = "Energía Reactiva";
            return new ChartTypeModel(collection, name);
        }

    }
}