﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace E7.Droid
{
    [Activity(Label = "fac3", Theme = "@style/Theme.DesignDemo")]
    public class fac3 : Activity
    {
        ClientesAdapter adapter_c;
        PuntoFacturacionAdapter adapter_p;
        JavaList<Clientes> clientes;
        JavaList<PuntoFacturacion> puntoFacturacion;
        Spinner CodCliente, IdPuntoFacturacion, Moneda;
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            // Create your application here

            SetContentView(layoutResID: Resource.Layout.fac3);

            //Filtros

            CodCliente = FindViewById<Spinner>(Resource.Id.spinner1);
            IdPuntoFacturacion = FindViewById<Spinner>(Resource.Id.spinner2);
            Moneda = FindViewById<Spinner>(Resource.Id.spinner3);


            CodCliente.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs>(CodCliente_ItemSelected);
            IdPuntoFacturacion.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs>(IdPuntoFacturacion_ItemSelected);
            Moneda.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs>(Moneda_ItemSelected);

            ListarClientes();
            ListarMoneda();

            void ListarMoneda()
            {
                string[] items = new[] { "--Seleccionar--", "USD", "PEN" };
                var Adaptador = new ArrayAdapter<string>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, items);
                Moneda.Adapter = Adaptador;
            }
            void Moneda_ItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
            {
                Spinner spinner = (Spinner)sender;
                //string toast = string.Format("Moneda seleccionada: {0}", spinner.GetItemAtPosition(e.Position));
                //Toast.MakeText(this, toast, ToastLength.Short).Show();
            }
            void ListarClientes()
            {
                clientes = COLogin.ListarClientes();
                adapter_c = new ClientesAdapter(this, clientes);
                CodCliente.Adapter = adapter_c;
                CodCliente.ItemSelected += CodCliente_ItemSelected;
            }

            void CodCliente_ItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
            {
                //string toast = string.Format("Cliente seleccionado: {0}", clientes[e.Position].NomCliente);
                //Toast.MakeText(this, toast, ToastLength.Short).Show();
                var _codCliente = clientes[e.Position].CodCliente;
                ListarPuntoFacturacion(_codCliente);
            }
            void ListarPuntoFacturacion(string _codCliente)
            {
                puntoFacturacion = COLogin.ListarPuntoFacturacion(_codCliente);
                adapter_p = new PuntoFacturacionAdapter(this, puntoFacturacion);
                IdPuntoFacturacion.Adapter = adapter_p;
                IdPuntoFacturacion.ItemSelected += IdPuntoFacturacion_ItemSelected;
            }
            void IdPuntoFacturacion_ItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
            {
                //string toast = string.Format("Punto de Facturación seleccionado: {0}", puntoFacturacion[e.Position].DescPuntoFacturacion);
                //Toast.MakeText(this, toast, ToastLength.Short).Show();
            }
        }
    }
}