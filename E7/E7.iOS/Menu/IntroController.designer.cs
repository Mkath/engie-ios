// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace E7.iOS
{
    [Register ("IntroController")]
    partial class IntroController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel lblSaludo { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel lblUsuario { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (lblSaludo != null) {
                lblSaludo.Dispose ();
                lblSaludo = null;
            }

            if (lblUsuario != null) {
                lblUsuario.Dispose ();
                lblUsuario = null;
            }
        }
    }
}