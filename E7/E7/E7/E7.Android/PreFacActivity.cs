﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using V7Toolbar = Android.Support.V7.Widget.Toolbar;
using Android.Support.V7.App;
using Android.Support.V4.Widget;
using Android.Support.Design.Widget;

namespace E7.Droid
{
    [Activity(Label = "PreFacActivity", Theme = "@style/Theme.DesignDemo")]
    public class PreFacActivity : AppCompatActivity
    {
        DrawerLayout drawerLayout;
        NavigationView navigationView;
        Button  btnc;
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            // Create your application here

            SetContentView(layoutResID: Resource.Layout.prefac);
            var toolbar = FindViewById<V7Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            SupportActionBar.SetDisplayShowTitleEnabled(false);
            SupportActionBar.SetHomeButtonEnabled(true);
            SupportActionBar.SetHomeAsUpIndicator(Resource.Drawable.menu);
            drawerLayout = FindViewById<DrawerLayout>(Resource.Id.drawer_layout);
            navigationView = FindViewById<NavigationView>(Resource.Id.nav_view);

            //btnf = FindViewById<Button>(Resource.Id.buttonc);
            btnc = FindViewById<Button>(Resource.Id.buttonc);
            var btn1 = FindViewById<ImageButton>(Resource.Id.imageButton1);
            var btn2 = FindViewById<ImageButton>(Resource.Id.imageButton2);
            var btn3 = FindViewById<ImageButton>(Resource.Id.imageButton3);
            //btn6 = FindViewById<Button>(Resource.Id.button6);

            btnc.Click += (sender, e) => Precosumo();
            //btnc.Click += (sender, e) => PreFacturacion();
            btn1.Click += (sender, e) => fac1();
            btn2.Click += (sender, e) => fac2();
            btn3.Click += (sender, e) => fac3();


            //void PreConsumo()
            //{
            //    StartActivity(typeof(PreConsActivity));
            //}
            void Precosumo()
            {
                StartActivity(typeof(PreConsActivity));
            }

            void fac1()
            {
                StartActivity(typeof(fac2));
            }
            void fac2()
            {
                StartActivity(typeof(fac1));
            }
            void fac3()
            {
                StartActivity(typeof(fac3));
            }
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Android.Resource.Id.Home:
                    drawerLayout.OpenDrawer(Android.Support.V4.View.GravityCompat.Start);
                    return true;
            }
            return base.OnOptionsItemSelected(item);
        }
    }
}