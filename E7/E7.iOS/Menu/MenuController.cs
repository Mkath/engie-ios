﻿using System;
using E7.iOS.Utils;
using UIKit;

namespace E7.iOS
{
    public partial class MenuController : BaseController
    {
        SaveAccountInfo saveAccount;
        public MenuController(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            saveAccount = new SaveAccountInfo();
        }

        partial void InicioButton_TouchUpInside(UIButton sender)
        {
            var introController = (IntroController)Storyboard.InstantiateViewController("IntroController");
            introController.NavigationItem.SetHidesBackButton(true, false);

            if (NavController.TopViewController as ContentController == null)
                NavController.PushViewController(introController, false);
            SidebarController.CloseMenu();
        }

        partial void ConsumoButton_TouchUpInside(UIButton sender)
        {
            var consumoController = (ConsumoController)Storyboard.InstantiateViewController("ConsumoController");
            consumoController.NavigationItem.SetHidesBackButton(true, false);

            if (NavController.TopViewController as ContentController == null)
                NavController.PushViewController(consumoController, false);
            SidebarController.CloseMenu();
        }

        partial void FacturacionButton_TouchUpInside(UIButton sender)
        {
            var facturacionController = (FacturacionController)Storyboard.InstantiateViewController("FacturacionController");
            facturacionController.NavigationItem.SetHidesBackButton(true, false);

            if (NavController.TopViewController as ContentController == null)
                NavController.PushViewController(facturacionController, false);
            SidebarController.CloseMenu();
        }

        partial void ModoOfflineButton_TouchUpInside(UIButton sender)
        {
            var modoOfflineController = (ModoOfflineController)Storyboard.InstantiateViewController("ModoOfflineController");
            modoOfflineController.NavigationItem.SetHidesBackButton(true, false);

            if (NavController.TopViewController as ContentController == null)
                NavController.PushViewController(modoOfflineController, false);
            SidebarController.CloseMenu();
        }

        partial void CerrarSesionButton_TouchUpInside(UIButton sender)
        {
            saveAccount.DeleteCredentials();

            var appDelegate = UIApplication.SharedApplication.Delegate as AppDelegate;

            var mainStoryboard = appDelegate.MainStoryboard;

            var loginPageViewController = appDelegate.GetViewController(mainStoryboard, "LoginController") as LoginController;

            appDelegate.SetRootViewController(loginPageViewController, true);
        }
    }
}

