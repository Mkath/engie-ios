﻿using System;
using System.Collections.Generic;
using System.Net.Http;

namespace E7.Droid
{
    public class COReporte_Consumo
    {
        public List<BEConsumoFacturado> Reporte_ConsumoFacturado(string Anio, string CodCliente, string CodPuntoFacturacion)
        {
            #region Método Get Listar Consumo Facturado
            string Api = "http://appwebservice.engie-energia.pe/appmovil/Api/SReportes/";

            List<BEConsumoFacturado> entidad = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Api);

                // Realizar la petición GET
                HttpResponseMessage response = client.GetAsync("ReporteC_ConsumoFacturado?Anio=" + Anio + "&CodCliente=" + CodCliente + "&CodPuntoFacturacion=" + CodPuntoFacturacion).Result;

                if (response.IsSuccessStatusCode)
                {
                    var res = response.Content.ReadAsStringAsync().Result;
                    entidad = (List<BEConsumoFacturado>)Newtonsoft.Json.JsonConvert.DeserializeObject<List<BEConsumoFacturado>>(res);
                }
            }
            return entidad;
            #endregion  
        }
        public List<BEMedicionGrafica> Reporte_MedidionGrafica(string FechaDesde,string FechaHasta, string CodCliente, string CodMedidor)
        {
            #region Método Get Listar Consumo Facturado
            string Api = "http://appwebservice.engie-energia.pe/appmovil/Api/SReportes/";

            List<BEMedicionGrafica> entidad = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Api);

                // Realizar la petición GET
                HttpResponseMessage response = client.GetAsync("ReporteC_MedicionyGrafica?FechaDesde=" + FechaDesde + "&FechaHasta=" + FechaHasta + "&CodCliente=" + CodCliente + "&CodMedidor=" + CodMedidor).Result;
                
                if (response.IsSuccessStatusCode)
                {
                    var res = response.Content.ReadAsStringAsync().Result;
                    entidad = (List<BEMedicionGrafica>)Newtonsoft.Json.JsonConvert.DeserializeObject<List<BEMedicionGrafica>>(res);
                }
            }
            return entidad;
            #endregion  
        }
    }
}