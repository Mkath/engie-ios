using Foundation;
using System;
using UIKit;
using E7.iOS.Utils;
using E7.Services;

namespace E7.iOS
{
    public partial class NewPasswordController : KeyboardBaseController
    {
        public NewPasswordController (IntPtr handle) : base (handle)
        {
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);

            btnContinuar.BackgroundColor = UIColor.FromRGB(2, 73, 125);
            btnContinuar.Layer.CornerRadius = 18.0f;
            btnContinuar.Layer.ShadowOpacity = 0.8f;
            btnContinuar.Layer.ShadowOffset = new CoreGraphics.CGSize(0.0f, 3.0f);
            btnContinuar.TouchDown += BtnContinuar_TouchDown;

            tfPassword.BackgroundColor = UIColor.FromRGBA(0, 0, 0, 256);
            tfPassword.TextColor = UIColor.White;
            tfPassword.Layer.BorderColor = UIColor.White.CGColor;
            tfPassword.Layer.BorderWidth = 1.5f;
            tfPassword.Layer.CornerRadius = 13.0f;
            tfPassword.AttributedPlaceholder = new NSAttributedString("Nueva Contraseña", null, UIColor.White);

            tfConfirm.BackgroundColor = UIColor.FromRGBA(0, 0, 0, 256);
            tfConfirm.TextColor = UIColor.White;
            tfConfirm.Layer.BorderColor = UIColor.White.CGColor;
            tfConfirm.Layer.BorderWidth = 1.5f;
            tfConfirm.Layer.CornerRadius = 13.0f;
            tfConfirm.AttributedPlaceholder = new NSAttributedString("Confirmar Contraseña", null, UIColor.White);
        }

        async void BtnContinuar_TouchDown(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(tfPassword.Text) || string.IsNullOrEmpty(tfConfirm.Text))
            {
                ShowMessage("Error", "Los campos son obligatorios");
                return;
            }
            if (tfPassword.Text != tfConfirm.Text)
            {
                ShowMessage("Error", "Las contraseñas no coinciden");
                return;
            }
            else
            {
                var resultado = await _client.CambiarClave(Application.Usuario, tfConfirm.Text);
                if (resultado.ValidarOperacion == "true")
                {
                    View.Window.RootViewController.DismissViewController(true, null);
                }
                else
                {
                    ShowMessage("Error", "Se encontró un error al actualizar la contraseña");
                }
            }
        }
    }
}