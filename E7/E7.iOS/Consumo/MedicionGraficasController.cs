﻿using System;
using E7.Services;
using E7.iOS.Utils;
using UIKit;
using System.Drawing;
using Foundation;
using System.Globalization;
using System.Diagnostics;
using E7.Models;

namespace E7.iOS
{
    public partial class MedicionGraficasController : UIViewController
    {
        PickerDataModel _pickerDataClientes, _pickerDataMedidor, _pickerDataDesdeDia, _pickerDataDesdeMes;
        PickerDataModel _pickerDataDesdeAnio, _pickerDataHastaDia, _pickerDataHastaMes, _pickerDataHastaAnio;
        TextField _pickerViewClientes, _pickerViewMedidor, _pickerViewDesdeDia, _pickerViewDesdeMes;
        TextField _pickerViewDesdeAnio, _pickerViewHastaDia, _pickerViewHastaMes, _pickerViewHastaAnio;
        Application _main;
        Alert _alerta;
        bool _showMessage;

        public MedicionGraficasController() : base("MedicionGraficasController", null)
        {
        }

        public MedicionGraficasController(IntPtr b) : base (b) { }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            // Perform any additional setup after loading the view, typically from a nib.

            if (this.View.Frame.Height <= 480)
            {
                NSLayoutConstraint.Create(txtTitle, NSLayoutAttribute.Top,
                                            NSLayoutRelation.Equal, this.View, NSLayoutAttribute.Top,
                                            1, 82).Active = true;
                NSLayoutConstraint.Create(stackPrincipalBg, NSLayoutAttribute.Top,
                                          NSLayoutRelation.Equal, this.View, NSLayoutAttribute.Top,
                                          1, 115).Active = true;
                NSLayoutConstraint.Create(stackPrincipal, NSLayoutAttribute.Top,
                                            NSLayoutRelation.Equal, this.View, NSLayoutAttribute.Top,
                                            1, 125).Active = true;
                lblClientes.Font = lblClientes.Font.WithSize(10);
                lblDesde.Font = lblClientes.Font.WithSize(10);
                lblHasta.Font = lblClientes.Font.WithSize(10);
                lblMedidor.Font = lblClientes.Font.WithSize(10);
            }

			_alerta = new Alert();
            _main = Application.GetInstance();
            _main.medicionGrafica = new MedicionGraficaModel();
            var dia = DateTime.Now.Day;
            var fecha = DateTime.Now.AddDays(-dia + 1);

            GetClientes();
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);
            NavigationController.NavigationBar.Translucent = true;
        }

        void _pickerDataClientes_ValueChanged(object sender, EventArgs e)
        {
            pickerTextFieldClientes.Text = _pickerDataClientes.SelectedItem;
            _main.medicionGrafica.Cliente = _pickerDataClientes.SelectedItem;

            _pickerDataMedidor.Items.Clear();
            foreach (var x in _main.clientes)
            {
                if (x.NomCliente == _pickerDataClientes.SelectedItem)
                {
                    _main.medicionGrafica.ClienteID = int.Parse(x.CodCliente);
					_main.medicionGrafica.MedidorID = 0;
                    if (x.MedCliente.Count != 0)
                        foreach (var y in x.MedCliente)
                        {
                            _pickerDataMedidor.Items.Add(y.Descripcion);
                        }
                    else
                        _pickerDataMedidor.Items.Add("SELECCIONE");
                        
                }
            }
            _pickerDataMedidor.SelectedIndex = 0;
            pickerTextFieldMedidor.Text = _pickerDataMedidor.SelectedItem;
        }

        void _pickerDataMedidor_ValueChanged(object sender, EventArgs e)
        {
            pickerTextFieldMedidor.Text = _pickerDataMedidor.SelectedItem;
            _main.medicionGrafica.Medidor = _pickerDataMedidor.SelectedItem;

            foreach (var x in _main.clientes)
                foreach (var y in x.MedCliente)
                    if (y.Descripcion == _pickerDataMedidor.SelectedItem)
                        _main.medicionGrafica.MedidorID = int.Parse(y.Codigo);
        }

        void _pickerDataDesdeDia_ValueChanged(object sender, EventArgs e)
        {
            var value = pickerTextFieldDesdeDia.Text;
            pickerTextFieldDesdeDia.Text = _pickerDataDesdeDia.SelectedItem;

            if (!IsDateValid())
            {
                pickerTextFieldDesdeDia.Text = value;
                _showMessage = true;
            }
        }

        void _pickerDataDesdeMes_ValueChanged(object sender, EventArgs e)
        {
            var value = pickerTextFieldDesdeMes.Text;
            pickerTextFieldDesdeMes.Text = _pickerDataDesdeMes.SelectedItem;

            if (!IsDateValid())
            {
				pickerTextFieldDesdeMes.Text = value;
                _showMessage = true;
            }
        }

        void _pickerDataDesdeAnio_ValueChanged(object sender, EventArgs e)
        {
            var value = pickerTextFieldDesdeAnio.Text;
            pickerTextFieldDesdeAnio.Text = _pickerDataDesdeAnio.SelectedItem;

            if (!IsDateValid())
            {
                pickerTextFieldDesdeAnio.Text = value;
                _showMessage = true;
            }
        }

        void _pickerDataHastaDia_ValueChanged(object sender, EventArgs e)
        {
            var value = pickerTextFieldHastaDia.Text;
            pickerTextFieldHastaDia.Text = _pickerDataHastaDia.SelectedItem;

            if (!IsDateValid())
            {
                pickerTextFieldHastaDia.Text = value;
                _showMessage = true;
            }
        }

        void _pickerDataHastaMes_ValueChanged(object sender, EventArgs e)
        {
            var value = pickerTextFieldHastaMes.Text;
            pickerTextFieldHastaMes.Text = _pickerDataHastaMes.SelectedItem;

            if (!IsDateValid())
            {
                pickerTextFieldHastaMes.Text = value;
                _showMessage = true;
			}
        }

        void _pickerDataHastaAnio_ValueChanged(object sender, EventArgs e)
        {
            var value = pickerTextFieldHastaAnio.Text;
            pickerTextFieldHastaAnio.Text = _pickerDataHastaAnio.SelectedItem;

            if (!IsDateValid())
            {
                pickerTextFieldHastaAnio.Text = value;
                _showMessage = true;
            }
        }

        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
            // Release any cached data, images, etc that aren't in use.
        }

        partial void UIButton34579_TouchUpInside(UIButton sender)
        {
            NavigationItem.BackBarButtonItem = new UIBarButtonItem("", UIBarButtonItemStyle.Plain, null);
        }

        public override bool ShouldPerformSegue(string segueIdentifier, Foundation.NSObject sender)
        {
            if (pickerTextFieldMedidor.Text == "SELECCIONE")
            {
                var a = _alerta.ShowMessage("Error", "Seleccione un punto de facturación");
                a.Show();
                return false;
            }
            return true;
        }

        void GetClientes()
        {
            var main = Application.GetInstance();
            var clientes = main.clientes;

            _pickerDataClientes = new PickerDataModel();
            _pickerDataMedidor = new PickerDataModel();
            _pickerDataDesdeDia = new PickerDataModel();
            _pickerDataDesdeMes = new PickerDataModel();
            _pickerDataDesdeAnio = new PickerDataModel();
            _pickerDataHastaDia = new PickerDataModel();
            _pickerDataHastaMes = new PickerDataModel();
            _pickerDataHastaAnio = new PickerDataModel();

            foreach (var c in clientes)
                _pickerDataClientes.Items.Add(c.NomCliente);
            
            if (clientes[0] != null)
                if (clientes[0].MedCliente.Count != 0)
                    foreach (var x in clientes[0].MedCliente)
                        _pickerDataMedidor.Items.Add(x.Descripcion);
                else
                    _pickerDataMedidor.Items.Add("SELECCIONE");

            _main.medicionGrafica.ClienteID = int.Parse((clientes[0] != null) ? clientes[0].CodCliente : "0");
            if (clientes[0].MedCliente.Count != 0)
                _main.medicionGrafica.MedidorID = int.Parse((clientes[0].MedCliente[0] != null) ? clientes[0].MedCliente[0].Codigo : "0");
            
			foreach (var x in Application.Dias)
				_pickerDataDesdeDia.Items.Add(x);
    
            foreach (var x in Application.Meses)
                _pickerDataDesdeMes.Items.Add(x);
    
            foreach (var x in Application.Anios)
                _pickerDataDesdeAnio.Items.Add(x);

			foreach (var x in Application.Dias)
				_pickerDataHastaDia.Items.Add(x);

            foreach (var x in Application.Meses)
                _pickerDataHastaMes.Items.Add(x);
            
            foreach (var x in Application.Anios)
                _pickerDataHastaAnio.Items.Add(x);

			_pickerViewClientes = new TextField(pickerTextFieldClientes);
			_pickerDataClientes.ValueChanged += _pickerDataClientes_ValueChanged;
			_pickerViewClientes.Model = _pickerDataClientes;
            pickerTextFieldClientes.Text = _pickerDataClientes.SelectedItem;
            pickerTextFieldClientes.InputView = _pickerViewClientes;

			_pickerViewMedidor = new TextField(pickerTextFieldMedidor);
			_pickerDataMedidor.ValueChanged += _pickerDataMedidor_ValueChanged;
			_pickerViewMedidor.Model = _pickerDataMedidor;
            pickerTextFieldMedidor.Text = _pickerDataMedidor.SelectedItem;
            pickerTextFieldMedidor.InputView = _pickerViewMedidor;

			_pickerViewDesdeDia = new TextField(pickerTextFieldDesdeDia);
            _pickerViewDesdeDia.DoneClicked += DoneClick;
			_pickerDataDesdeDia.ValueChanged += _pickerDataDesdeDia_ValueChanged;
			_pickerViewDesdeDia.Model = _pickerDataDesdeDia;
            pickerTextFieldDesdeDia.Text = _pickerDataDesdeDia.SelectedItem;
            pickerTextFieldDesdeDia.InputView = _pickerViewDesdeDia;

            _pickerViewDesdeMes = new TextField(pickerTextFieldDesdeMes);
            _pickerViewDesdeMes.DoneClicked += DoneClick;
            _pickerDataDesdeMes.ValueChanged += _pickerDataDesdeMes_ValueChanged;
            _pickerViewDesdeMes.Model = _pickerDataDesdeMes;
            pickerTextFieldDesdeMes.Text = _pickerDataDesdeMes.SelectedItem;
            pickerTextFieldDesdeMes.InputView = _pickerViewDesdeMes;

			_pickerViewDesdeAnio = new TextField(pickerTextFieldDesdeAnio);
            _pickerViewDesdeAnio.DoneClicked += DoneClick;
			_pickerDataDesdeAnio.ValueChanged += _pickerDataDesdeAnio_ValueChanged;
			_pickerViewDesdeAnio.Model = _pickerDataDesdeAnio;
            pickerTextFieldDesdeAnio.Text = _pickerDataDesdeAnio.SelectedItem;
            pickerTextFieldDesdeAnio.InputView = _pickerViewDesdeAnio;

			_pickerViewHastaDia = new TextField(pickerTextFieldHastaDia);
            _pickerViewHastaDia.DoneClicked += DoneClick;
			_pickerDataHastaDia.ValueChanged += _pickerDataHastaDia_ValueChanged;
			_pickerViewHastaDia.Model = _pickerDataHastaDia;
            pickerTextFieldHastaDia.Text = _pickerDataHastaDia.SelectedItem;
            pickerTextFieldHastaDia.InputView = _pickerViewHastaDia;

			_pickerViewHastaMes = new TextField(pickerTextFieldHastaMes);
            _pickerViewHastaMes.DoneClicked += DoneClick;
            _pickerDataHastaMes.ValueChanged += _pickerDataHastaMes_ValueChanged;
			_pickerViewHastaMes.Model = _pickerDataHastaMes;
            pickerTextFieldHastaMes.Text = _pickerDataHastaMes.SelectedItem;
            pickerTextFieldHastaMes.InputView = _pickerViewHastaMes;

			_pickerViewHastaAnio = new TextField(pickerTextFieldHastaAnio);
            _pickerViewHastaAnio.DoneClicked += DoneClick;
			_pickerDataHastaAnio.ValueChanged += _pickerDataHastaAnio_ValueChanged;
			_pickerViewHastaAnio.Model = _pickerDataHastaAnio;
            pickerTextFieldHastaAnio.Text = _pickerDataHastaAnio.SelectedItem;
            pickerTextFieldHastaAnio.InputView = _pickerViewHastaAnio;

            SetDates();
        }

        void SetDates()
        {
            var dia = DateTime.Now.Day.ToString();
            var mes = DateTime.Now.Month.ToString();
            var anio = DateTime.Now.Year.ToString();

            pickerTextFieldDesdeDia.Text = "01";
            pickerTextFieldDesdeMes.Text = DatesHelper.GetMonthName(mes);
            pickerTextFieldDesdeAnio.Text = anio;

            pickerTextFieldHastaDia.Text = (dia.Length == 1) ? "0" + dia : dia;
            pickerTextFieldHastaMes.Text = DatesHelper.GetMonthName(mes);
            pickerTextFieldHastaAnio.Text = anio;
			
            _pickerDataHastaDia.SelectedIndex = int.Parse(dia) - 1;
			_pickerViewHastaDia.Select(_pickerDataHastaDia.SelectedIndex, 0, true);
			
			_pickerDataDesdeMes.SelectedIndex = int.Parse(mes) - 1;
			_pickerViewDesdeMes.Select(_pickerDataDesdeMes.SelectedIndex, 0, true);
			_pickerDataHastaMes.SelectedIndex = int.Parse(mes) - 1;
			_pickerViewHastaMes.Select(_pickerDataHastaMes.SelectedIndex, 0, true);

            _main.medicionGrafica.Desde = string.Format("{0}/{1}/{2}", pickerTextFieldDesdeDia.Text, DatesHelper.GetMonthKey(pickerTextFieldDesdeMes.Text), pickerTextFieldDesdeAnio.Text);
            _main.medicionGrafica.Hasta = string.Format("{0}/{1}/{2}", pickerTextFieldHastaDia.Text, DatesHelper.GetMonthKey(pickerTextFieldHastaMes.Text), pickerTextFieldHastaAnio.Text);
        }

        bool IsDateValid()
        {
            var desdeString = string.Format("{0}/{1}/{2}", pickerTextFieldDesdeDia.Text, DatesHelper.GetMonthKey(pickerTextFieldDesdeMes.Text), pickerTextFieldDesdeAnio.Text);
            var hastaString = string.Format("{0}/{1}/{2}", pickerTextFieldHastaDia.Text, DatesHelper.GetMonthKey(pickerTextFieldHastaMes.Text), pickerTextFieldHastaAnio.Text);
			
            _main.medicionGrafica.Desde = desdeString;
			_main.medicionGrafica.Hasta = hastaString;

            DateTime desde;
            DateTime.TryParseExact(desdeString, "dd/MM/yyyy", new CultureInfo("en-US"), DateTimeStyles.None, out desde);
            DateTime hasta;
            DateTime.TryParseExact(hastaString, "dd/MM/yyyy", new CultureInfo("en-US"), DateTimeStyles.None, out hasta);

            if (desde <= hasta)
                return true;
            else
                return false;
        }

        void DoneClick(object sender, EventArgs e)
        {
            if (_showMessage)
            {
				_showMessage = false;
                var a = _alerta.ShowMessage("La fecha no es válida", "Por favor seleccione una fecha válida");
                a.Show();
            }
        }
    }
}
