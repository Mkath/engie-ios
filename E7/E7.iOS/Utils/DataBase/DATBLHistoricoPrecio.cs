﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using E7.Models;
using SQLite;

namespace E7.iOS.Utils.DataBase
{
    public class DATBLHistoricoPrecio
    {
        string folder = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);

        public Task<bool> CreateTableHP()
        {
            return Task.Run(() =>
            {
                try
                {
                    using (var connection = new SQLiteConnection(System.IO.Path.Combine(folder, "Extranet.db")))
                    {
                        connection.CreateTable<TBLHistoricoPrecio>();
                        return true;
                    }
                }
                catch (SQLiteException ex)
                {
                    return false;
                }
            });
        }

        public Task<bool> InsertTableHP(TBLHistoricoPrecio hp)
        {
            return Task.Run(() =>
            {
                try
                {
                    using (var connection = new SQLiteConnection(System.IO.Path.Combine(folder, "Extranet.db")))
                    {
                        var list = connection.Query<TBLHistoricoPrecio>("SELECT * FROM TBLHistoricoPrecio WHERE Fecha=? AND CodCliente=? AND CodPuntoFacturacion=? AND Moneda=? AND Concepto=?", hp.Fecha, hp.CodCliente, hp.CodPuntoFacturacion, hp.Moneda, hp.Concepto);
                        if (list.Count == 0)
                        {
                            connection.Insert(hp);
                        }
                        return true;
                    }
                }
                catch (SQLiteException ex)
                {
                    return false;
                }
            });
        }

        public Task<List<TBLHistoricoPrecio>> SelectTableHP(TBLHistoricoPrecio hp, DateTime desde, DateTime hasta)
        {
            return Task.Run(() =>
            {
                try
                {
                    List<TBLHistoricoPrecio> list = new List<TBLHistoricoPrecio>();
                    using (var connection = new SQLiteConnection(System.IO.Path.Combine(folder, "Extranet.db")))
                    {
                        list = connection.Query<TBLHistoricoPrecio>("SELECT * FROM TBLHistoricoPrecio WHERE CodCliente=? AND CodPuntoFacturacion=? AND Moneda=? AND Fecha>? AND Fecha<?", hp.CodCliente, hp.CodPuntoFacturacion, hp.Moneda, desde.Ticks, hasta.Ticks);
                        return list;
                    }
                }
                catch (SQLiteException ex)
                {
                    return null;
                }
            });
        }

        public Task<List<TBLHistoricoPrecio>> SelectTableHP(TBLHistoricoPrecio hp)
        {
            return Task.Run(() =>
            {
                try
                {
                    List<TBLHistoricoPrecio> list = new List<TBLHistoricoPrecio>();
                    using (var connection = new SQLiteConnection(System.IO.Path.Combine(folder, "Extranet.db")))
                    {
                        list = connection.Query<TBLHistoricoPrecio>("SELECT * FROM TBLHistoricoPrecio WHERE CodCliente=? AND CodPuntoFacturacion=? AND Moneda=?", hp.CodCliente, hp.CodPuntoFacturacion, hp.Moneda);
                        return list;
                    }
                }
                catch (SQLiteException ex)
                {
                    return null;
                }
            });
        }

        public Task<List<TBLHistoricoPrecio>> ValidateTableHP()
        {
            return Task.Run(() =>
            {
                try
                {
                    List<TBLHistoricoPrecio> list = new List<TBLHistoricoPrecio>();
                    using (var connection = new SQLiteConnection(System.IO.Path.Combine(folder, "Extranet.db")))
                    {
                        list = connection.Table<TBLHistoricoPrecio>().ToList();
                        return list;
                    }
                }
                catch (SQLiteException ex)
                {
                    return null;
                }
            });
        }

        public Task<bool> DeleteTableHP(DateTime date)
        {
            return Task.Run(() =>
            {
                try
                {
                    using (var connection = new SQLiteConnection(System.IO.Path.Combine(folder, "Extranet.db")))
                    {
                        connection.Query<TBLHistoricoPrecio>("DELETE FROM TBLHistoricoPrecio WHERE Fecha<?", date.Ticks);
                        return true;
                    }
                }
                catch (SQLiteException ex)
                {
                    return false;
                }
            });
        }
    }
}
