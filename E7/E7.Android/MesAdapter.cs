﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace E7.Droid
{
    public class Mes
    {
        public string IdMes { get; set; }
        public string NomMes { get; set; }
    }
    public class COMes {
        public static JavaList<Mes> ListarMes()
        {
            JavaList<Mes> listar = new JavaList<Mes>();

            listar.Add(new Mes() { IdMes = "1", NomMes = "Enero" });
            listar.Add(new Mes() { IdMes = "2", NomMes = "Febrero" });
            listar.Add(new Mes() { IdMes = "3", NomMes = "Marzo" });
            listar.Add(new Mes() { IdMes = "4", NomMes = "Abril" });
            listar.Add(new Mes() { IdMes = "5", NomMes = "Mayo" });
            listar.Add(new Mes() { IdMes = "6", NomMes = "Junio" });
            listar.Add(new Mes() { IdMes = "7", NomMes = "Julio" });
            listar.Add(new Mes() { IdMes = "8", NomMes = "Agosto" });
            listar.Add(new Mes() { IdMes = "9", NomMes = "Septiembre" });
            listar.Add(new Mes() { IdMes = "10", NomMes = "Octubre" });
            listar.Add(new Mes() { IdMes = "11", NomMes = "Noviembre" });
            listar.Add(new Mes() { IdMes = "12", NomMes = "Diciembre" });

            return listar;
        }
    }

    class MesAdapter : BaseAdapter
    {
        private Context c;
        private JavaList<Mes> mes;
        private LayoutInflater inflater;

        public MesAdapter(Context c, JavaList<Mes> mes)
        {
            this.c = c;
            this.mes = mes;
        }
        public override Java.Lang.Object GetItem(int position)
        {
            return mes.Get(position);
        }
        public override long GetItemId(int position)
        {
            return position;
        }
        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            if (inflater == null)
            {
                inflater = (LayoutInflater)c.GetSystemService(Context.LayoutInflaterService);
            }
            if (convertView == null)
            {
                convertView = inflater.Inflate(Resource.Layout.recursoSpinner, parent, false);
            }

            TextView cod = convertView.FindViewById<TextView>(Resource.Id.cod1);
            TextView nomb = convertView.FindViewById<TextView>(Resource.Id.nom1);
            cod.Text = mes[position].IdMes;
            nomb.Text = mes[position].NomMes;
            return convertView;
        }
        public override int Count
        {
            get { return mes.Size(); }
        }
    }

}